#FROM openjdk:11
#EXPOSE 8080
#ARG JAR_FILE=target/sist_five-0.0.1-SNAPSHOT.jar
#COPY ${JAR_FILE} .
#CMD [ "java", "-jar",  "/sist_five-0.0.1-SNAPSHOT.jar"]

FROM openjdk:11
ADD target/sist_five-0.0.1-SNAPSHOT.jar app.jar

RUN sh -c 'touch /app-jar'
ENTRYPOINT ["java","-jar","/app.jar"]
