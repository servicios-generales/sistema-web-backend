package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.Departamento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository
public interface DepartamentoRepositorio extends JpaRepository<Departamento,Long> {
    @Query(value= "{call dbo.sp_list_departamentos()}", nativeQuery = true)
    List<Departamento> get_departamentos();
}
