package com.idesolution.sist_five.repositorys;
import com.idesolution.sist_five.dto.log_error;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository
public interface LogErrorRepositorio extends JpaRepository<log_error,Long> {
    @Query(value= "{call dbo.sp_add_logerror(:loge_webmethod,:loge_error)}", nativeQuery = true)
    void add_logerror(@Param("loge_webmethod") String loge_webmethod,
                                    @Param("loge_error") String loge_error);
}
