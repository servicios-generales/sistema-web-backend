package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.Opcion_menu;
import com.idesolution.sist_five.models.Parametro;
import com.idesolution.sist_five.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository

public interface ParametroRepositorio extends JpaRepository<Parametro,Long> {
    @Query(value= "{call dbo.sp_get_parametro(:conc_id)}", nativeQuery = true)
    List<Map<String,Object>>  get_parametro(@Param("conc_id") Integer conc_id);

    @Query(value= "{call dbo.sp_get_fechaactual()}", nativeQuery = true)
    List<Map<String,Object>>  sp_get_fechaactual();

    @Query(value= "{call dbo.sp_get_list_parametros(:para_prefijo)}", nativeQuery = true)
    List<Map<String,Object>>  get_list_parametros(@Param("para_prefijo") Integer para_prefijo);

    @Query(value= "{call dbo.sp_list_parametro_x_prefijo(:para_prefijo)}", nativeQuery = true)
    List<Map<String,Object>>  get_parametros_x_prefijo(@Param("para_prefijo") Integer para_prefijo
    );

    @Query(value= "{call dbo.sp_list_parametro_todos()}", nativeQuery = true)
    List<Map<String,Object>>  get_parametros_todos(
    );


    @Query(value= "{call dbo.sp_list_parametro_x_prefijoycorrelativo(:para_prefijo, :para_correlativo)}", nativeQuery = true)
    List<Map<String,Object>>  get_parametro_x_prefijoycorrelativo(@Param("para_prefijo") Integer para_prefijo,@Param("para_correlativo") Integer para_correlativo
    );

    @Query(value= "{call dbo.sp_update_parametro_x_id(:para_id, :para_prefijo , :para_correlativo ,:para_int1 ,:para_int2 ,:para_cadena1,:para_cadena2 ,:para_fecha1,:para_fecha2)}", nativeQuery = true)
    void update_parametro_x_id(
            @Param("para_id") Long para_id,
            @Param("para_prefijo") Integer para_prefijo,
            @Param("para_correlativo") Integer para_correlativo,
            @Param("para_int1") Integer para_int1,
            @Param("para_int2") Integer para_int2,
            @Param("para_cadena1") String para_cadena1,
            @Param("para_cadena2") String para_cadena2,
            @Param("para_fecha1") Date para_fecha1,
            @Param("para_fecha2") Date para_fecha2
    );

    @Query(value= "{call dbo.sp_add_parametro(:para_prefijo , :para_correlativo ,:para_int1 ,:para_int2 ,:para_cadena1,:para_cadena2 ,:para_fecha1,:para_fecha2)}", nativeQuery = true)
    void add_parametro(
            @Param("para_prefijo") Integer para_prefijo,
            @Param("para_correlativo") Integer para_correlativo,
            @Param("para_int1") Integer para_int1,
            @Param("para_int2") Integer para_int2,
            @Param("para_cadena1") String para_cadena1,
            @Param("para_cadena2") String para_cadena2,
            @Param("para_fecha1") Date para_fecha1,
            @Param("para_fecha2") Date para_fecha2
    );



    @Query(value= "{call dbo.sp_parametro_exists(:para_cadena1)}", nativeQuery = true)
    boolean existsByPara_cadena1( @Param("para_cadena1") String para_cadena1);

    @Query(value= "{call dbo.sp_delete_parametro(:para_id)}", nativeQuery = true)
    void delete_parametro(
            @Param("para_id") Integer para_id
    );
}
