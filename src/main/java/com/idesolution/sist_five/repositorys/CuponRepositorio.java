package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.Cupon;
import com.idesolution.sist_five.models.Especialidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
@Repository
public interface CuponRepositorio extends JpaRepository<Cupon,Long> {
    /*@Query(value= "{call sp_listar_cupom_x_servicio(:serv_id)}", nativeQuery = true)
    List<Especialidad> get_especialidades_x_servicio(@Param("serv_id") Integer serv_id);

    @Query(value= "{call sp_listar_especialidad_x_usuario(:usua_id)}", nativeQuery = true)
    List<Especialidad> listar_especialidad_x_usuario(@Param("usua_id") Integer usua_id);*/

    @Query(value= "{call sp_listar_cupones()}", nativeQuery = true)
    List<Map<String, Object>>  get_listar_cupones();


    @Query(value= "{call dbo.sp_list_cupon_x_id(:cupo_id)}", nativeQuery = true)
    List<Cupon>  get_cupon_x_id(@Param("cupo_id") Integer cupo_id
    );

    @Query(value= "{call dbo.sp_update_cupon_x_id(:cupo_id, :cupo_codigo,:cupo_tipoAplica, :cupo_categoria , :cupo_tipoDcto , :cupo_importeDcto , :cupo_fechaCaduca, " +
            ":cupo_fechaInicio, :cupo_limitexCupon, :cupo_limitexCategoria, :cupo_limitexUsuario, :cupo_tipoDestino)}", nativeQuery = true)
    void update_cupon_x_id(
            @Param("cupo_id") Long cupo_id,
            @Param("cupo_codigo") String cupo_codigo,
            @Param("cupo_tipoAplica") Integer cupo_tipoAplica,
            @Param("cupo_categoria") String cupo_categoria,
            @Param("cupo_tipoDcto") Integer cupo_tipoDcto,
            @Param("cupo_importeDcto") Double cupo_importeDcto,
            @Param("cupo_fechaCaduca") Date cupo_fechaCaduca,
            @Param("cupo_fechaInicio") Date cupo_fechaInicio,
            @Param("cupo_limitexCupon") Integer cupo_limitexCupon,
            @Param("cupo_limitexCategoria") Integer cupo_limitexCategoria,
            @Param("cupo_limitexUsuario") Integer cupo_limitexUsuario,
            @Param("cupo_tipoDestino") Integer cupo_tipoDestino
    );

    @Query(value= "{call dbo.sp_add_cupon(:cupo_codigo,:cupo_tipoAplica, :cupo_categoria , :cupo_tipoDcto , :cupo_importeDcto , :cupo_fechaCaduca, :cupo_fechaInicio, :cupo_limitexCupon, :cupo_limitexCategoria, :cupo_limitexUsuario, :cupo_tipoDestino)}", nativeQuery = true)
    void add_cupon(
            @Param("cupo_codigo") String cupo_codigo,
            @Param("cupo_tipoAplica") Integer cupo_tipoAplica,
            @Param("cupo_categoria") String cupo_categoria,
            @Param("cupo_tipoDcto") Integer cupo_tipoDcto,
            @Param("cupo_importeDcto") Double cupo_importeDcto,
            @Param("cupo_fechaCaduca") Date cupo_fechaCaduca,
            @Param("cupo_fechaInicio") Date cupo_fechaInicio,
            @Param("cupo_limitexCupon") Integer cupo_limitexCupon,
            @Param("cupo_limitexCategoria") Integer cupo_limitexCategoria,
            @Param("cupo_limitexUsuario") Integer cupo_limitexUsuario,
            @Param("cupo_tipoDestino") Integer cupo_tipoDestino
    );


    @Query(value= "{call dbo.sp_delete_cupon(:cupo_id)}", nativeQuery = true)
    void delete_cupon(
            @Param("cupo_id") Integer cupo_id
    );

    @Query(value= "{call dbo.sp_public_cupon(:cupo_id)}", nativeQuery = true)
    Integer public_cupon(
            @Param("cupo_id") Integer cupo_id
    );

    @Query(value= "{call dbo.sp_active_cupon(:cupo_id)}", nativeQuery = true)
    void active_cupon(
            @Param("cupo_id") Integer cupo_id
    );

    @Query(value= "{call dbo.sp_aplicar_cupon_visita(:corr_id, :soci_id, :codigo)}", nativeQuery = true)
    List<Map<String, Object>>  aplicar_cupon_visita(@Param("corr_id") Integer corr_id,
                                                    @Param("soci_id") Integer soci_id,
                                                    @Param("codigo") String codigo);

    @Query(value= "{call dbo.sp_aplicar_cupon_proforma(:corr_id, :soci_id, :codigo)}", nativeQuery = true)
    List<Map<String, Object>>  aplicar_cupon_proforma(@Param("corr_id") Integer corr_id,
                                                    @Param("soci_id") Integer soci_id,
                                                    @Param("codigo") String codigo);

    @Query(value= "{call dbo.sp_cupon_exists(:cupo_codigo)}", nativeQuery = true)
    boolean existsByCupo_codigo( @Param("cupo_codigo") String cupo_codigo);

    @Query(value= "{call dbo.sp_caducar_cupones()}", nativeQuery = true)
    void caducar_cupones(
    );

}
