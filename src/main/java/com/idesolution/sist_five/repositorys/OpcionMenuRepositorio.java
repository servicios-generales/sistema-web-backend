package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.Opcion_menu;
import com.idesolution.sist_five.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Map;

@Repository

public interface OpcionMenuRepositorio extends JpaRepository<Opcion_menu,Long> {

    @Query(value= "{call dbo.sp_list_opciones(:usua_id,:opme_tipo)}", nativeQuery = true)
    List<Map<String,Object>> get_opcionesxid(@Param("usua_id") Integer usua_id,
                                             @Param("opme_tipo") Integer opme_tipo);
}
