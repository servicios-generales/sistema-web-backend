package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.SolicitudCita;
import com.idesolution.sist_five.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Map;

@Repository

public interface SolicitudCitaReposiorio extends JpaRepository<SolicitudCita,Long> {

    @Query(value= "{call dbo.sp_usuarios_cita(:usua_id,:espe_id,:soci_nivel,:soci_ubicacion,:soci_diaaten,:soci_horaaten, :soci_detalle, :soci_destino, :soci_estado)}", nativeQuery = true)
    List<Usuario> usuarios_cita(
            @Param("usua_id") Integer usua_id,
            @Param("espe_id") Integer espe_id,
            @Param("soci_nivel") Integer soci_nivel,
            @Param("soci_ubicacion") String soci_ubicacion,
            @Param("soci_diaaten") String soci_diaaten,
            @Param("soci_horaaten") String soci_horaaten,
            @Param("soci_detalle") String soci_detalle,
            @Param("soci_destino") Integer soci_destino,
            @Param("soci_estado") Integer soci_estado
    );

    @Query(value= "{call dbo.sp_solicitar_cita(:usua_id,:espe_id,:soci_nivel,:soci_ubicacion,:soci_diaaten,:soci_horaaten, :soci_detalle, :soci_destino, :soci_estado, :soci_direccion, :soci_latitud, :soci_longitud, :soci_instruccion , :soci_etiqueta, :soci_idetiqueta, :soci_guardaetiqueta)}", nativeQuery = true)
    void solicitar_cita(
            @Param("usua_id") Integer usua_id,
            @Param("espe_id") Integer espe_id,
            @Param("soci_nivel") Integer soci_nivel,
            @Param("soci_ubicacion") String soci_ubicacion,
            @Param("soci_diaaten") String soci_diaaten,
            @Param("soci_horaaten") String soci_horaaten,
            @Param("soci_detalle") String soci_detalle,
            @Param("soci_destino") Integer soci_destino,
            @Param("soci_estado") Integer soci_estado,
            @Param("soci_direccion") String soci_direccion,
            @Param("soci_latitud") Double soci_latitud,
            @Param("soci_longitud") Double soci_longitud,
            @Param("soci_instruccion") String soci_instruccion,
            @Param("soci_etiqueta") String soci_etiqueta,
            @Param("soci_idetiqueta") Integer soci_idetiqueta,
            @Param("soci_guardaetiqueta") Integer soci_guardaetiqueta
    );
    @Query(value= "{call dbo.sp_resolicitar_cita(:corr_id,:usua_id,:espe_id,:soci_nivel,:soci_ubicacion,:soci_diaaten,:soci_horaaten, :soci_detalle, :soci_destino, :soci_estado, :soci_direccion, :soci_latitud, :soci_longitud, :soci_instruccion , :soci_idetiqueta)}", nativeQuery = true)
    void resolicitar_cita(
            @Param("corr_id") Integer corr_id,
            @Param("usua_id") Integer usua_id,
            @Param("espe_id") Integer espe_id,
            @Param("soci_nivel") Integer soci_nivel,
            @Param("soci_ubicacion") String soci_ubicacion,
            @Param("soci_diaaten") String soci_diaaten,
            @Param("soci_horaaten") String soci_horaaten,
            @Param("soci_detalle") String soci_detalle,
            @Param("soci_destino") Integer soci_destino,
            @Param("soci_estado") Integer soci_estado,
            @Param("soci_direccion") String soci_direccion,
            @Param("soci_latitud") Double soci_latitud,
            @Param("soci_longitud") Double soci_longitud,
            @Param("soci_instruccion") String soci_instruccion,
            @Param("soci_idetiqueta") Integer soci_idetiqueta
    );

    @Query(value= "{call dbo.sp_solicitar_cita_multiple(:usua_id,:espe_id,:soci_nivel,:soci_ubicacion,:soci_diaaten,:soci_horaaten, :soci_detalle, :soci_destino, :soci_estado, :soci_direccion, :soci_latitud, :soci_longitud, :soci_instruccion , :soci_etiqueta, :soci_idetiqueta, :soci_guardaetiqueta, :corr_id)}", nativeQuery = true)
    void solicitar_cita_multiple(
            @Param("usua_id") Integer usua_id,
            @Param("espe_id") Integer espe_id,
            @Param("soci_nivel") Integer soci_nivel,
            @Param("soci_ubicacion") String soci_ubicacion,
            @Param("soci_diaaten") String soci_diaaten,
            @Param("soci_horaaten") String soci_horaaten,
            @Param("soci_detalle") String soci_detalle,
            @Param("soci_destino") Integer soci_destino,
            @Param("soci_estado") Integer soci_estado,
            @Param("soci_direccion") String soci_direccion,
            @Param("soci_latitud") Double soci_latitud,
            @Param("soci_longitud") Double soci_longitud,
            @Param("soci_instruccion") String soci_instruccion,
            @Param("soci_etiqueta") String soci_etiqueta,
            @Param("soci_idetiqueta") Integer soci_idetiqueta,
            @Param("soci_guardaetiqueta") Integer soci_guardaretiqueta,
            @Param("corr_id") Integer corr_id

    );

    @Query(value= "{call dbo.sp_resolicitar_cita_multiple(:corr_id,:usua_id,:espe_id,:soci_nivel,:soci_ubicacion,:soci_diaaten,:soci_horaaten, :soci_detalle, :soci_destino, :soci_estado, :soci_direccion, :soci_latitud, :soci_longitud, :soci_instruccion , :soci_idetiqueta )}", nativeQuery = true)
    void resolicitar_cita_multiple(
            @Param("corr_id") Integer corr_id,
            @Param("usua_id") Integer usua_id,
            @Param("espe_id") Integer espe_id,
            @Param("soci_nivel") Integer soci_nivel,
            @Param("soci_ubicacion") String soci_ubicacion,
            @Param("soci_diaaten") String soci_diaaten,
            @Param("soci_horaaten") String soci_horaaten,
            @Param("soci_detalle") String soci_detalle,
            @Param("soci_destino") Integer soci_destino,
            @Param("soci_estado") Integer soci_estado,
            @Param("soci_direccion") String soci_direccion,
            @Param("soci_latitud") Double soci_latitud,
            @Param("soci_longitud") Double soci_longitud,
            @Param("soci_instruccion") String soci_instruccion,
            @Param("soci_idetiqueta") Integer soci_idetiqueta
    );
    @Query(value= "{call dbo.sp_requerimientos(:usua_id,:tipo)}", nativeQuery = true)
    List<Map<String,Object>> get_requerimientos(
            @Param("usua_id") Integer usua_id,
            @Param("tipo") Integer tipo
    );
    @Query(value= "{call dbo.sp_solicitudes(:usua_id)}", nativeQuery = true)
    List<Map<String,Object>> get_solicitudes(
            @Param("usua_id") Integer usua_id
    );
    @Query(value= "{call dbo.sp_detalle_requerimiento(:soci_id)}", nativeQuery = true)
    List<Map<String,Object>> get_requerimiento_detalle(
            @Param("soci_id") Integer soci_id
    );

    @Query(value= "{call dbo.sp_confirmar_solicitud(:usua_id, :soci_id, :soci_tipo, :soci_estado, :deso_costo)}", nativeQuery = true)
    void confirmar_solicitud(
            @Param("usua_id") Integer usua_id,
            @Param("soci_id") Integer soci_id,
            @Param("soci_tipo") Integer soci_tipo,
            @Param("soci_estado") Integer soci_estado,
            @Param("deso_costo") Double deso_costo

    );
    @Query(value= "{call dbo.sp_confirmar_solicitud_pago(:usua_id, :soci_id, :soci_tipo, :soci_estado, :deso_costo, :conc_correlativo, :para_int1, :corr_id, :cupo_codigo)}", nativeQuery = true)
    void realizar_pago(
            @Param("usua_id") Integer usua_id,
            @Param("soci_id") Integer soci_id,
            @Param("soci_tipo") Integer soci_tipo,
            @Param("soci_estado") Integer soci_estado,
            @Param("deso_costo") Double deso_costo,
            @Param("conc_correlativo") Integer conc_correlativo,
            @Param("para_int1") Integer para_int1,
            @Param("corr_id") Integer corr_id,
            @Param("cupo_codigo") String cupo_codigo
    );

    @Query(value= "{call dbo.sp_confirmar_pago_cotizacion(:usua_id, :soci_id, :soci_tipo, :soci_estado, :deso_costo, :conc_correlativo, :para_int1, :corr_id, :cupo_codigo)}", nativeQuery = true)
    void realizar_pago_cotizacion(
            @Param("usua_id") Integer usua_id,
            @Param("soci_id") Integer soci_id,
            @Param("soci_tipo") Integer soci_tipo,
            @Param("soci_estado") Integer soci_estado,
            @Param("deso_costo") Double deso_costo,
            @Param("conc_correlativo") Integer conc_correlativo,
            @Param("para_int1") Integer para_int1,
            @Param("corr_id") Integer corr_id,
            @Param("cupo_codigo") String cupo_codigo

    );
    @Query(value= "{call dbo.sp_confirmar_pago_a_administrador(:usua_id, :soci_id, :tipo)}", nativeQuery = true)
    void confirmar_pago_a_administrador(
            @Param("usua_id") Integer usua_id,
            @Param("soci_id") Integer soci_id,
            @Param("tipo") Integer tipo

    );

    @Query(value= "{call dbo.sp_requerimientos_pendientes(:usua_id)}", nativeQuery = true)
    List<Map<String,Object>> get_requerimientos_pendientes(
            @Param("usua_id") Integer usua_id
    );

    @Query(value= "{call dbo.sp_detalle_requerimiento_pendiente(:soci_id)}", nativeQuery = true)
    List<Map<String,Object>> get_requerimiento_detalle_pendiente(
            @Param("soci_id") Integer soci_id
    );

    @Query(value= "{call dbo.sp_requerimientos_confirmadas(:usua_id)}", nativeQuery = true)
    List<Map<String,Object>> get_requerimientos_confirmadas(
            @Param("usua_id") Integer usua_id
    );

    @Query(value= "{call dbo.sp_detalle_requerimiento_confirmada(:soci_id)}", nativeQuery = true)
    List<Map<String,Object>> get_requerimiento_detalle_confirmada(
            @Param("soci_id") Integer soci_id
    );

    @Query(value= "{call dbo.sp_registrar_correlativo(:usua_id)}", nativeQuery = true)
    Integer registrar_correlativo(
            @Param("usua_id") Integer usua_id
    );

    @Query(value= "{call dbo.sp_solicitud_detalle_general(:corr_id)}", nativeQuery = true)
    List<Map<String,Object>> get_solicitud_detalle_general(
            @Param("corr_id") Integer corr_id
    );

    @Query(value= "{call dbo.sp_realizar_pago_solicitud(:corr_id,:soci_destino)}", nativeQuery = true)
    List<Map<String,Object>> get_realizar_pago_solicitud(
            @Param("corr_id") Integer corr_id,
            @Param("soci_destino") Integer soci_destino
    );

    @Query(value= "{call dbo.sp_realizar_pago_proforma(:corr_id,:soci_destino)}", nativeQuery = true)
    List<Map<String,Object>> get_realizar_pago_proforma(
            @Param("corr_id") Integer corr_id,
            @Param("soci_destino") Integer soci_destino
    );

    @Query(value= "{call dbo.sp_solicitud_detalle_especifico(:corr_id)}", nativeQuery = true)
    List<Map<String,Object>> get_solicitud_detalle_especifico(
            @Param("corr_id") Integer corr_id
    );
    @Query(value= "{call dbo.sp_add_alertascm(:correlativo,:fechasol,:horasol,:icono,:url,:usua_id)}", nativeQuery = true)
    void add_alerta(
            @Param("correlativo") Integer correlativo,
            @Param("fechasol") String fechasol,
            @Param("horasol") String horasol,
            @Param("icono") String icono,
            @Param("url") String url,
            @Param("usua_id") Integer usua_id
    );

    @Query(value= "{call dbo.sp_inactivar_solicitud(:corr_id)}", nativeQuery = true)
    void inactivar_solicitud(
            @Param("corr_id") Integer corr_id
    );

    @Query(value= "{call dbo.sp_list_alerta(:usua_id)}", nativeQuery = true)
    List<Map<String,Object>> list_alertas(
            @Param("usua_id") Integer usua_id
    );

    @Query(value= "{call dbo.sp_repetir_solicitud_cita(:corr_id)}", nativeQuery = true)
    List<Map<String,Object>> repetir_solicitud_cita(
            @Param("corr_id") Integer corr_id
    );

    @Query(value= "{call dbo.sp_subir_pago(:corr_id)}", nativeQuery = true)
    List<Map<String,Object>> subir_comprobante(
            @Param("corr_id") Integer corr_id
    );

    @Query(value= "{call dbo.sp_add_proforma(:usua_id,:soci_id,:prof_total)}", nativeQuery = true)
    Integer add_proforma(
            @Param("usua_id") Integer usua_id,
            @Param("soci_id") Integer soci_id,
            @Param("prof_total") Double prof_total
    );

    @Query(value= "{call dbo.sp_update_proforma(:usua_id,:soci_id,:prof_total)}", nativeQuery = true)
    Integer update_proforma(
            @Param("usua_id") Integer usua_id,
            @Param("soci_id") Integer soci_id,
            @Param("prof_total") Double prof_total
    );

    @Query(value= "{call dbo.sp_add_detalle_proforma(:depr_detalle,:depr_importe,:depr_cantidad,:prof_id)}", nativeQuery = true)
    void add_detalle_proforma(
            @Param("depr_detalle") String depr_detalle,
            @Param("depr_importe") Double depr_importe,
            @Param("depr_cantidad") Integer depr_cantidad,
            @Param("prof_id") Integer prof_id
    );

    @Query(value= "{call dbo.sp_detalle_proforma(:soci_id)}", nativeQuery = true)
    List<Map<String,Object>> detalle_proforma(
            @Param("soci_id") Integer soci_id
    );

    @Query(value= "{call dbo.sp_detalle_proforma_especialista(:soci_id)}", nativeQuery = true)
    List<Map<String,Object>> detalle_proforma_especialista(
            @Param("soci_id") Integer soci_id
    );

    @Query(value= "{call dbo.sp_confirmar_proforma(:usua_id, :soci_id)}", nativeQuery = true)
    void confirmar_proforma(
            @Param("usua_id") Integer usua_id,
            @Param("soci_id") Integer soci_id

    );

    @Query(value= "{call dbo.sp_rechazar_proforma(:usua_id, :soci_id)}", nativeQuery = true)
    void rechazar_proforma(
            @Param("usua_id") Integer usua_id,
            @Param("soci_id") Integer soci_id

    );

    @Query(value= "{call dbo.sp_subir_imagen_trabajo(:soci_id,:nombre)}", nativeQuery = true)
    void subir_imagen_trabajo(
            @Param("soci_id") Integer soci_id,
            @Param("nombre") String nombre
    );

    @Query(value= "{call dbo.sp_list_imagen_trabajo(:soci_id)}", nativeQuery = true)
    List<Map<String,Object>> list_imagen_trabajo(
            @Param("soci_id") Integer soci_id
    );

    @Query(value= "{call dbo.sp_delete_imagen_trabajo(:soci_id)}", nativeQuery = true)
    void delete_imagen_trabajo(
            @Param("soci_id") Integer soci_id
    );

    @Query(value= "{call dbo.sp_caducar_solicitudes()}", nativeQuery = true)
    void tarea_automatica_caducar(
    );

    @Query(value= "{call dbo.sp_registrar_suscripcion(:usua_id, :suscripcion)}", nativeQuery = true)
    List<Map<String,Object>> validar_suscripcion(
            @Param("usua_id") Integer usua_id,
            @Param("suscripcion") String suscripcion
    );
    @Query(value= "{call dbo.sp_inactivar_suscripcion(:usua_id, :suscripcion)}", nativeQuery = true)
    List<Map<String,Object>> inactivar_suscripcion(
            @Param("usua_id") Integer usua_id,
            @Param("suscripcion") String suscripcion
    );

    @Query(value= "{call dbo.sp_buscar_usuarios()}", nativeQuery = true)
    List<Map<String,Object>> buscar_usuarios(

    );
    @Query(value= "{call dbo.sp_listar_suscripciones(:usua_id)}", nativeQuery = true)
    List<Map<String,Object>> listar_suscripciones(
            @Param("usua_id") Integer usua_id
    );

    @Query(value= "{call dbo.sp_mostrar_notificacion_job2(:usua_id,:estado)}", nativeQuery = true)
    List<Map<String,Object>> mostrar_notificacion_job(
            @Param("usua_id") Integer usua_id,
            @Param("estado") Integer estado
    );

    @Query(value= "{call dbo.sp_buscar_usuarios_clientes_caducar()}", nativeQuery = true)
    List<Map<String,Object>> buscar_usuarios_clientes(

    );

    @Query(value= "{call dbo.sp_caducar_solicitudes_x_usuario(:usua_id)}", nativeQuery = true)
    void caducar_solicitudes(
            @Param("usua_id") Integer usua_id
    );
    @Query(value= "{call dbo.sp_insistir_notificacion_especialista_muyurgente()}", nativeQuery = true)
    void reenviar_notificacion(
    );
    @Query(value= "{call dbo.sp_insistir_notificacion_especialista_urgente()}", nativeQuery = true)
    void reenviar_notificacion_urgente(
    );
    @Query(value= "{call dbo.sp_insistir_notificacion_especialista_normal()}", nativeQuery = true)
    void reenviar_notificacion_normal(
    );
    @Query(value= "{call dbo.sp_monto_pago_visita(:soci_id, :codigo)}", nativeQuery = true)
    List<Map<String,Object>> monto_pago_visita(
            @Param("soci_id") Integer soci_id,
            @Param("codigo") String codigo
    );
    @Query(value= "{call dbo.sp_monto_pago_cotizacion(:soci_id, :codigo)}", nativeQuery = true)
    List<Map<String,Object>> monto_pago_cotizacion(
            @Param("soci_id") Integer soci_id,
            @Param("codigo") String codigo
    );
    @Query(value= "{call dbo.sp_validar_confirmar_pago(:soci_id, :deso_tipo)}", nativeQuery = true)
    Integer validar_confirmar_pago(

            @Param("soci_id") Integer soci_id,
            @Param("deso_tipo") Integer deso_tipo
    );

    @Query(value= "{call dbo.sp_cancelar_solicitud(:usua_id, :corr_id, :motivo)}", nativeQuery = true)
    void cancelar_solicitud(
            @Param("usua_id") Integer usua_id,
            @Param("corr_id") Integer corr_id,
            @Param("motivo") Integer motivo

    );
    @Query(value= "{call dbo.sp_detalle_requerimiento2(:soci_id)}", nativeQuery = true)
    List<Map<String,Object>> get_requerimiento_detalle2(
            @Param("soci_id") Integer soci_id
    );

    @Query(value= "{call dbo.sp_solicitudes_x_espeid(:usua_id, :espe_id)}", nativeQuery = true)
    List<Map<String,Object>> get_solicitudes_x_espeid(
            @Param("usua_id") Integer usua_id,
            @Param("espe_id") Integer espe_id
    );

    @Query(value= "{call dbo.sp_solicitudes_x_estado(:usua_id, :esta_id)}", nativeQuery = true)
    List<Map<String,Object>> get_solicitudes_x_estado(
            @Param("usua_id") Integer usua_id,
            @Param("esta_id") Integer esta_id
    );

    @Query(value= "{call dbo.sp_obtener_cobro_five(:soci_id)}", nativeQuery = true)
    String obtener_cobro_five(
            @Param("soci_id") Integer soci_id
    );

    @Query(value= "{call dbo.sp_obtener_cobro_five_largo(:soci_id)}", nativeQuery = true)
    List<Map<String,Object>> obtener_cobro_five_largo(
            @Param("soci_id") Integer soci_id
    );



}
