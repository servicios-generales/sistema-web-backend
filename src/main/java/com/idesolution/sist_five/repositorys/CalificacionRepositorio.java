package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.Calificacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Repository
public interface CalificacionRepositorio extends JpaRepository<Calificacion,Long>{

    @Query(value= "{call dbo.sp_add_calificacion(:usuario,:especialista,:comentario,:rpta,:calificacion, :soci_id)}", nativeQuery = true)
    void add_calificacion(
            @Param("usuario") Integer cliente_id,
            @Param("especialista") Integer especialista_id,
            @Param("comentario") String cali_opinion,
            @Param("rpta") Integer cali_rpta,
            @Param("calificacion") Integer cali_puntaje,
            @Param("soci_id") Integer soci_id
    );
    @Query(value= "{call dbo.sp_lista_tecnicos_calificar(:usua_id)}", nativeQuery = true)
    List<Map<String, Object>>  listar_tecnicos_calificar( @Param("usua_id") Integer usua_id
    );

    @Query(value= "{call dbo.sp_lista_tecnicos_calificar_xid(:soci_id)}", nativeQuery = true)
    List<Map<String, Object>>  listar_tecnicos_calificar_xid( @Param("soci_id") Integer soci_id
    );

    @Query(value= "{call dbo.sp_add_calificacion_previa(:usuario,:especialista,:soci_id)}", nativeQuery = true)
    List<Map<String, Object>>  guardar_tecnico_a_calificar( @Param("usuario") Integer usuario,
                                                            @Param("especialista") Integer especialista,
                                                            @Param("soci_id") Integer soci_id
    );

    @Query(value= "{call dbo.sp_informacion_tecnico(:usua_id)}", nativeQuery = true)
    List<Map<String, Object>>  informacion_tecnico( @Param("usua_id") Integer usua_id
    );

    @Query(value= "{call dbo.sp_comentarios_especialista(:usua_id)}", nativeQuery = true)
    List<Map<String, Object>>  comentarios_tecnico( @Param("usua_id") Integer usua_id
    );
}
