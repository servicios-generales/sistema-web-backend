package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.SmsRequest;

public interface SmsSender {
    void sendSms(SmsRequest smsRequest);
}
