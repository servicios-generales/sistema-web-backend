package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.Opcion_menu;
import com.idesolution.sist_five.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Repository
public interface UsuarioRepositorio extends JpaRepository<Usuario,Long> {
        @Query(value= "{call dbo.sp_list_usuario_login2(:usua_email,:usua_tipo,:usua_clave,:usua_tipocuenta, :usua_idcuenta)}", nativeQuery = true)
        List<Map<String, Object>> get_usuario_login( @Param("usua_email") String usua_email,
        @Param("usua_tipo") Integer usua_tipo,
        @Param("usua_clave") String usua_clave,@Param("usua_tipocuenta") Integer usua_tipocuenta,
                                                     @Param("usua_idcuenta") String usua_idcuenta);

        @Query(value= "{call dbo.sp_add_usuario2(:usua_dni,:usua_nombres,:usua_apellpat,:usua_apellmat,:usua_celular,:usua_fijo," +
                ":usua_email,:usua_antecedentes,:usua_foto,:usua_dnifoto,:usua_direccion,:usua_tipo, :usua_clave, :usua_longitud,:usua_latitud, :usua_ubigeo,:usua_referencia, :usua_vivienda, :usua_tipodoc, :usua_tipocuenta, :usua_idcuenta)}", nativeQuery = true)
        void add_usuario(
                @Param("usua_dni") String usua_dni,
                @Param("usua_nombres") String usua_nombres,
                @Param("usua_apellpat") String usua_apellpat,
                @Param("usua_apellmat") String usua_apellmat,
                @Param("usua_celular") String usua_celular,
                @Param("usua_fijo") String usua_fijo,
                @Param("usua_email") String usua_email,
                @Param("usua_antecedentes") String usua_antecedentes,
                @Param("usua_foto") String usua_foto,
                @Param("usua_dnifoto") String usua_dnifoto,
                @Param("usua_direccion") String usua_direccion,
                @Param("usua_tipo") Integer usua_tipo,
                @Param("usua_clave") String usua_clave,
                @Param("usua_longitud") Double usua_longitud,
                @Param("usua_latitud") Double usua_latitud,
                @Param("usua_ubigeo") String usua_ubigeo,
                @Param("usua_referencia") String usua_referencia,
                @Param("usua_vivienda") Integer usua_vivienda,
                @Param("usua_tipodoc") Integer usua_tipodoc,
                @Param("usua_tipocuenta") Integer usua_tipocuenta,
                @Param("usua_idcuenta") String usua_idcuenta
        );

        @Query(value= "{call dbo.sp_ver_usuarioxid2(:usua_id)}", nativeQuery = true)
        Usuario get_userxid( @Param("usua_id") Integer usua_id
                                         );
        @Query(value= "{call dbo.sp_ver_miubicacionxid(:usua_id)}", nativeQuery = true)
        List<Map<String, Object>>  get_miubicacionxid( @Param("usua_id") Integer usua_id
        );


        @Query(value= "{call dbo.sp_update_usuario(:usua_id,:usua_dni,:usua_nombres,:usua_apellpat,:usua_apellmat,:usua_celular,:usua_fijo," +
                ":usua_email,:usua_antecedentes,:usua_foto,:usua_dnifoto,:usua_direccion,:usua_tipo, :usua_clave,:usua_longitud,:usua_latitud, :usua_ubigeo, :usua_referencia, :usua_vivienda, :usua_tipodoc)}", nativeQuery = true)
        void update_usuario(
                @Param("usua_id") Long usua_id,
                @Param("usua_dni") String usua_dni,
                @Param("usua_nombres") String usua_nombres,
                @Param("usua_apellpat") String usua_apellpat,
                @Param("usua_apellmat") String usua_apellmat,
                @Param("usua_celular") String usua_celular,
                @Param("usua_fijo") String usua_fijo,
                @Param("usua_email") String usua_email,
                @Param("usua_antecedentes") String usua_antecedentes,
                @Param("usua_foto") String usua_foto,
                @Param("usua_dnifoto") String usua_dnifoto,
                @Param("usua_direccion") String usua_direccion,
                @Param("usua_tipo") Integer usua_tipo,
                @Param("usua_clave") String usua_clave,
                @Param("usua_longitud") Double usua_longitud,
                @Param("usua_latitud") Double usua_latitud,
                @Param("usua_ubigeo") String usua_ubigeo,
                @Param("usua_referencia") String usua_referencia,
                @Param("usua_vivienda") Integer usua_vivienda,
                @Param("usua_tipodoc") Integer usua_tipodoc
        );

        @Query(value= "{call dbo.sp_recuperar_clave(:usua_email)}", nativeQuery = true)
        Usuario get_recuperar_clave( @Param("usua_email") String usua_email
                                         );

        @Query(value= "{call dbo.sp_existsxEmail(:usua_email)}", nativeQuery = true)
        boolean existsByEmail( @Param("usua_email") String usua_email);
        @Query(value= "{call dbo.sp_existsxEmail_google(:usua_email)}", nativeQuery = true)
        boolean existsxEmail_google( @Param("usua_email") String usua_email);
        @Query(value= "{call dbo.sp_existsxDNI(:usua_dni)}", nativeQuery = true)
        boolean existsByDNI( @Param("usua_dni") String usua_dni);


        @Query(value= "{call dbo.sp_validar_clave_usuario(:usua_id, :usua_clave)}", nativeQuery = true)
        boolean existsByClave( @Param("usua_id") Integer usua_id,@Param("usua_clave") String usua_clave);

        @Query(value= "{call dbo.sp_usuarios_cita(:espe_id,:soci_nivel,:soci_ubicacion,:soci_diaaten,:soci_horaaten)}", nativeQuery = true)
        List<Map<String, Object>>  get_tecnicos_x_especilidad(
                                                          @Param("espe_id") Integer espe_id,
                                                          @Param("soci_nivel") Integer soci_nivel,
                                                          @Param("soci_ubicacion") String soci_ubicacion,
                                                          @Param("soci_diaaten") String soci_diaaten,
                                                          @Param("soci_horaaten") String soci_horaaten
        );

        @Query(value= "{call dbo.sp_usuarios_cita2(:corr_id,:espe_id,:soci_nivel,:soci_ubicacion,:soci_diaaten,:soci_horaaten)}", nativeQuery = true)
        List<Map<String, Object>>  get_tecnicos_x_especilidad2(
                @Param("corr_id") Integer corr_id,
                @Param("espe_id") Integer espe_id,
                @Param("soci_nivel") Integer soci_nivel,
                @Param("soci_ubicacion") String soci_ubicacion,
                @Param("soci_diaaten") String soci_diaaten,
                @Param("soci_horaaten") String soci_horaaten
        );

        @Query(value= "{call dbo.sp_update_usuario_foto(:usua_dni,:usua_foto)}", nativeQuery = true)
        void update_usuario_foto( @Param("usua_dni") String usua_dni, @Param("usua_foto") String usua_foto);

        @Query(value= "{call dbo.sp_enviar_codigo_de_verficacion(:usua_id)}", nativeQuery = true)
        String enviar_codigo_de_verficacion( @Param("usua_id") Long usua_id);

        @Query(value= "{call dbo.sp_validar_codigo_recuperacion(:codigo)}", nativeQuery = true)
        Integer validar_codigo_recuperacion( @Param("codigo") String codigo);

        @Query(value= "{call dbo.sp_resetear_clave(:codigo,:clave)}", nativeQuery = true)
        Integer resetear_clave( @Param("codigo") String codigo,@Param("clave") String clave);
}
