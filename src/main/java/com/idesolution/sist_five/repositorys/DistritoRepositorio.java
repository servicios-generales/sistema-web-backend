package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.Distrito;
import com.idesolution.sist_five.models.dist_json;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Map;

@Repository
public interface DistritoRepositorio extends JpaRepository<Distrito,Long> {
    @Query(value= "{call dbo.sp_list_distritos(:prov_id)}", nativeQuery = true)
    List<Distrito> get_distritos(@Param("prov_id") Integer prov_id);

    @Query(value= "{call dbo.sp_obtener_ubigeo_nombre(:ubigeo)}", nativeQuery = true)
    List<Map<String, Object>> get_ubigeo_nombre(@Param("ubigeo") String ubigeo);


    //@Query(value= "{call dbo.sp_obtener_ubigeo_nombre(:ubigeo)}", nativeQuery = true)
    //JSONArray get_ubigeo_nombre(@Param("ubigeo") String ubigeo);

    @Query(value= "{call dbo.sp_obtener_ubigeo_ids(:ubigeo)}", nativeQuery = true)
    String get_ubigeo_ids(@Param("ubigeo") String ubigeo);
}
