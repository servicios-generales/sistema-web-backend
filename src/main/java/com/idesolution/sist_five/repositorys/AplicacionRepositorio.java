package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.Aplicacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface AplicacionRepositorio extends JpaRepository<Aplicacion,Long> {
    @Query(value= "{call dbo.sp_obtener_version_app()}", nativeQuery = true)
    String validar_version( );
}
