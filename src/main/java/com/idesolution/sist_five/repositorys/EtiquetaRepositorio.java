package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.Etiqueta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface EtiquetaRepositorio extends JpaRepository<Etiqueta,Long> {
    @Query(value= "{call dbo.sp_list_etiquetas_xid(:usua_id)}", nativeQuery = true)
    List<Map<String,Object>> get_list_etiquetas_xid(@Param("usua_id") Integer usua_id);

    @Query(value= "{call dbo.sp_detalle_etiqueta_xid(:etiq_id)}", nativeQuery = true)
    List<Etiqueta> get_detalle_etiqueta_xid(@Param("etiq_id") Integer usua_id);
}
