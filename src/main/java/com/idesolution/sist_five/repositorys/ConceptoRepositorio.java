package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.Concepto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface ConceptoRepositorio extends JpaRepository<Concepto,Long> {
    @Query(value= "{call dbo.sp_list_conceptos(:conc_prefijo)}", nativeQuery = true)
    List<Concepto> get_conceptos(@Param("conc_prefijo") Integer conc_prefijo);

    @Query(value= "{call dbo.sp_list_terminos_condiciones()}", nativeQuery = true)
    List<Map<String,Object>> get_terminos_condiciones();

    @Query(value= "{call dbo.sp_list_medios_pago()}", nativeQuery = true)
    List<Concepto> get_medios_pago();

    @Query(value= "{call dbo.sp_list_medios_pago_tipos(:conc_correlativo)}", nativeQuery = true)
    List<Map<String,Object>> get_medios_pago_tipos(@Param("conc_correlativo") Integer conc_correlativo);

    @Query(value= "{call dbo.sp_list_medios_pago_tipos_detalle(:conc_correlativo,:para_int1)}", nativeQuery = true)
    List<Map<String,Object>> get_detalle_pago_tipos(@Param("conc_correlativo") Integer conc_correlativo,
                                                    @Param("para_int1") Integer para_int1);

    @Query(value= "{call dbo.sp_nota_visita_tecnico()}", nativeQuery = true)
    List<Concepto> get_nota_visita_tecnico();

    @Query(value= "{call dbo.sp_list_conceptos_leyenda(:conc_prefijo)}", nativeQuery = true)
    List<Map<String,Object>> get_conceptos_leyenda(@Param("conc_prefijo") Integer conc_prefijo);

    @Query(value= "{call dbo.sp_list_conceptos_cabecera()}", nativeQuery = true)
    List<Map<String,Object>> get_conceptos_cabecera();

    @Query(value= "{call dbo.sp_add_concepto(:conc_prefijo , :conc_correlativo ,:conc_descripcion, :conc_abreviatura)}", nativeQuery = true)
    void add_concepto(
            @Param("conc_prefijo") Integer conc_prefijo,
            @Param("conc_correlativo") Integer conc_correlativo,
            @Param("conc_descripcion") String conc_descripcion,
            @Param("conc_abreviatura") String conc_abreviatura
    );

    @Query(value= "{call dbo.sp_update_concepto(:conc_id,:conc_prefijo , :conc_correlativo ,:conc_descripcion, :conc_abreviatura)}", nativeQuery = true)
    void update_concepto(
            @Param("conc_id") Long conc_id,
            @Param("conc_prefijo") Integer conc_prefijo,
            @Param("conc_correlativo") Integer conc_correlativo,
            @Param("conc_descripcion") String conc_descripcion,
            @Param("conc_abreviatura") String conc_abreviatura
    );

    @Query(value= "{call dbo.sp_concepto_exists(:conc_descripcion)}", nativeQuery = true)
    boolean existsByConc_descripcion( @Param("conc_descripcion") String conc_descripcion);

    @Query(value= "{call dbo.sp_delete_concepto(:conc_id)}", nativeQuery = true)
    void delete_concepto(
            @Param("conc_id") Integer conc_id
    );
}
