package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.Especialidad;
import com.idesolution.sist_five.models.Servicio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Map;

@Repository

public interface EspecialidadRepositorio extends JpaRepository<Especialidad,Long> {
    @Query(value= "{call sp_listar_especialidad_x_servicio(:serv_id)}", nativeQuery = true)
    List<Especialidad> get_especialidades_x_servicio(@Param("serv_id") Integer serv_id);

    @Query(value= "{call sp_listar_especialidad_x_servicio_mantenimiento(:serv_id)}", nativeQuery = true)
    List<Especialidad> get_especialidades_x_servicio_mantenimiento(@Param("serv_id") Integer serv_id);

    @Query(value= "{call sp_listar_especialidad_x_usuario(:usua_id)}", nativeQuery = true)
    List<Especialidad> listar_especialidad_x_usuario(@Param("usua_id") Integer usua_id);

    @Query(value= "{call sp_listar_especialidades()}", nativeQuery = true)
    List<Map<String, Object>>  get_listar_especialidades();


    @Query(value= "{call dbo.sp_list_especialidad_x_id(:espe_id)}", nativeQuery = true)
    List<Especialidad>  get_especialidad_x_id(@Param("espe_id") Integer espe_id
    );

    @Query(value= "{call dbo.sp_update_especialidad_x_id(:espe_id, :espe_nombre , :espe_descripcion , :espe_imagen , :serv_id)}", nativeQuery = true)
    void update_especialidad_x_id(
            @Param("espe_id") Long espe_id,
            @Param("espe_nombre") String espe_nombre,
            @Param("espe_descripcion") String espe_descripcion,
            @Param("espe_imagen") String espe_imagen,
            @Param("serv_id") Integer serv_id
    );

    @Query(value= "{call dbo.sp_add_especialidad(:espe_nombre , :espe_descripcion , :espe_imagen , :serv_id)}", nativeQuery = true)
    Long add_especialidad(
            @Param("espe_nombre") String espe_nombre,
            @Param("espe_descripcion") String espe_descripcion,
            @Param("espe_imagen") String espe_imagen,
            @Param("serv_id") Integer serv_id
    );

    @Query(value= "{call dbo.sp_especialidad_exists(:espe_nombre)}", nativeQuery = true)
    boolean existsByEspe_nombre( @Param("espe_nombre") String espe_nombre);

    @Query(value= "{call dbo.sp_delete_especialidad(:espe_id)}", nativeQuery = true)
    void delete_especialidad(
            @Param("espe_id") Integer espe_id
    );
}
