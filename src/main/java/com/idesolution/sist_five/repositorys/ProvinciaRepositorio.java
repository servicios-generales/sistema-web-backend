package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.Provincia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository

public interface ProvinciaRepositorio extends JpaRepository<Provincia,Long> {

    @Query(value= "{call dbo.sp_list_provincias(:depa_id)}", nativeQuery = true)
    List<Provincia> get_provincias(@Param("depa_id") Integer depa_id);
}
