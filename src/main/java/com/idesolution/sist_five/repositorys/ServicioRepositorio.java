package com.idesolution.sist_five.repositorys;

import com.idesolution.sist_five.models.Servicio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface ServicioRepositorio extends JpaRepository<Servicio,Long> {

    @Query(value= "{call sp_listar_all_servicios}", nativeQuery = true)
    List<Servicio> get_all_servicios();

    @Query(value= "{call sp_listar_all_servicios_mantenimiento}", nativeQuery = true)
    List<Servicio> get_all_servicios_mantenimiento();

    @Query(value= "{call dbo.sp_list_servicio_x_id(:serv_id)}", nativeQuery = true)
    List<Servicio>  get_servicio_x_id(@Param("serv_id") Integer serv_id
    );

    @Query(value= "{call dbo.sp_update_servicio_x_id(:serv_id, :serv_nombre , :serv_descripcion , :serv_imagen )}", nativeQuery = true)
    void update_servicio_x_id(
            @Param("serv_id") Long serv_id,
            @Param("serv_nombre") String serv_nombre,
            @Param("serv_descripcion") String serv_descripcion,
            @Param("serv_imagen") String serv_imagen
    );

    @Query(value= "{call dbo.sp_add_servicio(:serv_nombre , :serv_descripcion , :serv_imagen )}", nativeQuery = true)
    Long add_servicio(
            @Param("serv_nombre") String serv_nombre,
            @Param("serv_descripcion") String serv_descripcion,
            @Param("serv_imagen") String serv_imagen
    );



    @Query(value= "{call dbo.sp_servicio_exists(:serv_nombre)}", nativeQuery = true)
    boolean existsByServ_nombre( @Param("serv_nombre") String serv_nombre);

    @Query(value= "{call dbo.sp_delete_servicio(:serv_id)}", nativeQuery = true)
    void delete_servicio(
            @Param("serv_id") Integer serv_id
    );

}

