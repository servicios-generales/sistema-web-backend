package com.idesolution.sist_five;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
@SpringBootApplication
@EnableScheduling
public class SistFiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistFiveApplication.class, args);
	}

}
