package com.idesolution.sist_five.dto;

import java.util.Date;

public class ParametroA {
    private Long para_id;
    private Integer para_prefijo;
    private Integer para_correlativo;
    private Integer para_int1;
    private Integer para_int2;
    private String para_cadena1;
    private String para_cadena2;
    private Date para_fecha1;
    private Date para_fecha2;

    public Long getPara_id() {
        return para_id;
    }

    public void setPara_id(Long para_id) {
        this.para_id = para_id;
    }

    public Integer getPara_prefijo() {
        return para_prefijo;
    }

    public void setPara_prefijo(Integer para_prefijo) {
        this.para_prefijo = para_prefijo;
    }

    public Integer getPara_correlativo() {
        return para_correlativo;
    }

    public void setPara_correlativo(Integer para_correlativo) {
        this.para_correlativo = para_correlativo;
    }

    public Integer getPara_int1() {
        return para_int1;
    }

    public void setPara_int1(Integer para_int1) {
        this.para_int1 = para_int1;
    }

    public Integer getPara_int2() {
        return para_int2;
    }

    public void setPara_int2(Integer para_int2) {
        this.para_int2 = para_int2;
    }

    public String getPara_cadena1() {
        return para_cadena1;
    }

    public void setPara_cadena1(String para_cadena1) {
        this.para_cadena1 = para_cadena1;
    }

    public String getPara_cadena2() {
        return para_cadena2;
    }

    public void setPara_cadena2(String para_cadena2) {
        this.para_cadena2 = para_cadena2;
    }

    public Date getPara_fecha1() {
        return para_fecha1;
    }

    public void setPara_fecha1(Date para_fecha1) {
        this.para_fecha1 = para_fecha1;
    }

    public Date getPara_fecha2() {
        return para_fecha2;
    }

    public void setPara_fecha2(Date para_fecha2) {
        this.para_fecha2 = para_fecha2;
    }
}
