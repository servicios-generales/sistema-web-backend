package com.idesolution.sist_five.dto;

public class Proforma {
    private Integer soci_id;
    private Integer usua_id;
    private Integer cantidad;
    private String detalle_prof;
    private Double importe_prof;
    private Double total;

    public Integer getSoci_id() {
        return soci_id;
    }

    public void setSoci_id(Integer soci_id) {
        this.soci_id = soci_id;
    }

    public Integer getUsua_id() {
        return usua_id;
    }

    public void setUsua_id(Integer usua_id) {
        this.usua_id = usua_id;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getDetalle_prof() {
        return detalle_prof;
    }

    public void setDetalle_prof(String detalle_prof) {
        this.detalle_prof = detalle_prof;
    }

    public Double getImporte_prof() {
        return importe_prof;
    }

    public void setImporte_prof(Double importe_prof) {
        this.importe_prof = importe_prof;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
