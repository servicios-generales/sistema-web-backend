package com.idesolution.sist_five.dto;

public class Pago {
    private Integer usua_id;
    private Integer soci_id;
    private Integer soci_tipo;
    private Integer soci_estado;
    private Double soci_costo;
    private Integer conc_correlativo;
    private Integer para_int1;

    public Integer getUsua_id() {
        return usua_id;
    }

    public void setUsua_id(Integer usua_id) {
        this.usua_id = usua_id;
    }

    public Integer getSoci_id() {
        return soci_id;
    }

    public void setSoci_id(Integer soci_id) {
        this.soci_id = soci_id;
    }

    public Integer getSoci_tipo() {
        return soci_tipo;
    }

    public void setSoci_tipo(Integer soci_tipo) {
        this.soci_tipo = soci_tipo;
    }

    public Integer getSoci_estado() {
        return soci_estado;
    }

    public void setSoci_estado(Integer soci_estado) {
        this.soci_estado = soci_estado;
    }

    public Double getSoci_costo() {
        return soci_costo;
    }

    public void setSoci_costo(Double soci_costo) {
        this.soci_costo = soci_costo;
    }

    public Integer getConc_correlativo() {
        return conc_correlativo;
    }

    public void setConc_correlativo(Integer conc_correlativo) {
        this.conc_correlativo = conc_correlativo;
    }

    public Integer getPara_int1() {
        return para_int1;
    }

    public void setPara_int1(Integer para_int1) {
        this.para_int1 = para_int1;
    }
}
