package com.idesolution.sist_five.dto;

import javax.persistence.*;

@Entity
@Table(name="tb_log_error")
public class log_error {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long loge_id;
    private String loge_webmethod;
    private String loge_error;

    public Long getLoge_id() {
        return loge_id;
    }

    public void setLoge_id(Long loge_id) {
        this.loge_id = loge_id;
    }

    public String getLoge_webmethod() {
        return loge_webmethod;
    }

    public void setLoge_webmethod(String loge_webmethod) {
        this.loge_webmethod = loge_webmethod;
    }

    public String getLoge_error() {
        return loge_error;
    }

    public void setLoge_error(String loge_error) {
        this.loge_error = loge_error;
    }
}
