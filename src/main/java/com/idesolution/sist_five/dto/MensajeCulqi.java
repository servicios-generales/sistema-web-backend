package com.idesolution.sist_five.dto;

public class MensajeCulqi {
    private String tipo;

    private String mensaje;

    public java.lang.String getTipo() {
        return tipo;
    }

    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }

    public java.lang.String getMensaje() {
        return mensaje;
    }

    public void setMensaje(java.lang.String mensaje) {
        this.mensaje = mensaje;
    }

    public MensajeCulqi(java.lang.String tipo, java.lang.String mensaje) {
        this.tipo = tipo;
        this.mensaje = mensaje;
    }
}
