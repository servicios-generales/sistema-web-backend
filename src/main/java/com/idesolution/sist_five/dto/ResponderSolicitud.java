package com.idesolution.sist_five.dto;

public class ResponderSolicitud {
    private Integer usua_id;
    private Integer soci_id;
    private Double soci_costo;

    public Integer getUsua_id() {
        return usua_id;
    }

    public void setUsua_id(Integer usua_id) {
        this.usua_id = usua_id;
    }

    public Integer getSoci_id() {
        return soci_id;
    }

    public void setSoci_id(Integer soci_id) {
        this.soci_id = soci_id;
    }

    public Double getSoci_costo() {
        return soci_costo;
    }

    public void setSoci_costo(Double soci_costo) {
        this.soci_costo = soci_costo;
    }
}
