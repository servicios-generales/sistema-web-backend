package com.idesolution.sist_five.dto;

import org.springframework.web.multipart.MultipartFile;

public class FotoUsuario {
    private String keyname;
    private MultipartFile file;

    public String getKeyname() {
        return keyname;
    }

    public void setKeyname(String keyname) {
        this.keyname = keyname;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
