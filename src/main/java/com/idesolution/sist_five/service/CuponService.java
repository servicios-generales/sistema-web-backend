package com.idesolution.sist_five.service;

import com.idesolution.sist_five.models.Cupon;
import com.idesolution.sist_five.repositorys.CuponRepositorio;
import com.idesolution.sist_five.repositorys.CuponRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CuponService {
    @Autowired
    CuponRepositorio cuponRepositorio;
  
    /*public List<Cupon> listar_cupon_x_usuario(Integer usua_id){
        return cuponRepositorio.listar_cupon_x_usuario(usua_id);
    }*/
    public List<Map<String, Object>> get_listar_cupones(){
        return cuponRepositorio.get_listar_cupones();
    }


    public List<Cupon> list_cupon_x_id(Integer cupo_id){
        return cuponRepositorio.get_cupon_x_id(cupo_id);
    }

    public void update_cupon(Cupon objc){
        cuponRepositorio.update_cupon_x_id(objc.getCupo_id(),objc.getCupo_codigo(),objc.getCupo_tipoAplica(), objc.getCupo_categoria() , objc.getCupo_tipoDcto() , objc.getCupo_importeDcto() , objc.getCupo_fechaCaduca(), objc.getCupo_fechaInicio(), objc.getCupo_limitexCupon(), objc.getCupo_limitexCategoria(), objc.getCupo_limitexUsuario(), objc.getCupo_tipoDestino());
    }

    public void add_cupon(Cupon objc){
        cuponRepositorio.add_cupon(objc.getCupo_codigo(),objc.getCupo_tipoAplica(), objc.getCupo_categoria() , objc.getCupo_tipoDcto() , objc.getCupo_importeDcto() , objc.getCupo_fechaCaduca(), objc.getCupo_fechaInicio(), objc.getCupo_limitexCupon(), objc.getCupo_limitexCategoria(), objc.getCupo_limitexUsuario(), objc.getCupo_tipoDestino());
    }

    public void delete_cupon(Integer cupo_id){
        cuponRepositorio.delete_cupon(cupo_id);
    }

    public Integer public_cupon(Integer cupo_id){
       return cuponRepositorio.public_cupon(cupo_id);
    }

    public void active_cupon(Integer cupo_id){
        cuponRepositorio.active_cupon(cupo_id);
    }

    public List<Map<String, Object>> aplicar_cupon_visita(Integer corr_id,Integer soci_id,String codigo){
        return cuponRepositorio.aplicar_cupon_visita(corr_id,soci_id,codigo);
    }

    public boolean existsByCupo_codigo(String cupo_codigo){
        return cuponRepositorio.existsByCupo_codigo(cupo_codigo);
    }

    public  void caducar_cupones(){
        cuponRepositorio.caducar_cupones();

    }

    public List<Map<String, Object>> aplicar_cupon_proforma(Integer corr_id,Integer soci_id,String codigo){
        return cuponRepositorio.aplicar_cupon_proforma(corr_id,soci_id,codigo);
    }
}
