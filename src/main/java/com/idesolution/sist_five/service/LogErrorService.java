package com.idesolution.sist_five.service;

import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.Usuario;
import com.idesolution.sist_five.repositorys.LogErrorRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LogErrorService {
    @Autowired
    LogErrorRepositorio logErrorRepositorio;

    public void add_log_error(log_error obj){
        logErrorRepositorio.add_logerror(obj.getLoge_webmethod(),obj.getLoge_error());
    }
}
