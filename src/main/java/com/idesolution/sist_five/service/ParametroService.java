package com.idesolution.sist_five.service;

import com.idesolution.sist_five.dto.ParametroA;
import com.idesolution.sist_five.models.Opcion_menu;
import com.idesolution.sist_five.models.Parametro;
import com.idesolution.sist_five.models.Usuario;
import com.idesolution.sist_five.repositorys.ParametroRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ParametroService {
    @Autowired

    private ParametroRepositorio parametroRepositorio;

    public List<Map<String,Object>>  get_parametro(Integer conc_id){
        return parametroRepositorio.get_parametro(conc_id);
    }

    public List<Map<String,Object>>  get_fechactual(){
        return parametroRepositorio.sp_get_fechaactual();
    }
    public List<Map<String,Object>>  get_list_parametro(Integer para_prefijo){
        return parametroRepositorio.get_list_parametros(para_prefijo);
    }

    public List<Map<String,Object>> get_parametros_x_prefijo(Integer para_prefijo){
        return parametroRepositorio.get_parametros_x_prefijo(para_prefijo);
    }

    public List<Map<String,Object>> get_parametros_todos(){
        return parametroRepositorio.get_parametros_todos();
    }

    public List<Map<String,Object>> get_parametro_x_prefijoycorrelativo(Integer para_prefijo, Integer para_correlativo){
        return parametroRepositorio.get_parametro_x_prefijoycorrelativo(para_prefijo, para_correlativo);
    }

    public void update_parametro_x_id(ParametroA objp){
        parametroRepositorio.update_parametro_x_id(objp.getPara_id(), objp.getPara_prefijo(), objp.getPara_correlativo(), objp.getPara_int1(), objp.getPara_int2(), objp.getPara_cadena1(), objp.getPara_cadena2(),objp.getPara_fecha1(),objp.getPara_fecha2());
    }

    public void add_parametro(ParametroA objp){
        parametroRepositorio.add_parametro( objp.getPara_prefijo(), objp.getPara_correlativo(), objp.getPara_int1(), objp.getPara_int2(), objp.getPara_cadena1(), objp.getPara_cadena2(),objp.getPara_fecha1(),objp.getPara_fecha2());
    }

    public boolean existsByPara_cadena1(String para_cadena1){
        return parametroRepositorio.existsByPara_cadena1(para_cadena1);
    }

    public void delete_parametro(Integer para_id){
        parametroRepositorio.delete_parametro(para_id);
    }
}
