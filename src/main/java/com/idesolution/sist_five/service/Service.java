package com.idesolution.sist_five.service;

import com.idesolution.sist_five.models.SmsRequest;
import com.idesolution.sist_five.repositorys.SmsSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@org.springframework.stereotype.Service
public class Service {
    private final SmsSender smsSender;

    @Autowired
    public Service(@Qualifier("twilio") TwilioSmsSender smsSender){
        this.smsSender=smsSender;
    }
    public  void sendSms(SmsRequest smsRequest){
        smsSender.sendSms(smsRequest);
    }
}
