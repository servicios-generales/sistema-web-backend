package com.idesolution.sist_five.service;

import com.idesolution.sist_five.models.Especialidad;
import com.idesolution.sist_five.models.Servicio;
import com.idesolution.sist_five.repositorys.EspecialidadRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class EspecialidadService {
    @Autowired
    EspecialidadRepositorio especialidadRepositorio;
    public List<Especialidad> listar_especialidades_x_servicio(Integer serv_id){
        return especialidadRepositorio.get_especialidades_x_servicio(serv_id);
    }

    public List<Especialidad> listar_especialidades_x_servicio_mantenimiento(Integer serv_id){
        return especialidadRepositorio.get_especialidades_x_servicio_mantenimiento(serv_id);
    }

    public List<Especialidad> listar_especialidad_x_usuario(Integer usua_id){
        return especialidadRepositorio.listar_especialidad_x_usuario(usua_id);
    }
    public List<Map<String, Object>> get_listar_especialidades(){
        return especialidadRepositorio.get_listar_especialidades();
    }


    public List<Especialidad> list_especialidad_x_id(Integer espe_id){
        return especialidadRepositorio.get_especialidad_x_id(espe_id);
    }

    public void update_especialidad(Especialidad obje){
        especialidadRepositorio.update_especialidad_x_id(obje.getEspe_id(),obje.getEspe_nombre(),obje.getEspe_descripcion(), obje.getEspe_imagen(), obje.getServ_id());
    }

    public Long add_especialidad(Especialidad obje){
        return especialidadRepositorio.add_especialidad(obje.getEspe_nombre(),obje.getEspe_descripcion(),obje.getEspe_imagen(),obje.getServ_id());
    }

    public boolean existsByEspe_nombre(String espe_nombre){
        return especialidadRepositorio.existsByEspe_nombre(espe_nombre);
    }

    public void delete_especialidad(Integer espe_id){
        especialidadRepositorio.delete_especialidad(espe_id);
    }
}

