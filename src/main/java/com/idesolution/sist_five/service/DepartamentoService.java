package com.idesolution.sist_five.service;

import com.idesolution.sist_five.models.Departamento;
import com.idesolution.sist_five.repositorys.DepartamentoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Service
@Transactional
public class DepartamentoService {
    @Autowired
    private DepartamentoRepositorio departamentoRepositorio;
    public List<Departamento> get_departamentos(){
        return departamentoRepositorio.get_departamentos();
    }
}
