package com.idesolution.sist_five.service;

import com.idesolution.sist_five.dto.Mensaje;
import com.idesolution.sist_five.models.Calificacion;
import com.idesolution.sist_five.models.Concepto;
import com.idesolution.sist_five.repositorys.AplicacionRepositorio;
import com.idesolution.sist_five.repositorys.ConceptoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class AplicacionService {

    @Autowired
    private AplicacionRepositorio aplicacionRepositorio;
    public String validar_version(){
       return  aplicacionRepositorio.validar_version();
    }
}
