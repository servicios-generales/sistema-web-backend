package com.idesolution.sist_five.service;

import com.idesolution.sist_five.models.Concepto;
import com.idesolution.sist_five.models.Etiqueta;
import com.idesolution.sist_five.repositorys.ConceptoRepositorio;
import com.idesolution.sist_five.repositorys.EtiquetaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class EtiquetaService {

    @Autowired
    private EtiquetaRepositorio etiquetaRepositorio;
    public List<Map<String,Object>> get_list_etiquetas_xid(Integer usua_id){ return  etiquetaRepositorio.get_list_etiquetas_xid(usua_id);}

    public List<Etiqueta> get_detalle_etiqueta_xid(Integer etiq_id){ return  etiquetaRepositorio.get_detalle_etiqueta_xid(etiq_id);}

}
