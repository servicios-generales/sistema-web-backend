package com.idesolution.sist_five.service;

import com.idesolution.sist_five.models.Distrito;
import com.idesolution.sist_five.models.dist_json;
import com.idesolution.sist_five.repositorys.DistritoRepositorio;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class DistritoService {
    @Autowired
    private DistritoRepositorio distritoRepositorio;


    public List<Distrito> get_distritos(Integer prov_id){
        return distritoRepositorio.get_distritos(prov_id);
    }

    public List<Map<String, Object>> get_ubigeo_nombre(String ubigeo){
        return distritoRepositorio.get_ubigeo_nombre(ubigeo);
    }

    //MARIA
    //public JSONArray get_ubigeo_nombre(String ubigeo){
      //  return distritoRepositorio.get_ubigeo_nombre(ubigeo);
    //}

    public String get_ubigeo_ids(String ubigeo){
        return distritoRepositorio.get_ubigeo_ids(ubigeo);
    }
}
