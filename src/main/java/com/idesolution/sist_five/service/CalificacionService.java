package com.idesolution.sist_five.service;

import com.idesolution.sist_five.models.Calificacion;
import com.idesolution.sist_five.repositorys.CalificacionRepositorio;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
@Service
@Transactional
public class CalificacionService {

    @Autowired
    CalificacionRepositorio calificacionRepositorio;
    public void add_calificacion(Calificacion sc){
        calificacionRepositorio.add_calificacion(sc.getCliente_id(),sc.getEspecialista_id(),sc.getCali_opinion(),sc.getCali_rpta(),sc.getCali_puntaje(), sc.getSoci_id()
        );
    }

    public List<Map<String, Object>> listar_tecnicos_calificar(Integer usua_id){
        return calificacionRepositorio.listar_tecnicos_calificar(usua_id);
    }

    public List<Map<String, Object>> listar_tecnicos_calificar_xid(Integer soci_id){
        return calificacionRepositorio.listar_tecnicos_calificar_xid(soci_id);
    }
    public List<Map<String, Object>> guardar_tecnico_a_calificar(Integer usuario,Integer especialista,Integer soci_id){
        return calificacionRepositorio.guardar_tecnico_a_calificar(usuario,especialista,soci_id);
    }
    public List<Map<String, Object>> get_informacion_tecnico(Integer usua_id){
        return calificacionRepositorio.informacion_tecnico(usua_id);
    }
    public List<Map<String, Object>> get_comentarios_tecnico(Integer usua_id){
        return calificacionRepositorio.comentarios_tecnico(usua_id);
    }
}

