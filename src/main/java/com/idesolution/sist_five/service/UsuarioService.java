package com.idesolution.sist_five.service;

import com.idesolution.sist_five.models.Usuario;
import com.idesolution.sist_five.repositorys.UsuarioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service
@Transactional
public class UsuarioService {
    @Autowired
    UsuarioRepositorio usuarioRepositorio;

    @Autowired
    private JavaMailSender javaMailSender;

    public List<Map<String, Object>> get_usuario_login(String usua_email, Integer usua_tipo,String usua_clave ,Integer usua_tipocuenta,String usua_idcuenta ){
       return usuarioRepositorio.get_usuario_login(usua_email, usua_tipo, usua_clave ,usua_tipocuenta,usua_idcuenta);
    }

    public void add_usuarios(Usuario usuario){
        usuarioRepositorio.add_usuario(usuario.getUsua_dni(),usuario.getUsua_nombres(),
                usuario.getUsua_apellpat(),usuario.getUsua_apellmat(),usuario.getUsua_celular(),usuario.getUsua_fijo(),
                usuario.getUsua_email(),usuario.getUsua_antecedentes(), usuario.getUsua_foto(), usuario.getUsua_dnifoto(),
                usuario.getUsua_direccion(),usuario.getUsua_tipo(), usuario.getUsua_clave(), usuario.getUsua_longitud(),usuario.getUsua_latitud(),usuario.getUsua_ubigeo(),usuario.getUsua_referencia(),usuario.getUsua_vivienda(),usuario.getUsua_tipodoc(),usuario.getUsua_tipocuenta(),usuario.getUsua_idcuenta());
    }

    public Usuario get_userxid(Integer usua_id){
        return usuarioRepositorio.get_userxid(usua_id);
    }
    public List<Map<String, Object>> get_miubicacion_xid(Integer usua_id){
        return usuarioRepositorio.get_miubicacionxid(usua_id);
    }

    public void update_usuario(Usuario usuario){
        usuarioRepositorio.update_usuario(usuario.getUsua_id(),usuario.getUsua_dni(),usuario.getUsua_nombres(),
                usuario.getUsua_apellpat(),usuario.getUsua_apellmat(),usuario.getUsua_celular(),usuario.getUsua_fijo(),
                usuario.getUsua_email(),usuario.getUsua_antecedentes(), usuario.getUsua_foto(), usuario.getUsua_dnifoto(),
                usuario.getUsua_direccion(),usuario.getUsua_tipo(), usuario.getUsua_clave(), usuario.getUsua_longitud(),usuario.getUsua_latitud(),usuario.getUsua_ubigeo(),usuario.getUsua_referencia(),usuario.getUsua_vivienda(),usuario.getUsua_tipodoc());
    }

    public Usuario recuperar_clave(String usua_email){
        return usuarioRepositorio.get_recuperar_clave(usua_email);
    }

    public boolean existsByEmail(String usua_email){
        return usuarioRepositorio.existsByEmail(usua_email);
    }
    public boolean existsxEmail_google(String usua_email){
        return usuarioRepositorio.existsxEmail_google(usua_email);
    }

    public boolean existsByDNI(String usua_dni){
        return usuarioRepositorio.existsByDNI(usua_dni);
    }
    public boolean existsByClave(Integer usua_id, String usua_clave){
        return usuarioRepositorio.existsByClave(usua_id,  usua_clave);
    }


    public void sendEmail(String from, String to, String subject, String body){

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(from);
        mailMessage.setTo(to);
        mailMessage.setSubject(subject);
        mailMessage.setText(body);

        javaMailSender.send(mailMessage);

    }

    public List<Map<String, Object>>  get_tecnicos_x_especilidad(Integer espe_id, Integer soci_nivel, String soci_ubicacion, String soci_diaaten, String soci_horaaten){
        return usuarioRepositorio.get_tecnicos_x_especilidad( espe_id, soci_nivel, soci_ubicacion, soci_diaaten, soci_horaaten);
    }
    public List<Map<String, Object>>  get_tecnicos_x_especilidad2(Integer corr_id,Integer espe_id, Integer soci_nivel, String soci_ubicacion, String soci_diaaten, String soci_horaaten){
        return usuarioRepositorio.get_tecnicos_x_especilidad2( corr_id,espe_id, soci_nivel, soci_ubicacion, soci_diaaten, soci_horaaten);
    }

    public void update_usuario_foto(String usua_dni, String usua_foto){
        usuarioRepositorio.update_usuario_foto(usua_dni,usua_foto);
    }

    public String enviar_codigo_de_verficacion(Long usua_id){
        return usuarioRepositorio.enviar_codigo_de_verficacion(usua_id);
    }

    public Integer validar_codigo_recuperacion(String codigo){
        return usuarioRepositorio.validar_codigo_recuperacion(codigo);
    }

    public Integer resetear_clave(String codigo,String clave){
        return usuarioRepositorio.resetear_clave(codigo,clave);
    }
}
