package com.idesolution.sist_five.service;

import com.idesolution.sist_five.dto.ParametroA;
import com.idesolution.sist_five.models.Concepto;
import com.idesolution.sist_five.repositorys.ConceptoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ConceptoService {
    @Autowired
    private ConceptoRepositorio conceptoRepositorio;
    public List<Concepto> get_conceptos(Integer conc_prefijo){ return  conceptoRepositorio.get_conceptos(conc_prefijo);}

    public List<Map<String,Object>> get_terminos_condiciones(){ return  conceptoRepositorio.get_terminos_condiciones();}
    public List<Concepto> get_medios_pago(){ return  conceptoRepositorio.get_medios_pago();}
    public List<Map<String,Object>> get_medios_pago_tipos(Integer conc_correlativo){ return  conceptoRepositorio.get_medios_pago_tipos(conc_correlativo);}

    public List<Map<String,Object>> get_detalle_pago_tipos(Integer conc_correlativo, Integer para_int1){ return  conceptoRepositorio.get_detalle_pago_tipos(conc_correlativo,para_int1);}

    public List<Concepto> get_nota_visita_tecnico(){ return  conceptoRepositorio.get_nota_visita_tecnico();}
    public List<Map<String,Object>> get_conceptos_leyenda(Integer conc_prefijo){ return  conceptoRepositorio.get_conceptos_leyenda(conc_prefijo);}

    public List<Map<String,Object>> get_conceptos_cabecera(){ return  conceptoRepositorio.get_conceptos_cabecera();}

    public void update_concepto(Concepto objc){
        conceptoRepositorio.update_concepto(objc.getConc_id(),objc.getConc_prefijo(),objc.getConc_correlativo(),objc.getConc_descripcion(),objc.getConc_abreviatura());
    }

    public void add_concepto(Concepto objc){
        conceptoRepositorio.add_concepto(objc.getConc_prefijo(),objc.getConc_correlativo(),objc.getConc_descripcion(),objc.getConc_abreviatura());
    }

    public boolean existsByConc_descripcion(String conc_descripcion){
        return conceptoRepositorio.existsByConc_descripcion(conc_descripcion);
    }

    public void delete_concepto(Integer conc_id){
        conceptoRepositorio.delete_concepto(conc_id);
    }
}
