package com.idesolution.sist_five.service;

import com.idesolution.sist_five.models.Concepto;
import com.idesolution.sist_five.models.Servicio;
import com.idesolution.sist_five.repositorys.ServicioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Service
public class ServicioService {
    @Autowired
    private ServicioRepositorio servicioRepositorio;
    public List<Servicio> lista(){
        return servicioRepositorio.get_all_servicios();
    }

    public List<Servicio> lista_mantenimiento(){
        return servicioRepositorio.get_all_servicios_mantenimiento();
    }

    public List<Servicio> list_servicio_x_id(Integer serv_id){
        return servicioRepositorio.get_servicio_x_id(serv_id);
    }

    public void update_servicio(Servicio objs){
        servicioRepositorio.update_servicio_x_id(objs.getServ_id(),objs.getServ_nombre(),objs.getServ_descripcion(),objs.getServ_imagen());
    }

    public Long add_servicio(Servicio objs){
        return servicioRepositorio.add_servicio(objs.getServ_nombre(),objs.getServ_descripcion(),objs.getServ_imagen());
    }

    public boolean existsByServ_nombre(String serv_nombre){
        return servicioRepositorio.existsByServ_nombre(serv_nombre);
    }

    public void delete_servicio(Integer serv_id){
        servicioRepositorio.delete_servicio(serv_id);
    }
}
