package com.idesolution.sist_five.service;

import com.idesolution.sist_five.models.Provincia;
import com.idesolution.sist_five.repositorys.ProvinciaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Service
@Transactional
public class ProvinciaService {
    @Autowired
    private ProvinciaRepositorio provinciaRepositorio;

    public List<Provincia> get_provincias(Integer depa_id){
        return provinciaRepositorio.get_provincias(depa_id);
    }
}
