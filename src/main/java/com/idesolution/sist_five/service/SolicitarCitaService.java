package com.idesolution.sist_five.service;

import com.idesolution.sist_five.dto.ConfirmarSolicitud;
import com.idesolution.sist_five.dto.Pago;
import com.idesolution.sist_five.dto.PagoCupon;
import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.*;
import com.idesolution.sist_five.repositorys.SolicitudCitaReposiorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SolicitarCitaService {
    @Autowired
    SolicitudCitaReposiorio solicitudCitaReposiorio;

    public void solicitar_cita(SolicitudCitaEtiqueta sc){
        solicitudCitaReposiorio.solicitar_cita(sc.getUsua_id(),sc.getEspe_id(),sc.getSoci_nivel(),sc.getSoci_ubicacion(),sc.getSoci_diaaten(),sc.getSoci_horaaten(),sc.getSoci_detalle(),sc.getSoci_destino(),sc.getSoci_estado(), sc.getSoci_direccion(), sc.getSoci_latitud(),sc.getSoci_longitud(), sc.getSoci_instruccion(),sc.getSoci_etiqueta(),sc.getSoci_idetiqueta(),sc.getSoci_guardaetiqueta());
    }

    public void resolicitar_cita(SolicitudCitaEtiqueta sc){
        solicitudCitaReposiorio.resolicitar_cita(sc.getCorr_id(),sc.getUsua_id(),sc.getEspe_id(),sc.getSoci_nivel(),sc.getSoci_ubicacion(),sc.getSoci_diaaten(),sc.getSoci_horaaten(),sc.getSoci_detalle(),sc.getSoci_destino(),sc.getSoci_estado(), sc.getSoci_direccion(), sc.getSoci_latitud(),sc.getSoci_longitud(), sc.getSoci_instruccion(),sc.getSoci_idetiqueta());
    }

    public void solicitar_cita_multiple(SolicitudCitaEtiqueta sc){
        solicitudCitaReposiorio.solicitar_cita_multiple(sc.getUsua_id(),sc.getEspe_id(),sc.getSoci_nivel(),sc.getSoci_ubicacion(),sc.getSoci_diaaten(),sc.getSoci_horaaten(),sc.getSoci_detalle(),sc.getSoci_destino(),sc.getSoci_estado(), sc.getSoci_direccion(), sc.getSoci_latitud(),sc.getSoci_longitud(), sc.getSoci_instruccion(),sc.getSoci_etiqueta(),sc.getSoci_idetiqueta(),sc.getSoci_guardaetiqueta(),sc.getCorr_id());
    }

    public void resolicitar_cita_multiple(SolicitudCitaEtiqueta sc){
        solicitudCitaReposiorio.resolicitar_cita_multiple(sc.getCorr_id(),sc.getUsua_id(),sc.getEspe_id(),sc.getSoci_nivel(),sc.getSoci_ubicacion(),sc.getSoci_diaaten(),sc.getSoci_horaaten(),sc.getSoci_detalle(),sc.getSoci_destino(),sc.getSoci_estado(), sc.getSoci_direccion(), sc.getSoci_latitud(),sc.getSoci_longitud(), sc.getSoci_instruccion(),sc.getSoci_idetiqueta());
    }
    public List<Usuario> usuarios_cita(SolicitudCita sc){
        return solicitudCitaReposiorio.usuarios_cita(sc.getUsua_id(),sc.getEspe_id(),sc.getSoci_nivel(),sc.getSoci_ubicacion(),sc.getSoci_diaaten(),sc.getSoci_horaaten(),sc.getSoci_detalle(),sc.getSoci_destino(),sc.getSoci_estado());
    }

    public  List<Map<String,Object>> get_requerimientos(Integer usua_id, Integer tipo){
        return solicitudCitaReposiorio.get_requerimientos(usua_id, tipo);

    }

    public  List<Map<String,Object>> get_requerimientos_pendientes(Integer usua_id){
        return solicitudCitaReposiorio.get_requerimientos_pendientes(usua_id);

    }
   public  List<Map<String,Object>> get_solicitudes(Integer usua_id){
        return solicitudCitaReposiorio.get_solicitudes(usua_id);

    }
    public  List<Map<String,Object>> get_solicitudes_x_espeid(Integer usua_id, Integer espe_id){
        return solicitudCitaReposiorio.get_solicitudes_x_espeid(usua_id,espe_id);

    }

    public  List<Map<String,Object>> get_requerimiento_detalle(Integer soci_id){
        return solicitudCitaReposiorio.get_requerimiento_detalle(soci_id);

    }
    public  List<Map<String,Object>> get_solicitudes_x_estado(Integer usua_id, Integer esta_id){
        return solicitudCitaReposiorio.get_solicitudes_x_estado(usua_id,esta_id);

    }

    public void confirmar_solicitud(ConfirmarSolicitud cs){
       solicitudCitaReposiorio.confirmar_solicitud(cs.getUsua_id(),cs.getSoci_id(),cs.getSoci_tipo(),cs.getSoci_estado(),cs.getSoci_costo());
    }

    public void realizar_pago(PagoCupon p){
        solicitudCitaReposiorio.realizar_pago(p.getUsua_id(),p.getSoci_id(),p.getSoci_tipo(),p.getSoci_estado(),p.getSoci_costo(), p.getConc_correlativo(),p.getPara_int1(),p.getCorr_id(),p.getCupo_codigo());
    }

    public void realizar_pago_cotizacion(PagoCupon p){
        solicitudCitaReposiorio.realizar_pago_cotizacion(p.getUsua_id(),p.getSoci_id(),p.getSoci_tipo(),p.getSoci_estado(),p.getSoci_costo(), p.getConc_correlativo(),p.getPara_int1(),p.getCorr_id(),p.getCupo_codigo());
    }

    public void confirmar_pago_a_administrador(Integer usua_id,Integer soci_id, Integer tipo){
        solicitudCitaReposiorio.confirmar_pago_a_administrador(usua_id,soci_id,tipo);
    }

    public  List<Map<String,Object>> get_requerimiento_detalle_pendiente(Integer soci_id){
        return solicitudCitaReposiorio.get_requerimiento_detalle_pendiente(soci_id);

    }

    public  List<Map<String,Object>> get_requerimientos_confirmadas(Integer usua_id){
        return solicitudCitaReposiorio.get_requerimientos_confirmadas(usua_id);

    }
    public  List<Map<String,Object>> get_requerimiento_detalle_confirmada(Integer soci_id){
        return solicitudCitaReposiorio.get_requerimiento_detalle_confirmada(soci_id);

    }

    public Integer registrar_correlativo(Integer usua_id){
        return solicitudCitaReposiorio.registrar_correlativo(usua_id);
    }

    public  List<Map<String,Object>> get_solicitud_detalle_general(Integer corr_id){
        return solicitudCitaReposiorio.get_solicitud_detalle_general(corr_id);

    }
    public  List<Map<String,Object>> get_realizar_pago_solicitud(Integer corr_id,Integer soci_destino){
        return solicitudCitaReposiorio.get_realizar_pago_solicitud(corr_id,soci_destino);

    }

    public  List<Map<String,Object>> get_realizar_pago_proforma(Integer corr_id,Integer soci_destino){
        return solicitudCitaReposiorio.get_realizar_pago_proforma(corr_id,soci_destino);

    }

    public  List<Map<String,Object>> get_solicitud_detalle_especifico(Integer corr_id){
        return solicitudCitaReposiorio.get_solicitud_detalle_especifico(corr_id);

    }
    public void add_alerta(Integer correlativo, String fechasol, String horasol,String icono, String url,Integer usua_id){
        solicitudCitaReposiorio.add_alerta(correlativo,fechasol,horasol,icono,url,usua_id);
    }

    public  List<Map<String,Object>> list_alertas(Integer usua_id){
        return solicitudCitaReposiorio.list_alertas(usua_id);

    }
    public  List<Map<String,Object>> repetir_solicitud_cita(Integer corr_id){
        return solicitudCitaReposiorio.repetir_solicitud_cita(corr_id);

    }

    public void inactivar_solicitud(Integer corr_id){
        solicitudCitaReposiorio.inactivar_solicitud(corr_id);
    }

    public  List<Map<String,Object>> subir_comprobante(Integer corr_id){
        return solicitudCitaReposiorio.subir_comprobante(corr_id);

    }
    public Integer add_proforma(Integer usua_id, Integer soci_id, Double prof_total){
       return solicitudCitaReposiorio.add_proforma(usua_id,soci_id,prof_total);
    }

    public Integer update_proforma(Integer usua_id, Integer soci_id, Double prof_total){
        return solicitudCitaReposiorio.update_proforma(usua_id,soci_id,prof_total);
    }

    public void add_detalle_proforma(String depr_detalle,Double depr_importe, Integer depr_cantidad, Integer prof_id){
        solicitudCitaReposiorio.add_detalle_proforma(depr_detalle,depr_importe,depr_cantidad,prof_id);
    }

    public  List<Map<String,Object>> list_detalle_proforma(Integer soci_id){
        return solicitudCitaReposiorio.detalle_proforma(soci_id);

    }

    public  List<Map<String,Object>> list_detalle_proforma_especialista(Integer soci_id){
        return solicitudCitaReposiorio.detalle_proforma_especialista(soci_id);

    }

    public void confirmar_proforma(Integer usua_id , Integer soci_id){
        solicitudCitaReposiorio.confirmar_proforma(usua_id,soci_id);
    }

    public void rechazar_proforma(Integer usua_id , Integer soci_id){
        solicitudCitaReposiorio.rechazar_proforma(usua_id,soci_id);
    }

    public  void subir_imagen_trabajo(Integer soci_id, String nombre){
        solicitudCitaReposiorio.subir_imagen_trabajo(soci_id,nombre);
    }

    public  List<Map<String,Object>> list_imagen_trabajo(Integer soci_id){
        return solicitudCitaReposiorio.list_imagen_trabajo(soci_id);

    }

    public  void delete_imagen_trabajo(Integer soci_id){
        solicitudCitaReposiorio.delete_imagen_trabajo(soci_id);
    }

    public  void tarea_automatica_caducar(){
        solicitudCitaReposiorio.tarea_automatica_caducar();
    }

    public  List<Map<String,Object>>  validar_suscripcion(Integer usua_id, String suscripcion){
       return  solicitudCitaReposiorio.validar_suscripcion(usua_id, suscripcion);
    }

    public  List<Map<String,Object>>  inactivar_suscripcion(Integer usua_id, String suscripcion){
        return  solicitudCitaReposiorio.inactivar_suscripcion(usua_id, suscripcion);
    }

    public  List<Map<String,Object>> buscar_usuarios(){
        return solicitudCitaReposiorio.buscar_usuarios();

    }
    public  List<Map<String,Object>> listar_suscripciones(Integer usua_id){
        return solicitudCitaReposiorio.listar_suscripciones(usua_id);

    }
    public  List<Map<String,Object>> mostrar_notificacion_job(Integer usua_id,Integer estado){
        return solicitudCitaReposiorio.mostrar_notificacion_job(usua_id, estado);

    }

    public  List<Map<String,Object>> buscar_usuarios_clientes(){
        return solicitudCitaReposiorio.buscar_usuarios_clientes();

    }
    public  void caducar_solicitudes(Integer usua_id){
         solicitudCitaReposiorio.caducar_solicitudes(usua_id);

    }

    public  void reenviar_notificacion(){
        solicitudCitaReposiorio.reenviar_notificacion();

    }
    public  void reenviar_notificacion_urgente(){
        solicitudCitaReposiorio.reenviar_notificacion_urgente();

    }
    public  void reenviar_notificacion_normal(){
        solicitudCitaReposiorio.reenviar_notificacion_normal();

    }

    public  List<Map<String,Object>> monto_pago_visita(Integer soci_id,String codigo){
        return solicitudCitaReposiorio.monto_pago_visita(soci_id,codigo);

    }
    public  List<Map<String,Object>> monto_pago_cotizacio(Integer soci_id,String codigo){
        return solicitudCitaReposiorio.monto_pago_cotizacion(soci_id,codigo);

    }

    public Integer validar_confirmar_pago(Integer soci_id, Integer deso_tipo){
        return solicitudCitaReposiorio.validar_confirmar_pago(soci_id,deso_tipo);
    }

    public void cancelar_solicitud(Integer usua_id , Integer corr_id, Integer motivo){
        solicitudCitaReposiorio.cancelar_solicitud(usua_id,corr_id, motivo);
    }

    public  List<Map<String,Object>> get_requerimiento_detalle2(Integer soci_id){
        return solicitudCitaReposiorio.get_requerimiento_detalle2(soci_id);

    }
    public  String obtener_cobro_five(Integer soci_id){
        return solicitudCitaReposiorio.obtener_cobro_five(soci_id);

    }

    public  List<Map<String,Object>> obtener_cobro_five_largo(Integer soci_id){
        return solicitudCitaReposiorio.obtener_cobro_five_largo(soci_id);

    }

}
