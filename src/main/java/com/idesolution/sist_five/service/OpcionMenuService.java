package com.idesolution.sist_five.service;

import com.idesolution.sist_five.models.Opcion_menu;
import com.idesolution.sist_five.repositorys.OpcionMenuRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class OpcionMenuService {
    @Autowired
    OpcionMenuRepositorio opcionMenuRepositorio;

    public List<Map<String,Object>> get_opcionesxid(Integer usua_id, Integer opme_tipo){
        return opcionMenuRepositorio.get_opcionesxid(usua_id, opme_tipo);
    }


}
