package com.idesolution.sist_five.models;

import javax.persistence.*;

@Entity
@Table(name="tb_departamento")
public class Departamento {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long depa_id;
    private String depa_codigo;
    private String depa_nombre;
    private Integer depa_estado;


    public Long getDepa_id() {
        return depa_id;
    }

    public void setDepa_id(Long depa_id) {
        this.depa_id = depa_id;
    }

    public String getDepa_codigo() {
        return depa_codigo;
    }

    public void setDepa_codigo(String depa_codigo) {
        this.depa_codigo = depa_codigo;
    }

    public String getDepa_nombre() {
        return depa_nombre;
    }

    public void setDepa_nombre(String depa_nombre) {
        this.depa_nombre = depa_nombre;
    }

    public Integer getDepa_estado() {
        return depa_estado;
    }

    public void setDepa_estado(Integer depa_estado) {
        this.depa_estado = depa_estado;
    }
}
