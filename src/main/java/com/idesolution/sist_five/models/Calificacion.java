package com.idesolution.sist_five.models;
import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name="tb_calificacion")
public class Calificacion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long cali_id;
    private Integer cliente_id;
    private Integer especialista_id;
    private String cali_opinion;
    private Integer cali_puntaje;
    private Date cali_fechareg;
    private Integer cali_rpta;
    private Integer soci_id;


    public java.lang.Integer getCali_rpta() {
        return cali_rpta;
    }

    public void setCali_rpta(java.lang.Integer cali_rpta) {
        this.cali_rpta = cali_rpta;
    }

    public java.lang.Long getCali_id() {
        return cali_id;
    }

    public void setCali_id(java.lang.Long cali_id) {
        this.cali_id = cali_id;
    }

    public java.lang.Integer getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(java.lang.Integer cliente_id) {
        this.cliente_id = cliente_id;
    }

    public java.lang.Integer getEspecialista_id() {
        return especialista_id;
    }

    public void setEspecialista_id(java.lang.Integer especialista_id) {
        this.especialista_id = especialista_id;
    }

    public java.lang.String getCali_opinion() {
        return cali_opinion;
    }

    public void setCali_opinion(java.lang.String cali_opinion) {
        this.cali_opinion = cali_opinion;
    }

    public java.lang.Integer getCali_puntaje() {
        return cali_puntaje;
    }

    public void setCali_puntaje(java.lang.Integer cali_puntaje) {
        this.cali_puntaje = cali_puntaje;
    }

    public Date getCali_fechareg() {
        return cali_fechareg;
    }

    public void setCali_fechareg(Date cali_fechareg) {
        this.cali_fechareg = cali_fechareg;
    }

    public java.lang.Integer getSoci_id() {
        return soci_id;
    }

    public void setSoci_id(java.lang.Integer soci_id) {
        this.soci_id = soci_id;
    }
}
