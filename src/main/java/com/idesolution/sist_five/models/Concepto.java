package com.idesolution.sist_five.models;

import javax.persistence.*;

@Entity
@Table(name="tb_concepto")
public class Concepto {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long conc_id;
    private Integer conc_prefijo;
    private Integer conc_correlativo;
    private String conc_descripcion;
    private String conc_abreviatura;

    public Long getConc_id() {
        return conc_id;
    }

    public void setConc_id(Long conc_id) {
        this.conc_id = conc_id;
    }

    public Integer getConc_prefijo() {
        return conc_prefijo;
    }

    public void setConc_prefijo(Integer conc_prefijo) {
        this.conc_prefijo = conc_prefijo;
    }

    public Integer getConc_correlativo() {
        return conc_correlativo;
    }

    public void setConc_correlativo(Integer conc_correlativo) {
        this.conc_correlativo = conc_correlativo;
    }

    public String getConc_descripcion() {
        return conc_descripcion;
    }

    public void setConc_descripcion(String conc_descripcion) {
        this.conc_descripcion = conc_descripcion;
    }

    public String getConc_abreviatura() {
        return conc_abreviatura;
    }

    public void setConc_abreviatura(String conc_abreviatura) {
        this.conc_abreviatura = conc_abreviatura;
    }
}
