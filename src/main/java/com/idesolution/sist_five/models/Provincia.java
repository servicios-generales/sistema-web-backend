package com.idesolution.sist_five.models;

import javax.persistence.*;

@Entity
@Table(name="tb_provincia")
public class Provincia {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long prov_id;
    private String prov_codigo;
    private String prov_nombre;
    private Integer prov_estado;
    private Integer depa_id;


    public Long getProv_id() {
        return prov_id;
    }

    public void setProv_id(Long prov_id) {
        this.prov_id = prov_id;
    }

    public String getProv_codigo() {
        return prov_codigo;
    }

    public void setProv_codigo(String prov_codigo) {
        this.prov_codigo = prov_codigo;
    }

    public String getProv_nombre() {
        return prov_nombre;
    }

    public void setProv_nombre(String prov_nombre) {
        this.prov_nombre = prov_nombre;
    }

    public Integer getProv_estado() {
        return prov_estado;
    }

    public void setProv_estado(Integer prov_estado) {
        this.prov_estado = prov_estado;
    }

    public Integer getDepa_id() {
        return depa_id;
    }

    public void setDepa_id(Integer depa_id) {
        this.depa_id = depa_id;
    }
}
