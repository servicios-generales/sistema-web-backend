package com.idesolution.sist_five.models;

import javax.persistence.*;

@Entity
@Table(name="tb_opcion_menu")
public class Opcion_menu {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long opme_id;
    private String opme_denominacion;
    private String opme_icono;
    private String opme_ruta;
    private Integer opme_orden;
    private Integer opme_origen;
    private Integer opme_estado;
    private Integer apli_id;
    private Integer opme_tipo;

    public Long getOpme_id() {
        return opme_id;
    }

    public void setOpme_id(Long opme_id) {
        this.opme_id = opme_id;
    }

    public Integer getOpme_origen() {
        return opme_origen;
    }

    public void setOpme_origen(Integer opme_origen) {
        this.opme_origen = opme_origen;
    }

    public String getOpme_denominacion() {
        return opme_denominacion;
    }

    public void setOpme_denominacion(String opme_denominacion) {
        this.opme_denominacion = opme_denominacion;
    }

    public String getOpme_icono() {
        return opme_icono;
    }

    public void setOpme_icono(String opme_icono) {
        this.opme_icono = opme_icono;
    }

    public String getOpme_ruta() {
        return opme_ruta;
    }

    public void setOpme_ruta(String opme_ruta) {
        this.opme_ruta = opme_ruta;
    }

    public Integer getOpme_orden() {
        return opme_orden;
    }

    public void setOpme_orden(Integer opme_orden) {
        this.opme_orden = opme_orden;
    }

    public Integer getOpme_estado() {
        return opme_estado;
    }

    public void setOpme_estado(Integer opme_estado) {
        this.opme_estado = opme_estado;
    }

    public Integer getApli_id() {
        return apli_id;
    }

    public void setApli_id(Integer apli_id) {
        this.apli_id = apli_id;
    }

    public Integer getOpme_tipo() {
        return opme_tipo;
    }

    public void setOpme_tipo(Integer opme_tipo) {
        this.opme_tipo = opme_tipo;
    }

}
