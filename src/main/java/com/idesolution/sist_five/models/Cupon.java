package com.idesolution.sist_five.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="tb_cupon")
public class Cupon {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long cupo_id;
    private Integer cupo_tipoAplica;
    private Double cupo_importeMin;
    private Double cupo_importeMax;
    private String cupo_categoria;
    private Integer cupo_tipoDcto;
    private Double cupo_importeDcto;
    private Date cupo_fechaCaduca;
    private Date cupo_fechaInicio;
    private Date cupo_fechaCrea;
    private Integer cupo_limitexCupon;
    private Integer cupo_limitexCategoria;
    private Integer cupo_limitexUsuario;
    private Integer cupo_tipoDestino;
    private Integer cupo_estado;
    private String cupo_codigo;

    public Long getCupo_id() {
        return cupo_id;
    }

    public void setCupo_id(Long cupo_id) {
        this.cupo_id = cupo_id;
    }

    public Integer getCupo_tipoAplica() {
        return cupo_tipoAplica;
    }

    public void setCupo_tipoAplica(Integer cupo_tipoAplica) {
        this.cupo_tipoAplica = cupo_tipoAplica;
    }

    public Double getCupo_importeMin() {
        return cupo_importeMin;
    }

    public void setCupo_importeMin(Double cupo_importeMin) {
        this.cupo_importeMin = cupo_importeMin;
    }

    public Double getCupo_importeMax() {
        return cupo_importeMax;
    }

    public void setCupo_importeMax(Double cupo_importeMax) {
        this.cupo_importeMax = cupo_importeMax;
    }

    public String getCupo_categoria() {
        return cupo_categoria;
    }

    public void setCupo_categoria(String cupo_categoria) {
        this.cupo_categoria = cupo_categoria;
    }

    public Integer getCupo_tipoDcto() {
        return cupo_tipoDcto;
    }

    public void setCupo_tipoDcto(Integer cupo_tipoDcto) {
        this.cupo_tipoDcto = cupo_tipoDcto;
    }

    public Double getCupo_importeDcto() {
        return cupo_importeDcto;
    }

    public void setCupo_importeDcto(Double cupo_importeDcto) {
        this.cupo_importeDcto = cupo_importeDcto;
    }

    public Date getCupo_fechaCaduca() {
        return cupo_fechaCaduca;
    }

    public void setCupo_fechaCaduca(Date cupo_fechaCaduca) {
        this.cupo_fechaCaduca = cupo_fechaCaduca;
    }

    public Date getCupo_fechaInicio() {
        return cupo_fechaInicio;
    }

    public void setCupo_fechaInicio(Date cupo_fechaInicio) {
        this.cupo_fechaInicio = cupo_fechaInicio;
    }

    public Date getCupo_fechaCrea() {
        return cupo_fechaCrea;
    }

    public void setCupo_fechaCrea(Date cupo_fechaCrea) {
        this.cupo_fechaCrea = cupo_fechaCrea;
    }

    public Integer getCupo_limitexCupon() {
        return cupo_limitexCupon;
    }

    public void setCupo_limitexCupon(Integer cupo_limitexCupon) {
        this.cupo_limitexCupon = cupo_limitexCupon;
    }

    public Integer getCupo_limitexCategoria() {
        return cupo_limitexCategoria;
    }

    public void setCupo_limitexCategoria(Integer cupo_limitexCategoria) {
        this.cupo_limitexCategoria = cupo_limitexCategoria;
    }

    public Integer getCupo_limitexUsuario() {
        return cupo_limitexUsuario;
    }

    public void setCupo_limitexUsuario(Integer cupo_limitexUsuario) {
        this.cupo_limitexUsuario = cupo_limitexUsuario;
    }

    public Integer getCupo_tipoDestino() {
        return cupo_tipoDestino;
    }

    public void setCupo_tipoDestino(Integer cupo_tipoDestino) {
        this.cupo_tipoDestino = cupo_tipoDestino;
    }

    public Integer getCupo_estado() {
        return cupo_estado;
    }

    public void setCupo_estado(Integer cupo_estado) {
        this.cupo_estado = cupo_estado;
    }

    public String getCupo_codigo() {
        return cupo_codigo;
    }

    public void setCupo_codigo(String cupo_codigo) {
        this.cupo_codigo = cupo_codigo;
    }
}
