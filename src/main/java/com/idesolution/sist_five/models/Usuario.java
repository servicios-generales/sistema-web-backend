package com.idesolution.sist_five.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
@Entity
@Table(name="tb_usuario")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long usua_id;
    private String usua_dni;
    private String usua_nombres;
    private String usua_apellpat;
    private String usua_apellmat;
    private String usua_celular;
    private String usua_fijo;
    private String usua_email;
    private String usua_antecedentes;
    private String usua_foto;
    private String usua_dnifoto;
    private String usua_direccion;
    private Integer usua_tipo;
    private Integer usua_estado;
    private Date  usua_fechareg;
    private String usua_clave;
    private Double usua_longitud;
    private Double usua_latitud;
    private String usua_ubigeo;
    private String usua_referencia;
    private Integer usua_vivienda;
    private String usua_descripcion;
    private Integer usua_tipodoc;
    private String usua_idcuenta;
    private Integer usua_tipocuenta;



    public Long getUsua_id() {
        return usua_id;
    }

    public void setUsua_id(Long usua_id) {
        this.usua_id = usua_id;
    }

    public String getUsua_dni() {
        return usua_dni;
    }

    public void setUsua_dni(String usua_dni) {
        this.usua_dni = usua_dni;
    }

    public String getUsua_nombres() {
        return usua_nombres;
    }

    public void setUsua_nombres(String usua_nombres) {
        this.usua_nombres = usua_nombres;
    }

    public String getUsua_apellpat() {
        return usua_apellpat;
    }

    public void setUsua_apellpat(String usua_apellpat) {
        this.usua_apellpat = usua_apellpat;
    }

    public String getUsua_apellmat() {
        return usua_apellmat;
    }

    public void setUsua_apellmat(String usua_apellmat) {
        this.usua_apellmat = usua_apellmat;
    }

    public String getUsua_celular() {
        return usua_celular;
    }

    public void setUsua_celular(String usua_celular) {
        this.usua_celular = usua_celular;
    }

    public String getUsua_fijo() {
        return usua_fijo;
    }

    public void setUsua_fijo(String usua_fijo) {
        this.usua_fijo = usua_fijo;
    }

    public String getUsua_email() {
        return usua_email;
    }

    public void setUsua_email(String usua_email) {
        this.usua_email = usua_email;
    }

    public String getUsua_antecedentes() {
        return usua_antecedentes;
    }

    public void setUsua_antecedentes(String usua_antecedentes) {
        this.usua_antecedentes = usua_antecedentes;
    }

    public String getUsua_foto() {
        return usua_foto;
    }

    public void setUsua_foto(String usua_foto) {
        this.usua_foto = usua_foto;
    }

    public String getUsua_dnifoto() {
        return usua_dnifoto;
    }

    public void setUsua_dnifoto(String usua_dnifoto) {
        this.usua_dnifoto = usua_dnifoto;
    }

    public String getUsua_direccion() {
        return usua_direccion;
    }

    public void setUsua_direccion(String usua_direccion) {
        this.usua_direccion = usua_direccion;
    }

    public Integer getUsua_tipo() {
        return usua_tipo;
    }

    public void setUsua_tipo(Integer usua_tipo) {
        this.usua_tipo = usua_tipo;
    }

    public Integer getUsua_estado() {
        return usua_estado;
    }

    public void setUsua_estado(Integer usua_estado) {
        this.usua_estado = usua_estado;
    }

    public Date getUsua_fechareg() {
        return usua_fechareg;
    }

    public void setUsua_fechareg(Date usua_fechareg) {
        this.usua_fechareg = usua_fechareg;
    }

    public String getUsua_clave() {
        return usua_clave;
    }

    public void setUsua_clave(String usua_clave) {
        this.usua_clave = usua_clave;
    }


    public Double getUsua_longitud() {
        return usua_longitud;
    }

    public void setUsua_longitud(Double usua_longitud) {
        this.usua_longitud = usua_longitud;
    }

    public Double getUsua_latitud() {
        return usua_latitud;
    }

    public void setUsua_latitud(Double usua_latitud) {
        this.usua_latitud = usua_latitud;
    }

    public String getUsua_ubigeo() {
        return usua_ubigeo;
    }

    public void setUsua_ubigeo(String usua_ubigeo) {
        this.usua_ubigeo = usua_ubigeo;
    }

    public String getUsua_referencia() {
        return usua_referencia;
    }

    public void setUsua_referencia(String usua_referencia) {
        this.usua_referencia = usua_referencia;
    }

    public Integer getUsua_vivienda() {
        return usua_vivienda;
    }

    public void setUsua_vivienda(Integer usua_vivienda) {
        this.usua_vivienda = usua_vivienda;
    }

    public String getUsua_descripcion() {
        return usua_descripcion;
    }

    public void setUsua_descripcion(String usua_descripcion) {
        this.usua_descripcion = usua_descripcion;
    }

    public Integer getUsua_tipodoc() {
        return usua_tipodoc;
    }

    public void setUsua_tipodoc(Integer usua_tipodoc) {
        this.usua_tipodoc = usua_tipodoc;
    }

    public String getUsua_idcuenta() {
        return usua_idcuenta;
    }

    public void setUsua_idcuenta(String usua_idcuenta) {
        this.usua_idcuenta = usua_idcuenta;
    }

    public Integer getUsua_tipocuenta() {
        return usua_tipocuenta;
    }

    public void setUsua_tipocuenta(Integer usua_tipocuenta) {
        this.usua_tipocuenta = usua_tipocuenta;
    }
}
