package com.idesolution.sist_five.models;


import javax.persistence.*;

@Entity
@Table(name = "tb_especialidad")
public class Especialidad {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long espe_id;
    private String espe_nombre;
    private String espe_descripcion;
    private String espe_imagen;
    private int espe_estado;
    private int serv_id;

    public Long getEspe_id() {
        return espe_id;
    }

    public void setEspe_id(Long espe_id) {
        this.espe_id = espe_id;
    }

    public String getEspe_nombre() {
        return espe_nombre;
    }

    public void setEspe_nombre(String espe_nombre) {
        this.espe_nombre = espe_nombre;
    }

    public String getEspe_descripcion() {
        return espe_descripcion;
    }

    public void setEspe_descripcion(String espe_descripcion) {
        this.espe_descripcion = espe_descripcion;
    }

    public String getEspe_imagen() {
        return espe_imagen;
    }

    public void setEspe_imagen(String espe_imagen) {
        this.espe_imagen = espe_imagen;
    }

    public int getEspe_estado() {
        return espe_estado;
    }

    public void setEspe_estado(int espe_estado) {
        this.espe_estado = espe_estado;
    }

    public int getServ_id() {
        return serv_id;
    }

    public void setServ_id(int serv_id) {
        this.serv_id = serv_id;
    }
}
