package com.idesolution.sist_five.models;

import javax.persistence.*;

@Entity
@Table(name="tb_parametro")
public class Parametro {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long para_id;
    private String para_codigo;
    private String para_nombre;
    private String para_contenido;
    private int para_tipo;
    private int para_estado;

    public Long getPara_id() {
        return para_id;
    }

    public void setPara_id(Long para_id) {
        this.para_id = para_id;
    }

    public String getPara_codigo() {
        return para_codigo;
    }

    public void setPara_codigo(String para_codigo) {
        this.para_codigo = para_codigo;
    }

    public String getPara_nombre() {
        return para_nombre;
    }

    public void setPara_nombre(String para_nombre) {
        this.para_nombre = para_nombre;
    }

    public String getPara_contenido() {
        return para_contenido;
    }

    public void setPara_contenido(String para_contenido) {
        this.para_contenido = para_contenido;
    }

    public int getPara_tipo() {
        return para_tipo;
    }

    public void setPara_tipo(int para_tipo) {
        this.para_tipo = para_tipo;
    }

    public int getPara_estado() {
        return para_estado;
    }

    public void setPara_estado(int para_estado) {
        this.para_estado = para_estado;
    }
}
