package com.idesolution.sist_five.models;

import javax.persistence.*;

@Entity
@Table(name="tb_dist_json")
public class dist_json {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer depa_id;
    private String depa_nombre;
    private Integer prov_id;
    private String prov_nombre;
    private Integer dist_id;
    private String dist_nombre;

    public dist_json(Integer depa_id, String depa_nombre, Integer prov_id, String prov_nombre, Integer dist_id, String dist_nombre) {
        this.depa_id = depa_id;
        this.depa_nombre = depa_nombre;
        this.prov_id = prov_id;
        this.prov_nombre = prov_nombre;
        this.dist_id = dist_id;
        this.dist_nombre = dist_nombre;
    }

    public Integer getDepa_id() {
        return depa_id;
    }

    public void setDepa_id(Integer depa_id) {
        this.depa_id = depa_id;
    }

    public String getDepa_nombre() {
        return depa_nombre;
    }

    public void setDepa_nombre(String depa_nombre) {
        this.depa_nombre = depa_nombre;
    }

    public Integer getProv_id() {
        return prov_id;
    }

    public void setProv_id(Integer prov_id) {
        this.prov_id = prov_id;
    }

    public String getProv_nombre() {
        return prov_nombre;
    }

    public void setProv_nombre(String prov_nombre) {
        this.prov_nombre = prov_nombre;
    }

    public Integer getDist_id() {
        return dist_id;
    }

    public void setDist_id(Integer dist_id) {
        this.dist_id = dist_id;
    }

    public String getDist_nombre() {
        return dist_nombre;
    }

    public void setDist_nombre(String dist_nombre) {
        this.dist_nombre = dist_nombre;
    }
}
