package com.idesolution.sist_five.models;

import javax.persistence.*;

@Entity
@Table(name = "tb_servicio")
public class Servicio {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long serv_id;
    private String serv_nombre;
    private String serv_descripcion;
    private String serv_imagen;
    private int serv_estado;


    public Long getServ_id() {
        return serv_id;
    }

    public void setServ_id(Long serv_id) {
        this.serv_id = serv_id;
    }

    public String getServ_nombre() {
        return serv_nombre;
    }

    public void setServ_nombre(String serv_nombre) {
        this.serv_nombre = serv_nombre;
    }

    public String getServ_descripcion() {
        return serv_descripcion;
    }

    public void setServ_descripcion(String serv_descripcion) {
        this.serv_descripcion = serv_descripcion;
    }

    public String getServ_imagen() {
        return serv_imagen;
    }

    public void setServ_imagen(String serv_imagen) {
        this.serv_imagen = serv_imagen;
    }

    public int getServ_estado() {
        return serv_estado;
    }

    public void setServ_estado(int serv_estado) {
        this.serv_estado = serv_estado;
    }
}