package com.idesolution.sist_five.models;

import javax.persistence.*;

@Entity
@Table(name="tb_resolicitud")
public class ReSolicitud {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long soci_id;
    private Integer usua_id;
    private Integer espe_id;
    private Integer soci_nivel;
    private String soci_ubicacion;
    private String soci_diaaten;
    private String soci_horaaten;
    private String soci_detalle;
    private String soci_destino;
    private Integer soci_estado;
    private String soci_direccion;
    private Double soci_latitud;
    private Double soci_longitud;
    private String soci_instruccion;
    private String soci_etiqueta;
    private Integer soci_idetiqueta;
    private Integer corr_id;

    public Long getSoci_id() {
        return soci_id;
    }

    public void setSoci_id(Long soci_id) {
        this.soci_id = soci_id;
    }

    public Integer getUsua_id() {
        return usua_id;
    }

    public void setUsua_id(Integer usua_id) {
        this.usua_id = usua_id;
    }

    public Integer getEspe_id() {
        return espe_id;
    }

    public void setEspe_id(Integer espe_id) {
        this.espe_id = espe_id;
    }

    public Integer getSoci_nivel() {
        return soci_nivel;
    }

    public void setSoci_nivel(Integer soci_nivel) {
        this.soci_nivel = soci_nivel;
    }

    public String getSoci_ubicacion() {
        return soci_ubicacion;
    }

    public void setSoci_ubicacion(String soci_ubicacion) {
        this.soci_ubicacion = soci_ubicacion;
    }

    public String getSoci_diaaten() {
        return soci_diaaten;
    }

    public void setSoci_diaaten(String soci_diaaten) {
        this.soci_diaaten = soci_diaaten;
    }

    public String getSoci_horaaten() {
        return soci_horaaten;
    }

    public void setSoci_horaaten(String soci_horaaten) {
        this.soci_horaaten = soci_horaaten;
    }

    public String getSoci_detalle() {
        return soci_detalle;
    }

    public void setSoci_detalle(String soci_detalle) {
        this.soci_detalle = soci_detalle;
    }

    public String getSoci_destino() {
        return soci_destino;
    }

    public void setSoci_destino(String soci_destino) {
        this.soci_destino = soci_destino;
    }

    public Integer getSoci_estado() {
        return soci_estado;
    }

    public void setSoci_estado(Integer soci_estado) {
        this.soci_estado = soci_estado;
    }

    public String getSoci_direccion() {
        return soci_direccion;
    }

    public void setSoci_direccion(String soci_direccion) {
        this.soci_direccion = soci_direccion;
    }

    public Double getSoci_latitud() {
        return soci_latitud;
    }

    public void setSoci_latitud(Double soci_latitud) {
        this.soci_latitud = soci_latitud;
    }

    public Double getSoci_longitud() {
        return soci_longitud;
    }

    public void setSoci_longitud(Double soci_longitud) {
        this.soci_longitud = soci_longitud;
    }

    public String getSoci_instruccion() {
        return soci_instruccion;
    }

    public void setSoci_instruccion(String soci_instruccion) {
        this.soci_instruccion = soci_instruccion;
    }

    public String getSoci_etiqueta() {
        return soci_etiqueta;
    }

    public void setSoci_etiqueta(String soci_etiqueta) {
        this.soci_etiqueta = soci_etiqueta;
    }

    public Integer getCorr_id() {
        return corr_id;
    }

    public void setCorr_id(Integer corr_id) {
        this.corr_id = corr_id;
    }

    public Integer getSoci_idetiqueta() {
        return soci_idetiqueta;
    }

    public void setSoci_idetiqueta(Integer soci_idetiqueta) {
        this.soci_idetiqueta = soci_idetiqueta;
    }
}
