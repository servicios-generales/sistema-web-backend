package com.idesolution.sist_five.models;
import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name="tb_etiqueta")
public class Etiqueta {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int etiq_id;
    private String etiq_nombre;
    private int etiq_estado;
    private String etiq_direccion;
    private  Date etiq_fechareg;
    private Double etiq_longitud;
    private Double etiq_latitud;
    private String etiq_instruccion;
    private String etiq_ubicacion;
    private int usua_id;

    public int getEtiq_id() {
        return etiq_id;
    }

    public void setEtiq_id(int etiq_id) {
        this.etiq_id = etiq_id;
    }

    public String getEtiq_nombre() {
        return etiq_nombre;
    }

    public void setEtiq_nombre(String etiq_nombre) {
        this.etiq_nombre = etiq_nombre;
    }

    public int getEtiq_estado() {
        return etiq_estado;
    }

    public void setEtiq_estado(int etiq_estado) {
        this.etiq_estado = etiq_estado;
    }

    public String getEtiq_direccion() {
        return etiq_direccion;
    }

    public void setEtiq_direccion(String etiq_direccion) {
        this.etiq_direccion = etiq_direccion;
    }

    public Date getEtiq_fechareg() {
        return etiq_fechareg;
    }

    public void setEtiq_fechareg(Date etiq_fechareg) {
        this.etiq_fechareg = etiq_fechareg;
    }

    public Double getEtiq_longitud() {
        return etiq_longitud;
    }

    public void setEtiq_longitud(Double etiq_longitud) {
        this.etiq_longitud = etiq_longitud;
    }

    public Double getEtiq_latitud() {
        return etiq_latitud;
    }

    public void setEtiq_latitud(Double etiq_latitud) {
        this.etiq_latitud = etiq_latitud;
    }

    public String getEtiq_instruccion() {
        return etiq_instruccion;
    }

    public void setEtiq_instruccion(String etiq_instruccion) {
        this.etiq_instruccion = etiq_instruccion;
    }

    public String getEtiq_ubicacion() {
        return etiq_ubicacion;
    }

    public void setEtiq_ubicacion(String etiq_ubicacion) {
        this.etiq_ubicacion = etiq_ubicacion;
    }

    public int getUsua_id() {
        return usua_id;
    }

    public void setUsua_id(int usua_id) {
        this.usua_id = usua_id;
    }
}
