package com.idesolution.sist_five.models;

import javax.persistence.*;

@Entity
@Table(name="tb_distrito")
public class Distrito {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long dist_id;
    private String dist_codigo;
    private String dist_nombre;
    private Integer dist_estado;
    private Integer prov_id;

    public Long getDist_id() {
        return dist_id;
    }

    public void setDist_id(Long dist_id) {
        this.dist_id = dist_id;
    }

    public String getDist_codigo() {
        return dist_codigo;
    }

    public void setDist_codigo(String dist_codigo) {
        this.dist_codigo = dist_codigo;
    }

    public String getDist_nombre() {
        return dist_nombre;
    }

    public void setDist_nombre(String dist_nombre) {
        this.dist_nombre = dist_nombre;
    }

    public Integer getDist_estado() {
        return dist_estado;
    }

    public void setDist_estado(Integer dist_estado) {
        this.dist_estado = dist_estado;
    }

    public Integer getProv_id() {
        return prov_id;
    }

    public void setProv_id(Integer prov_id) {
        this.prov_id = prov_id;
    }
}
