package com.idesolution.sist_five.models;

import javax.persistence.*;

@Entity
@Table(name="tb_alerta")
public class Alerta {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long aler_id;
    private String aler_contenido;
    private String aler_icono;
    private String aler_url;
    private int aler_estado;
    private String aler_fecha;
    private Integer usua_id;
    private Long corr_id;

    public Long getAler_id() {
        return aler_id;
    }

    public void setAler_id(Long aler_id) {
        this.aler_id = aler_id;
    }

    public String getAler_contenido() {
        return aler_contenido;
    }

    public void setAler_contenido(String aler_contenido) {
        this.aler_contenido = aler_contenido;
    }

    public String getAler_icono() {
        return aler_icono;
    }

    public void setAler_icono(String aler_icono) {
        this.aler_icono = aler_icono;
    }

    public String getAler_url() {
        return aler_url;
    }

    public void setAler_url(String aler_url) {
        this.aler_url = aler_url;
    }

    public int getAler_estado() {
        return aler_estado;
    }

    public void setAler_estado(int aler_estado) {
        this.aler_estado = aler_estado;
    }

    public String getAler_fecha() {
        return aler_fecha;
    }

    public void setAler_fecha(String aler_fecha) {
        this.aler_fecha = aler_fecha;
    }

    public Integer getUsua_id() {
        return usua_id;
    }

    public void setUsua_id(Integer usua_id) {
        this.usua_id = usua_id;
    }

    public Long getCorr_id() {
        return corr_id;
    }

    public void setCorr_id(Long corr_id) {
        this.corr_id = corr_id;
    }
}
