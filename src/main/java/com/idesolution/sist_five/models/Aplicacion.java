package com.idesolution.sist_five.models;

import javax.persistence.*;

@Entity
@Table(name="tb_aplicacion")
public class Aplicacion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long apli_id;
    private String apli_denominacion;
    private String apli_abreviatura;
    private String apli_logo;
    private int apli_estado;

    @Override
    public String toString() {
        return "Aplicacion{" +
                "apli_id=" + apli_id +
                ", apli_denominacion='" + apli_denominacion + '\'' +
                ", apli_abreviatura='" + apli_abreviatura + '\'' +
                ", apli_logo='" + apli_logo + '\'' +
                ", apli_estado=" + apli_estado +
                '}';
    }

    public Long getApli_id() {
        return apli_id;
    }

    public void setApli_id(Long apli_id) {
        this.apli_id = apli_id;
    }

    public String getApli_denominacion() {
        return apli_denominacion;
    }

    public void setApli_denominacion(String apli_denominacion) {
        this.apli_denominacion = apli_denominacion;
    }

    public String getApli_abreviatura() {
        return apli_abreviatura;
    }

    public void setApli_abreviatura(String apli_abreviatura) {
        this.apli_abreviatura = apli_abreviatura;
    }

    public String getApli_logo() {
        return apli_logo;
    }

    public void setApli_logo(String apli_logo) {
        this.apli_logo = apli_logo;
    }

    public int getApli_estado() {
        return apli_estado;
    }

    public void setApli_estado(int apli_estado) {
        this.apli_estado = apli_estado;
    }
}
