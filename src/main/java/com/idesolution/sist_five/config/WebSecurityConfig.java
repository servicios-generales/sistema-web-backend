package com.idesolution.sist_five.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
    private UserDetailsService jwtUserDetailsService;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // configure AuthenticationManager so that it knows from where to load
        // user for matching credentials
        // Use BCryptPasswordEncoder
        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        // We don't need CSRF for this example
        httpSecurity.csrf().disable()
                // dont authenticate this particular request
                .authorizeRequests().antMatchers("/authenticate").permitAll()
                .antMatchers("/api/get_version/**").permitAll()
                .antMatchers("/api/file/**").permitAll()
                .antMatchers("/api/servicio/**").permitAll()
                .antMatchers("/api/especialidad/listxservicio/**").permitAll()
                .antMatchers("/api/user/recover_password/**").permitAll()
                .antMatchers("/api/user/validar_codigo_recuperacion/**").permitAll()
                .antMatchers("/api/user/resetear_clave/**").permitAll()
                .antMatchers("/api/user/verificar_email_public/**").permitAll()
                .antMatchers("/api/solicitudes/add_suscripcion/**").permitAll()
                .antMatchers("/api/file/upload/**").permitAll()
                .antMatchers("/api/solicitudes/subir_comprobante/**").permitAll()
                .antMatchers("/api/culqi/**").permitAll()
                .antMatchers("/api/solicitudes/subir_comprobante/**").permitAll()
                .antMatchers("/api/solicitudes/subir_imagenes/**").permitAll()
                .antMatchers("/api/solicitudes/inactivar_suscripcion_get/**").permitAll()
                .antMatchers("/api/opcion/opcionesxid/**").permitAll()
                .antMatchers("/api/concepto/list_cabeceras/**").permitAll()

                //.antMatchers("/api/culqi/**").permitAll()
               // .antMatchers("/api/user/especialistas/**").permitAll()
                .antMatchers("/api/cupon/aplicar_cupon_proforma/**").permitAll()
                //.antMatchers("/api/solicitudes/ver_detalle_proforma_especialista/**").permitAll()


                // all other requests need to be authenticated
                      //  anyRequest().authenticated().and().
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll().anyRequest().authenticated().and().
                // make sure we use stateless session; session won't be used to
                // store user's state.
                        exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // Add a filter to validate the tokens with every request
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }
}

