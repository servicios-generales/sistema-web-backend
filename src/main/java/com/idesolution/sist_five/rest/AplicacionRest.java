package com.idesolution.sist_five.rest;

import com.culqi.Culqi;
import com.google.gson.JsonObject;
import com.idesolution.sist_five.dto.Mensaje;
import com.idesolution.sist_five.dto.MensajeCulqi;
import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.Concepto;
import com.idesolution.sist_five.service.AplicacionService;
import com.idesolution.sist_five.service.ConceptoService;
import com.idesolution.sist_five.service.LogErrorService;
import com.idesolution.sist_five.service.SolicitarCitaService;
import nl.martijndwars.webpush.Notification;
import nl.martijndwars.webpush.PushService;
import nl.martijndwars.webpush.Subscription;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.http.HttpResponse;
import org.json.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.net.URI;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import com.google.gson.Gson;
import org.springframework.web.client.RestTemplate;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Scanner;
import java.security.Security;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/")
public class AplicacionRest {
    private static final String PUBLIC_KEY = "BAPGG2IY3Vn48d_H8QNuVLRErkBI0L7oDOOCAMUBqYMTMTzukaIAuB5OOcmkdeRICcyQocEwD-oxVc81YXXZPRY";
    private static final String PRIVATE_KEY = "A7xDGuxMZ4ufflcAhBW23xpoWZNOLwM4Rw2wXjP0y6M";
    private static final String SUBJECT = "Foobarbaz";
    private static final String PAYLOAD = "";
    Culqi culqi=new Culqi();

    @Autowired
    private SolicitarCitaService solicitarCitaService;
    @Autowired
    private LogErrorService logErrorService;
    @Autowired
    private AplicacionService aplicacionService;
    @GetMapping(path="/")
    public ResponseEntity<Mensaje> get_saludo(){
        return new ResponseEntity<>(new Mensaje("Bienvenido a la API de FIVE"), HttpStatus.OK);
    }
    @GetMapping("/api")
    public ResponseEntity<Mensaje> get_mensaje_api(){
        
        /*OkHttpClient client = new OkHttpClient().newBuilder().build();
        MediaType mediaType = MediaType.parse("application/json");
        //okhttp3.RequestBody obj= okhttp3.RequestBody.create(mediaType,"{\n" + " \"from\":\"InfoSMS\",\n" + " \"to\":\"41793026727\",\n" + " \"text\":\"My first Infobip SMS\"\n" + "}\n");
        okhttp3.RequestBody  body = okhttp3.RequestBody.create(mediaType, "{\n" + " \"from\":\"InfoSMS\",\n" + " \"to\":\"41793026727\",\n" + " \"text\":\"My first Infobip SMS\"\n" + "}\n");
        Request request = new Request.Builder()
                .url("https://api.infobip.com/sms/1/text/single")
                .method("POST", body)
                .addHeader("Authorization", "Basic Rml2ZS5BbGZhcm86RmxlbWluZ0AyNw==")
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .build();
        try {
            Response response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
      /*
        JSONObject myObject = new JSONObject();
        myObject.put("country", "+51");
        myObject.put("dateToSend", "2021-04-28 00:08:00");
        myObject.put("message", "FIVE<br>Hola Usuario.mensaje es enviado desde Tellid.");
        myObject.put("encoding", "");
        myObject.put("messageFormat", 0);

        JSONObject myObject2 = new JSONObject();
        myObject2.put("mobile", "966841797");
        myObject2.put("correlationLabel", "2");
        myObject2.put("url", "https://five.ide-solution.com/");
        JSONArray listsms=new JSONArray();
        listsms.put(myObject2);
        myObject.put("addresseeList", listsms);

        System.out.println(myObject);

        try {
            String url = "https://apitellit.aldeamo.com/SmsiWS/smsSendPost/";
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setRequestMethod("POST");

            String user_name="IDESolution";
            String password="Ides@2021#";

            String userCredentials = user_name+":"+password;
            String basicAuth = "Basic " + new String(new Base64().encode(userCredentials.getBytes()));
            con.setRequestProperty ("Authorization", basicAuth);
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:27.0) Gecko/20100101 Firefox/27.0.2 Waterfox/27.0");
            con.setRequestProperty("Content-Type", "application/json");
            //String urlParameters = "mobile=925938900&country=+51&message=five2021&messageFormat=";
            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.write(myObject.toString().getBytes("UTF-8"));
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + myObject.toString());
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            System.out.println(response.toString());


        } catch (Exception e) {
            System.out.println("Error : "+e);
        }*/
        return new ResponseEntity<>(new Mensaje("Existe Recurso"), HttpStatus.OK);
    }
    //@Scheduled(cron = "* */5 0-23 * * *", zone = "America/Mexico_City")
    /*@GetMapping("/api/enviosms")
    public void envio_sms(String mensaje, String numero){
        List<Map<String,Object>> listu= solicitarCitaService.buscar_usuarios();

        //api infobip
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        MediaType mediaType = MediaType.parse("application/json");
        //okhttp3.RequestBody obj= okhttp3.RequestBody.create(mediaType,"{\n" + " \"from\":\"InfoSMS\",\n" + " \"to\":\"41793026727\",\n" + " \"text\":\"My first Infobip SMS\"\n" + "}\n");
        okhttp3.RequestBody  body = okhttp3.RequestBody.create(mediaType, "{\n" + " \"from\":\"FiveSMS\",\n" + " \"to\":\""+numero+"\",\n" + " \"text\":\"" +mensaje+ "\"}\n");
        Request request = new Request.Builder()
                .url("https://api.infobip.com/sms/1/text/single")
                .method("POST", body)
                .addHeader("Authorization", "Basic Rml2ZS5BbGZhcm86RmxlbWluZ0AyNw==")
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .build();
        try {
            Response response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    @RequestMapping("/send")
    @Scheduled(cron = "*/1 * * * * *", zone = "America/Mexico_City") //investigar que log por parte del cron y por parte de method
    public String send() {
        Security.addProvider(new BouncyCastleProvider());
        try {
            List<Map<String,Object>> listu= solicitarCitaService.buscar_usuarios();
            List<Map<String,Object>> lists= solicitarCitaService.listar_suscripciones(Integer.parseInt(listu.get(0).get("usua_id").toString()));

            for (int i = 0; i < lists.size(); i++) {
                List<Map<String,Object>> list=null;
                if(lists.size()-1==i){
                    list= solicitarCitaService.mostrar_notificacion_job(Integer.parseInt(listu.get(0).get("usua_id").toString()),1);
                }else{
                    list= solicitarCitaService.mostrar_notificacion_job(Integer.parseInt(listu.get(0).get("usua_id").toString()),0);
                }

                Integer varid=Integer.parseInt(list.get(0).get("aler_id").toString());
                if(varid>0){
                    String subscriptionJson= lists.get(i).get("suscripcion").toString();
                    String contenido = list.get(0).get("aler_contenido").toString();
                    //this.envio_sms(contenido+"  https://appfive.pe/","51"+lists.get(i).get("celular").toString());
                    PushService pushService = new PushService(PUBLIC_KEY, PRIVATE_KEY, SUBJECT);
                    Subscription subscription = new Gson().fromJson(subscriptionJson, Subscription.class);
                    Notification notification = new Notification(subscription, contenido);
                    HttpResponse httpResponse = pushService.send(notification);
                   // System.out.print(httpResponse+"/n");
                  //  int statusCode = httpResponse.getStatusLine().getStatusCode();
                   // return String.valueOf(statusCode);
                }else{
                    return "";
                }
            }
            return "";
        } catch (Exception e) {
            return ExceptionUtils.getStackTrace(e);
        }
    }

    @RequestMapping("/api/culqi")
    public ResponseEntity<MensajeCulqi>   confirmar_pago_culqi(@RequestParam("soci_id") String soci_id,@RequestParam("codigo") String codigo,@RequestParam("tipo") String tipo,@RequestParam("token") String token_created) {//


        //culqi.public_key = "pk_test_e9a022ffa830bffa";

        //culqi.secret_key = "sk_test_7876c5bcfc36f5b7";
        culqi.public_key = "pk_test_0d92a002f01e7258";

        culqi.secret_key = "sk_test_bd7b52e586e20c18";
        JsonObject jsontoken = new Gson().fromJson(token_created, JsonObject.class);
        String monto="";
        if(Integer.parseInt(tipo)==1){
            List<Map<String,Object>> list=solicitarCitaService.monto_pago_visita(Integer.parseInt(soci_id),codigo);
            System.out.print("Monto de base:"+list.get(0).get("monto").toString());
            monto=list.get(0).get("monto").toString();
            System.out.print(monto);
        }else  if(Integer.parseInt(tipo)==2){
            List<Map<String,Object>> list=solicitarCitaService.monto_pago_cotizacio(Integer.parseInt(soci_id),codigo);
            System.out.print("Monto de base:"+list.get(0).get("monto").toString());
            monto=list.get(0).get("monto").toString();
            System.out.print(monto);
        }
        System.out.println(jsontoken.get("id").toString().replaceAll("^\"|\"$", ""));
        try {

            Map<String, Object> charge = new HashMap<String, Object>();
            Map<String, Object> metadata = new HashMap<String, Object>();
            metadata.put("oder_id", soci_id);
            charge.put("amount", Integer.parseInt(""+monto));
            charge.put("currency_code","PEN");
            charge.put("email",jsontoken.get("email").toString().replaceAll("^\"|\"$", ""));
            charge.put("source_id", jsontoken.get("id").toString().replaceAll("^\"|\"$", ""));
            charge.put("metadata", metadata);
            Map<String, Object> charge_created = culqi.charge.create(charge);

            System.out.print(charge_created);

            if(charge_created.get("object").toString().equals("error")){
                return new ResponseEntity<>(new MensajeCulqi("error",charge_created.get("user_message").toString()),HttpStatus.BAD_REQUEST);
            }else{
                return new ResponseEntity<>(new MensajeCulqi("exito","Se realizó Correctamente el Pago."),HttpStatus.OK);
            }

        } catch (Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("api/culqi");
            logErrorService.add_log_error(obj);
            System.out.print(ExceptionUtils.getStackTrace(e));
            return new ResponseEntity<>(new MensajeCulqi("error",ExceptionUtils.getStackTrace(e)),HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping(path="/api/get_version")
    public ResponseEntity<Mensaje> validar_version(){
        return new ResponseEntity<>(new Mensaje(aplicacionService.validar_version()), HttpStatus.OK);
    }



}
