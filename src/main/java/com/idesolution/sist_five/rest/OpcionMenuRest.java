package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.Especialidad;
import com.idesolution.sist_five.models.Opcion_menu;
import com.idesolution.sist_five.service.LogErrorService;
import com.idesolution.sist_five.service.OpcionMenuService;
import com.idesolution.sist_five.service.SolicitarCitaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/opcion")
public class OpcionMenuRest {
    @Autowired
    private OpcionMenuService opcionMenuService;
    @Autowired
    private LogErrorService logErrorService;
    @Autowired
    private SolicitarCitaService solicitarCitaService;
    @GetMapping(path="/opcionesxid/{usua_id}/{opme_tipo}")
    public ResponseEntity<List<Map<String,Object>>> get_opcionesxid(@PathVariable("usua_id") Integer usua_id,
                                                                    @PathVariable("opme_tipo") Integer opme_tipo){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try{
            List<Map<String,Object>> list=opcionMenuService.get_opcionesxid( usua_id,opme_tipo);
            return new ResponseEntity<>(list, HttpStatus.OK);

        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("opcion/opcionesxid/{usua_id}/{opme_tipo}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }
}
