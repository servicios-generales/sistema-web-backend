package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.Mensaje;
import com.idesolution.sist_five.dto.ParametroA;
import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.Opcion_menu;
import com.idesolution.sist_five.models.Parametro;
import com.idesolution.sist_five.models.Usuario;
import com.idesolution.sist_five.service.LogErrorService;
import com.idesolution.sist_five.service.ParametroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/parametro")
public class ParametroRest {
    @Autowired
    private ParametroService parametroService;
    @Autowired
    private LogErrorService logErrorService;
    @GetMapping(path="/obtener/{conc_id}")
    public ResponseEntity<List<Map<String,Object>>> get_parametro(@PathVariable("conc_id") Integer conc_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try{
            List<Map<String,Object>> list=parametroService.get_parametro( conc_id);
            return new ResponseEntity<>(list, HttpStatus.OK);

        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("parametro/obtener/{conc_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;

    }
    @GetMapping(path="/fecha_hora")
    public ResponseEntity<List<Map<String,Object>>> get_fechactual(){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try{
            List<Map<String,Object>> list=parametroService.get_fechactual( );
            return new ResponseEntity<>(list, HttpStatus.OK);

        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("parametro/fecha_hora");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;

    }

    @GetMapping(path="/list/{para_prefijo}")
    public ResponseEntity<List<Map<String,Object>>> get_list_parametro(@PathVariable("para_prefijo") Integer para_prefijo){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try{
            List<Map<String,Object>> list=parametroService.get_list_parametro( para_prefijo);
            return new ResponseEntity<>(list, HttpStatus.OK);

        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("parametro/obtener/{para_prefijo}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;

    }
    @GetMapping(path="/get_parametros_x_prefijo/{para_prefijo}")
    public ResponseEntity<List<Map<String,Object>>> get_parametros_x_prefijo(@PathVariable("para_prefijo") Integer para_prefijo){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String,Object>>  listp = parametroService.get_parametros_x_prefijo(para_prefijo);
            return new ResponseEntity<>(listp, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("parametro/get_parametros_x_prefijo/{para_prefijo}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }
    @GetMapping(path="/get_parametros_todos")
    public ResponseEntity<List<Map<String,Object>>> get_parametros_todos(){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String,Object>>  listp = parametroService.get_parametros_todos();
            return new ResponseEntity<>(listp, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("parametro/get_parametros_todos");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @GetMapping(path="/get_parametro_x_prefijoycorrelativo/{para_prefijo}/{para_correlativo}")
    public ResponseEntity<List<Map<String,Object>>> get_parametro_x_prefijoycorrelativo(@PathVariable("para_prefijo") Integer para_prefijo,@PathVariable("para_correlativo") Integer para_correlativo){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String,Object>>  listp = parametroService.get_parametro_x_prefijoycorrelativo(para_prefijo,para_correlativo);
            return new ResponseEntity<>(listp, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("parametro/get_parametro_x_prefijoycorrelativo/{para_prefijo}/{para_correlativo}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @PostMapping ("/update_parametro")
    public ResponseEntity<Mensaje> update_parametro(@RequestBody ParametroA objp){
        ResponseEntity<Mensaje> response = null;
        try {
            parametroService.update_parametro_x_id(objp );
            return new ResponseEntity<Mensaje>(new Mensaje("Parametro actualizado correctamente"), HttpStatus.CREATED);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("parametro/update_parametro");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @PostMapping ("/add_parametro")
    public ResponseEntity<Mensaje> add_parametro(@RequestBody ParametroA objp){
        ResponseEntity<Mensaje> response = null;

        if(parametroService.existsByPara_cadena1(objp.getPara_cadena1()))
            return new ResponseEntity(new Mensaje("Parametro ya existe"), HttpStatus.BAD_REQUEST);

        try {
            parametroService.add_parametro(objp );
            return new ResponseEntity<Mensaje>(new Mensaje("Parametro registrado correctamente"), HttpStatus.CREATED);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("parametro/add_parametro");
            logErrorService.add_log_error(obj);

        }
        return response;
    }


    @DeleteMapping("/delete_parametro/{para_id}")
    void deleteEmployee(@PathVariable Integer para_id) {
        try {
            parametroService.delete_parametro(para_id);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("parametro/delete_parametro");
            logErrorService.add_log_error(obj);

        }

    }
}
