package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.log_error;

import com.idesolution.sist_five.models.Concepto;
import com.idesolution.sist_five.models.Departamento;
import com.idesolution.sist_five.models.Parametro;
import com.idesolution.sist_five.service.DepartamentoService;
import com.idesolution.sist_five.service.LogErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/departamento")
public class DepartamentoRest {
    @Autowired
    private DepartamentoService departamentoService;
    @Autowired
    private LogErrorService logErrorService;
    @GetMapping(path="/list")
    public ResponseEntity<List<Departamento>> get_departamentos(){
        ResponseEntity<List<Departamento>> response = null;
        try {
            List<Departamento> list=departamentoService.get_departamentos( );
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("departamento/list");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
      return response;
    }

}
