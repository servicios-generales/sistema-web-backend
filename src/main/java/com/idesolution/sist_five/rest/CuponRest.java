package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.Mensaje;
import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.Cupon;
import com.idesolution.sist_five.service.CuponService;
import com.idesolution.sist_five.service.LogErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/cupon")
public class CuponRest {
    @Autowired
    private CuponService cuponService;
    @Autowired
    private LogErrorService logErrorService;

    @GetMapping(path="/list")
    public ResponseEntity<List<Map<String, Object>>> get_listar_cupones(){
        ResponseEntity<List<Map<String, Object>>> response = null;
        try{
            List<Map<String, Object>> lista = cuponService.get_listar_cupones();
            return new ResponseEntity(lista, HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("cupon/list");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }


    @GetMapping(path="/listxid/{cupo_id}")
    public ResponseEntity<List<Cupon>> listar_x_id(@PathVariable Integer cupo_id){
        ResponseEntity<List<Cupon>> response = null;
        try{
            List<Cupon> lista = cuponService.list_cupon_x_id(cupo_id);
            return new ResponseEntity(lista, HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("cupon/listxid/{cupo_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @PostMapping("/update_cupon")
    public ResponseEntity<?> update_cupon(@RequestBody Cupon objc){
        ResponseEntity<Cupon> response = null;
        try {
            cuponService.update_cupon(objc);
            return new ResponseEntity<>(objc, HttpStatus.CREATED);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("cupon/update_cupon");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @PostMapping ("/add_cupon")
    public ResponseEntity<?> add_cupon(@RequestBody Cupon objc){
        ResponseEntity<Cupon> response = null;
        if(cuponService.existsByCupo_codigo(objc.getCupo_codigo()))
            return new ResponseEntity(new Mensaje("Codigo de Cupon ya existe"), HttpStatus.BAD_REQUEST);

        try {
            cuponService.add_cupon(objc);
            return new ResponseEntity<>(objc, HttpStatus.CREATED);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("cupon/add_cupon");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @DeleteMapping("/delete_cupon/{cupo_id}")
    void delete_especialidad(@PathVariable Integer cupo_id) {
        try {
            cuponService.delete_cupon(cupo_id);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("cupon/delete_cupon");
            logErrorService.add_log_error(obj);

        }

    }


    @GetMapping(path="/public/{cupo_id}")
    public ResponseEntity<Mensaje> public_cupon(@PathVariable Integer cupo_id){
        ResponseEntity<Mensaje> response = null;
        try{
            Integer sw=cuponService.public_cupon(cupo_id);
            //if(sw == 1)
                return new ResponseEntity<Mensaje>(new Mensaje("Publicado correctamente"), HttpStatus.OK);
           // else
             //   return new ResponseEntity<Mensaje>(new Mensaje("Ya existe un cupon publicado"), HttpStatus.BAD_REQUEST);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("cupon/public/{cupo_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
            return new ResponseEntity<Mensaje>(new Mensaje("Error: No se puede registrar. Comuniquese con el Administrador del Sistema. "), HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping(path="/active/{cupo_id}")
    public ResponseEntity<?> active_cupon(@PathVariable Integer cupo_id){
        ResponseEntity<Mensaje> response = null;
        try{
            cuponService.active_cupon(cupo_id);
            return new ResponseEntity(new Mensaje("Activado correctamente"), HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("cupon/active/{cupo_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @GetMapping(path="/aplicar_cupon_visita/{corr_id}/{soci_id}/{codigo}")
    public ResponseEntity<List<Map<String, Object>>> aplicar_cupon_visita(@PathVariable Integer corr_id,
                                                                          @PathVariable Integer soci_id,
                                                                          @PathVariable String codigo){
        ResponseEntity<List<Map<String, Object>>> response = null;
        try{
            List<Map<String, Object>> lista = cuponService.aplicar_cupon_visita(corr_id,soci_id,codigo);
            return new ResponseEntity(lista, HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("cupon/aplicar_cupon_visita/{corr_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }


    @GetMapping(path="/aplicar_cupon_proforma/{corr_id}/{soci_id}/{codigo}")
    public ResponseEntity<List<Map<String, Object>>> aplicar_cupon_proforma(@PathVariable Integer corr_id,
                                                                          @PathVariable Integer soci_id,
                                                                          @PathVariable String codigo){
        ResponseEntity<List<Map<String, Object>>> response = null;
        try{
            List<Map<String, Object>> lista = cuponService.aplicar_cupon_proforma(corr_id,soci_id,codigo);
            return new ResponseEntity(lista, HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("cupon/aplicar_cupon_proforma/{corr_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @Scheduled(cron = "* */1 0-23 * * *", zone = "America/Mexico_City")
    public void caducar_cupones(){
        try {
            cuponService.caducar_cupones();
            java.util.Date fecha = new Date();
            System.out.println (fecha.getDay());

        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("cupones/caducar_cupones");
            logErrorService.add_log_error(obj);

        }
    }
}
