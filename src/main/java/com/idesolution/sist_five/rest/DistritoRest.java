package com.idesolution.sist_five.rest;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.Concepto;
import com.idesolution.sist_five.models.Distrito;
import com.idesolution.sist_five.models.dist_json;
import com.idesolution.sist_five.models.Parametro;
import com.idesolution.sist_five.service.DistritoService;
import com.idesolution.sist_five.service.LogErrorService;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.json.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/distrito")
public class DistritoRest {
    @Autowired
    private DistritoService distritoService;
    @Autowired
    private LogErrorService logErrorService;
    @GetMapping(path="/list/{prov_id}")
    public ResponseEntity<List<Distrito>> get_distritos(@PathVariable("prov_id") Integer prov_id){
        ResponseEntity<List<Distrito>> response = null;
        try {
        List<Distrito> list=distritoService.get_distritos( prov_id);
        return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("distrito/list/{prov_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
      return response;

    }

    @GetMapping(path="/ubigeo_nombre/{ubigeo}")
    public ResponseEntity<List<Map<String, Object>>> get_ubigeo_nombre(@PathVariable("ubigeo") String ubigeo){
        ResponseEntity<List<Map<String, Object>>> response = null;
        try{
        List<Map<String, Object>> list = (distritoService.get_ubigeo_nombre( ubigeo)) ;
        return new ResponseEntity<>(list,HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("get_ubigeo_nombre");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
      return response;
    }


    //@GetMapping(path="/ubigeo_nombre/{ubigeo}")
    //public ResponseEntity<JSONArray> get_ubigeo_nombre(@PathVariable("ubigeo") String ubigeo){
        //String cadena=;
      //  JSONArray json = distritoService.get_ubigeo_nombre( ubigeo);
        //return new ResponseEntity<>(json, HttpStatus.OK);
    //}

    @GetMapping(path="/ubigeo_ids/{ubigeo}")
    public ResponseEntity<String> get_ubigeo_ids(@PathVariable("ubigeo") String ubigeo){
        String cadena=distritoService.get_ubigeo_ids( ubigeo);
        return new ResponseEntity<>(cadena, HttpStatus.OK);
    }
}
