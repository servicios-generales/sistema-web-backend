package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.*;
import com.idesolution.sist_five.models.SolicitudCita;
import com.idesolution.sist_five.repositorys.S3Services;
import com.idesolution.sist_five.service.LogErrorService;
import com.idesolution.sist_five.service.SolicitarCitaService;
import com.idesolution.sist_five.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.idesolution.sist_five.repositorys.S3Services;

import javax.xml.crypto.dsig.keyinfo.KeyName;
import java.io.ByteArrayOutputStream;
import java.util.*;

import org.springframework.scheduling.annotation.Scheduled;

@RestController
@RequestMapping("/api/solicitudes")
public class Solicitudes {

    @Autowired
    private SolicitarCitaService solicitarCitaService;
    @Autowired
    private LogErrorService logErrorService;

    @Autowired
    S3Services s3Services;
    @Autowired
    private UsuarioService usuarioService;


    @PostMapping("/subir_comprobante")
    public String uploadMultipartFile(@RequestParam("keyname") String keyName, @RequestParam("file") MultipartFile file) {
        // obtener elementos del pago
        try {
            List<Map<String,Object>> obj= solicitarCitaService.subir_comprobante(Integer.parseInt(keyName));
            String cadena=obj.get(0).get("keyName").toString();
            String keyName2= cadena;
            s3Services.uploadFile(keyName2+".png", file);

            return "Upload Successfully. -> KeyName = " +keyName2;
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/list/{usua_id}");
            logErrorService.add_log_error(obj);
            return "Ocurriò un error en la subida de datos.";
        }
    }

    @GetMapping(path="/list/{usua_id}")
    public ResponseEntity<List<Map<String,Object>>> get_requerimientos(@PathVariable("usua_id") Integer usua_id){
        ResponseEntity<List<Map<String,Object>>> response = null;

        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_solicitudes(usua_id);
            /*for (int i = 0; i < list.size(); i++) {
                ByteArrayOutputStream downloadInputStream = s3Services.downloadFile(list.get(i).get("serv_imagen").toString());
                Map<String, Object> map = new HashMap<>();

                map=list.get(i);
                map.put("serv_imagen", "oaa");

                list.set(i,map);
                System.out.println(list.get(i).values());
            }*/
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return response;
    }

    @GetMapping(path="/list_x_especialidad/{usua_id}/{espe_id}")
    public ResponseEntity<List<Map<String,Object>>> get_requerimientos_x_especialidad(@PathVariable("usua_id") Integer usua_id,@PathVariable("espe_id") Integer espe_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_solicitudes_x_espeid(usua_id,espe_id);

            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/list_x_especialidad/{usua_id}/{espe_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @GetMapping(path="/list_x_estado/{usua_id}/{esta_id}")
    public ResponseEntity<List<Map<String,Object>>> get_solicitudes_x_estado(@PathVariable("usua_id") Integer usua_id,@PathVariable("esta_id") Integer esta_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_solicitudes_x_estado(usua_id,esta_id);

            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/list_x_estado/{usua_id}/{espe_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }


    @GetMapping(path="/detalle_general/{corr_id}")
    public ResponseEntity<List<Map<String,Object>>> get_solicitud_detalle_general(@PathVariable("corr_id") Integer corr_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_solicitud_detalle_general(corr_id);

            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/detalle_general/{corr_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }
    
    @GetMapping(path="/confirmar_servicio/{corr_id}/{usua_id}")
    public ResponseEntity<List<Map<String,Object>>> get_realizar_pago_solicitud(@PathVariable("corr_id") Integer corr_id,@PathVariable("usua_id") Integer usua_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_realizar_pago_solicitud(corr_id,usua_id);

            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/confirmar_servicio/{corr_id}/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }


    @GetMapping(path="/confirmar_servicio_proforma/{corr_id}/{usua_id}")
    public ResponseEntity<List<Map<String,Object>>> get_realizar_pago_proforma(@PathVariable("corr_id") Integer corr_id,@PathVariable("usua_id") Integer usua_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_realizar_pago_proforma(corr_id,usua_id);

            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/confirmar_servicio_proforma/{corr_id}/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @GetMapping(path="/ver_detalle_proforma/{soci_id}")
    public ResponseEntity<List<Map<String,Object>>> ver_detalle_proforma(@PathVariable("soci_id") Integer soci_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.list_detalle_proforma(soci_id);

            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/ver_detalle_proforma/{soci_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }


    @GetMapping(path="/ver_detalle_proforma_especialista/{soci_id}")
    public ResponseEntity<List<Map<String,Object>>> ver_detalle_proforma_especialista(@PathVariable("soci_id") Integer soci_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.list_detalle_proforma_especialista(soci_id);

            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/ver_detalle_proforma_especialista/{corr_id}/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @GetMapping(path="/detalle_especifico/{corr_id}")
    public ResponseEntity<List<Map<String,Object>>> get_solicitud_detalle_especifico(@PathVariable("corr_id") Integer corr_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_solicitud_detalle_especifico(corr_id);

            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/detalle_especifico/{corr_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }


    @PostMapping("/aceptar")
    public ResponseEntity<Mensaje> acepta_solicitud(@RequestBody ConfirmarSolicitud cs){
        try {
            cs.setSoci_tipo(4); ///cuando se acepta
            solicitarCitaService.confirmar_solicitud(cs);
            return new ResponseEntity<Mensaje>(new Mensaje("Correcto"), HttpStatus.CREATED);
        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/aceptar");
            logErrorService.add_log_error(obj);
            return new ResponseEntity<Mensaje>(new Mensaje("Error: No se puede registrar. Comuniquese con el Administrador del Sistema. "), HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping("/realizar_pago")
    public ResponseEntity<Mensaje> realizar_pago(@RequestBody PagoCupon p){
        try {
            p.setSoci_tipo(4); ///cuando se acepta
            Integer cont=solicitarCitaService.validar_confirmar_pago(p.getSoci_id(),4);
            if(cont==0){
                solicitarCitaService.realizar_pago(p);
                System.out.println("holi");
                return new ResponseEntity<Mensaje>(new Mensaje("Correcto"), HttpStatus.CREATED);
            }else{
                return new ResponseEntity<Mensaje>(new Mensaje("Incorrecto"), HttpStatus.CREATED);
            }

        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/aceptar");
            logErrorService.add_log_error(obj);
            return new ResponseEntity<Mensaje>(new Mensaje("Error: No se puede registrar. Comuniquese con el Administrador del Sistema. "), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/realizar_pago_cotizacion")
    public ResponseEntity<Mensaje> realizar_pago_cotizacion(@RequestBody PagoCupon p){
        try {
            p.setSoci_tipo(7); ///cuando se acepta
            Integer cont=solicitarCitaService.validar_confirmar_pago(p.getSoci_id(),7);
            if(cont==0){
               // HttpRequest request = HttpRequest.newBuilder(URI.create("https://apitellit.aldeamo.com/SmsiWS/smsSendPost/")).header("Content-Type", "application/json").POST(HttpRequest.BodyPublishers.ofString(inputJson)).build();
                solicitarCitaService.realizar_pago_cotizacion(p);
                return new ResponseEntity<Mensaje>(new Mensaje("Correcto"), HttpStatus.CREATED);
            }else{
                return new ResponseEntity<Mensaje>(new Mensaje("Incorrecto"), HttpStatus.CREATED);
            }
        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/realizar_pago_cotizacion");
            logErrorService.add_log_error(obj);
            return new ResponseEntity<Mensaje>(new Mensaje("Error: No se puede registrar. Comuniquese con el Administrador del Sistema. "), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/rechazar")
    public ResponseEntity<Mensaje> rechazar_solicitud(@RequestBody ConfirmarSolicitud cs){
        try {
            cs.setSoci_tipo(5); ///cuando se acepta
            solicitarCitaService.confirmar_solicitud(cs);
            return new ResponseEntity<Mensaje>(new Mensaje("Rechazado"), HttpStatus.CREATED);
        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/rechazar");
            logErrorService.add_log_error(obj);
            return new ResponseEntity<Mensaje>(new Mensaje("Error: No se puede registrar. Comuniquese con el Administrador del Sistema. "), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/confirmar_proforma/{usua_id}/{soci_id}")
    public ResponseEntity<Mensaje> aprobar_proforma(@PathVariable Integer usua_id,@PathVariable Integer soci_id){
        try {

            solicitarCitaService.confirmar_proforma(usua_id,soci_id);
            return new ResponseEntity<Mensaje>(new Mensaje("Proforma Aprobada"), HttpStatus.CREATED);
        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/confirmar_proforma");
            logErrorService.add_log_error(obj);
            return new ResponseEntity<Mensaje>(new Mensaje("Error: No se puede registrar. Comuniquese con el Administrador del Sistema. "), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/confirmar_pago_a_administrador/{usua_id}/{soci_id}/{tipo}")
    public ResponseEntity<Mensaje> confirmar_pago_a_administrador(@PathVariable Integer usua_id,@PathVariable Integer soci_id,@PathVariable Integer tipo){
        try {

            solicitarCitaService.confirmar_pago_a_administrador(usua_id,soci_id,tipo);
            return new ResponseEntity<Mensaje>(new Mensaje("Se confirmó el pago. Gracias."), HttpStatus.CREATED);
        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/confirmar_pago_a_administrador/{usua_id}/{soci_id}");
            logErrorService.add_log_error(obj);
            return new ResponseEntity<Mensaje>(new Mensaje("Error: No se puede registrar. Comuniquese con el Administrador del Sistema. "), HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/rechazar_proforma/{usua_id}/{soci_id}")
    public ResponseEntity<Mensaje> rechazar_proforma(@PathVariable Integer usua_id,@PathVariable Integer soci_id){
        try {

            solicitarCitaService.rechazar_proforma(usua_id,soci_id);
            return new ResponseEntity<Mensaje>(new Mensaje("Proforma Rechazada"), HttpStatus.CREATED);
        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/confirmar_proforma");
            logErrorService.add_log_error(obj);
            return new ResponseEntity<Mensaje>(new Mensaje("Error: No se puede registrar. Comuniquese con el Administrador del Sistema. "), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/subir_imagenes")
    public String uploadMultipartFile(@RequestParam("keyname") String keyName, @RequestParam("file") List<MultipartFile> file) {
        // obtener elementos del pago
        try {
            System.out.println(file.size());
            Integer cont=0;
            solicitarCitaService.delete_imagen_trabajo(Integer.parseInt(keyName));
            for (MultipartFile filei:file) {
                cont++;
                //String cadena=obj.get(0).get("keyName").toString();
                String keyName2= "img_"+keyName+"_"+cont;
                s3Services.uploadFile(keyName2+".png", filei);
                solicitarCitaService.subir_imagen_trabajo(Integer.parseInt(keyName),keyName2+".png");

            }
            return "Archivos Cargados Correctamente." ;
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/list/{usua_id}");
            logErrorService.add_log_error(obj);
            return "Ocurriò un error en la subida de datos.";
        }
    }

    @GetMapping(path="/list_imagenes_trabajo/{soci_id}")
    public ResponseEntity<List<Map<String,Object>>> gat_list_imagen_trabajo(@PathVariable("soci_id") Integer soci_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.list_imagen_trabajo(soci_id);

            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/list_imagenes_trabajo/{corr_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @GetMapping("/add_suscripcion_get/{usua_id}/{suscripcion}")
    public String add_suscripcion(@PathVariable("usua_id") Integer usua_id,@PathVariable("suscripcion") String suscripcion){
        String response = null;
        try{
            List<Map<String,Object>> list=solicitarCitaService.validar_suscripcion(usua_id,suscripcion);
            String rpta=list.get(0).get("susc_numero").toString();

            return "Se validò Correctamente";
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("solicitarcitar/usuarios_cita");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @PostMapping("/add_suscripcion")
    public String add_suscripcion_post(@RequestParam("usua_id") Integer usua_id,@RequestParam("subscriptionJson") String subscriptionJson){
        String response = null;
        try{
            List<Map<String,Object>> list=solicitarCitaService.validar_suscripcion(usua_id,subscriptionJson);
            String rpta=list.get(0).get("susc_numero").toString();

            return "Se validò Correctamente";
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("solicitarcitar/usuarios_cita");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }
    @GetMapping("/inactivar_suscripcion_get/{usua_id}/{suscripcion}")
    public String inactivar_suscripcion_get(@PathVariable("usua_id") Integer usua_id,@PathVariable("suscripcion") String suscripcion){
        String response = null;
        try{
            List<Map<String,Object>> list=solicitarCitaService.validar_suscripcion(usua_id,suscripcion);
            String rpta=list.get(0).get("susc_numero").toString();

            return "Se validò Correctamente";
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("solicitarcitar/usuarios_cita");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @PostMapping("/inactivar_suscripcion")
    public String inactivar_suscripcion_post(@RequestParam("usua_id") Integer usua_id,@RequestParam("subscriptionJson") String subscriptionJson){
        String response = null;
        try{
            List<Map<String,Object>> list=solicitarCitaService.inactivar_suscripcion(usua_id,subscriptionJson);
            String rpta=list.get(0).get("susc_numero").toString();

            return "Se validò Correctamente";
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("solicitarcitar/usuarios_cita");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @Scheduled(cron = "* */1 0-23 * * *", zone = "America/Mexico_City")
    public void caducar_solicitudes(){
        try {
            List<Map<String,Object>> listu= solicitarCitaService.buscar_usuarios_clientes();
            if(Integer.parseInt(listu.get(0).get("usua_id").toString())>0){
                solicitarCitaService.caducar_solicitudes(Integer.parseInt(listu.get(0).get("usua_id").toString()));
            }
            //solicitarCitaService.reenviar_notificacion();
            java.util.Date fecha = new Date();
            System.out.println (fecha.getDay());

        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/aceptar");
            logErrorService.add_log_error(obj);

        }
    }

    //@Scheduled(cron = "* */5 0-23 * * *", zone = "America/Mexico_City")
    public void reenviar_solicitud(){
        try {

            solicitarCitaService.reenviar_notificacion();
            System.out.printf("Ejecutando api /n");
        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/aceptar");
            logErrorService.add_log_error(obj);

        }
    }

    //@Scheduled(cron = "* */20 0-23 * * *", zone = "America/Mexico_City")
    public void reenviar_solicitud_urgente(){
        try {

            solicitarCitaService.reenviar_notificacion_urgente();
            System.out.printf("Ejecutando api /n");
        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/aceptar");
            logErrorService.add_log_error(obj);

        }
    }
    //@Scheduled(cron = "* */5 0-23 * * *", zone = "America/Mexico_City")
    public void reenviar_solicitud_normal(){
        try {

            solicitarCitaService.reenviar_notificacion_normal();
            System.out.printf("Ejecutando api /n");
        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/aceptar");
            logErrorService.add_log_error(obj);

        }
    }

    @GetMapping("/cancelar_solicitud/{usua_id}/{corr_id}/{motivo}")
    public ResponseEntity<Mensaje> cancelar_solicitud(@PathVariable Integer usua_id,@PathVariable Integer corr_id, @PathVariable Integer motivo){
        try {

            solicitarCitaService.cancelar_solicitud(usua_id,corr_id,motivo);
            return new ResponseEntity<Mensaje>(new Mensaje("Solicitud Cancelada"), HttpStatus.CREATED);
        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/cancelar_solicitud");
            logErrorService.add_log_error(obj);
            return new ResponseEntity<Mensaje>(new Mensaje("Error: No se puede registrar. Comuniquese con el Administrador del Sistema. "), HttpStatus.BAD_REQUEST);
        }
    }

    /*@GetMapping(path="/get_notificacion/{usua_id}")
    public ResponseEntity<List<Map<String,Object>>> get_notificacion(@PathVariable("usua_id") Integer usua_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.mostrar_notificaciones(usua_id);

            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitudes/get_notificacion/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }*/


}
