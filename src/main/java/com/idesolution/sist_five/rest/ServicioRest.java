package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.Mensaje;
import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.Concepto;
import com.idesolution.sist_five.models.Provincia;
import com.idesolution.sist_five.models.Servicio;
import com.idesolution.sist_five.models.Usuario;
import com.idesolution.sist_five.service.LogErrorService;
import com.idesolution.sist_five.service.ServicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/servicio")
public class ServicioRest {
    @Autowired
    private ServicioService servicioService;
    @Autowired
    private LogErrorService logErrorService;
    @GetMapping(path="/list_all")
    public ResponseEntity<List<Servicio>> lista(){
        ResponseEntity<List<Servicio>> response = null;
        try{
            List<Servicio> lista = servicioService.lista();
            return new ResponseEntity(lista, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("servicio/list_all");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @GetMapping(path="/list_all_mantenimiento")
    public ResponseEntity<List<Servicio>> lista_mantenimiento(){
        ResponseEntity<List<Servicio>> response = null;
        try{
            List<Servicio> lista = servicioService.lista_mantenimiento();
            return new ResponseEntity(lista, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("servicio/list_all_mantenimiento");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @GetMapping(path="/list_servicio_x_id/{serv_id}")
    public ResponseEntity<List<Servicio>> list_servicio_x_id(@PathVariable Integer serv_id){
        ResponseEntity<List<Servicio>> response = null;
        try{
            List<Servicio> lista = servicioService.list_servicio_x_id(serv_id);
            return new ResponseEntity(lista, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("servicio/list_servicio_x_id");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @PostMapping("/update_servicio")
    public ResponseEntity<?> update_servicio(@RequestBody Servicio objs){
        ResponseEntity<Servicio> response = null;
        try {
            servicioService.update_servicio(objs);
            return new ResponseEntity<>(objs, HttpStatus.CREATED);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("servicio/update_servicio");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @PostMapping ("/add_servicio")
    public ResponseEntity<?> add_servicio(@RequestBody Servicio objs){
        ResponseEntity<Usuario> response = null;

        if(servicioService.existsByServ_nombre(objs.getServ_nombre()))
            return new ResponseEntity(new Mensaje("Servicio ya existe"), HttpStatus.BAD_REQUEST);

        try {
            Long insertado=servicioService.add_servicio(objs );
            objs.setServ_id(insertado);
            return new ResponseEntity<>(objs, HttpStatus.CREATED);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("servicio/add_servicio");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @DeleteMapping("/delete_servicio/{serv_id}")
    void delete_servicio(@PathVariable Integer serv_id) {
        try {

        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("servicio/delete_servicio");
            logErrorService.add_log_error(obj);

        }
        servicioService.delete_servicio(serv_id);
    }
}