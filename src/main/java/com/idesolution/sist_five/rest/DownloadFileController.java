package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.Mensaje;
import com.idesolution.sist_five.models.Usuario;
import com.idesolution.sist_five.repositorys.S3Services;
import com.idesolution.sist_five.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.util.Base64;
import java.util.List;
@RestController
public class DownloadFileController {

    @Autowired
    S3Services s3Services;
    @Autowired
    private UsuarioService usuarioService;
    /*
     * Download Files
     */
    @GetMapping("/api/file/{id_usuario}")
    public ResponseEntity<byte[]> downloadFile(@PathVariable Integer id_usuario) {
        String foto="";

        if(id_usuario==0){
            foto=id_usuario+".png";
            ByteArrayOutputStream downloadInputStream = s3Services.downloadFile(foto);
            return ResponseEntity.ok()
                    .contentType(contentType(foto))
                    .header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\"" + foto + "\"")
                    .body(downloadInputStream.toByteArray());
            //return new ResponseEntity(new Mensaje(Base64.getEncoder().encodeToString(downloadInputStream.toByteArray())), HttpStatus.OK);
        }else{
            Usuario objU= usuarioService.get_userxid(id_usuario);
            ByteArrayOutputStream downloadInputStream = s3Services.downloadFile(objU.getUsua_foto());
            foto=objU.getUsua_foto();
            return ResponseEntity.ok()
                    .contentType(contentType(objU.getUsua_foto()))
                    .header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\"" + objU.getUsua_foto() + "\"")
                    .body(downloadInputStream.toByteArray());
            //return new ResponseEntity(new Mensaje(Base64.getEncoder().encodeToString(downloadInputStream.toByteArray())), HttpStatus.OK);
        }

    }

    @GetMapping("/api/servicio/imagen/{serv_imagen}")
    public ResponseEntity<byte[]> downloadFileServicio(@PathVariable String serv_imagen) {
        ByteArrayOutputStream downloadInputStream = s3Services.downloadFile(serv_imagen);

       return ResponseEntity.ok()
                .contentType(contentType(serv_imagen))
                .header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\"" + serv_imagen + "\"")
                .body(downloadInputStream.toByteArray());

        //return Base64.getEncoder().encodeToString(downloadInputStream.toByteArray());
        //return new ResponseEntity(new Mensaje(Base64.getEncoder().encodeToString(downloadInputStream.toByteArray())), HttpStatus.OK);
    }
    /*
     * List ALL Files
     */
    @GetMapping("/api/files")
    public List<String> listAllFiles(){
        return s3Services.listFiles();
    }

    private MediaType contentType(String keyname) {
        String[] arr = keyname.split("\\.");
        String type = arr[arr.length-1];
        switch(type) {
            case "txt": return MediaType.TEXT_PLAIN;
            case "png": return MediaType.IMAGE_PNG;
            case "jpg": return MediaType.IMAGE_JPEG;
            default: return MediaType.APPLICATION_OCTET_STREAM;
        }
    }
}
