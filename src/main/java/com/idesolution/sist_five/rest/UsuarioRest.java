package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.Mensaje;
import com.idesolution.sist_five.models.Usuario;
import com.idesolution.sist_five.service.LogErrorService;
import com.idesolution.sist_five.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.idesolution.sist_five.dto.log_error;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/user")
public class UsuarioRest {
    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private LogErrorService logErrorService;
    @GetMapping(path="/get_userlogin/{usua_email}/{usua_tipo}/{usua_clave}/{usua_tipocuenta}/{usua_idcuenta}")
    public ResponseEntity<List<Map<String, Object>>> get_usuario_login(@PathVariable("usua_email") String usua_email,
                                                           @PathVariable("usua_tipo") Integer usua_tipo,
                                                           @PathVariable("usua_clave") String usua_clave,
                                                           @PathVariable("usua_tipocuenta") Integer usua_tipocuenta,
                                                           @PathVariable("usua_idcuenta") String usua_idcuenta){

        ResponseEntity<List<Map<String, Object>>> response = null;
        try{
            List<Map<String, Object>> objU=usuarioService.get_usuario_login( usua_email, usua_tipo, usua_clave ,usua_tipocuenta,usua_idcuenta);
            return new ResponseEntity<>(objU, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("usuario/get_userlogin");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @PostMapping ("/add_user")
    public ResponseEntity<Mensaje> add_usuario(@RequestBody Usuario usuario){

        if(usuarioService.existsByDNI(usuario.getUsua_dni()))
            return new ResponseEntity(new Mensaje("Documento ya existe"), HttpStatus.BAD_REQUEST);
        if(usuarioService.existsByEmail(usuario.getUsua_email()))
            return new ResponseEntity(new Mensaje("Ya existe una cuenta con este Email"), HttpStatus.BAD_REQUEST);

        try {
            usuarioService.add_usuarios(usuario );
            return new ResponseEntity<Mensaje>(new Mensaje("Correcto"), HttpStatus.CREATED);
        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("usuario/add_usuario");
            logErrorService.add_log_error(obj);
            return new ResponseEntity<Mensaje>(new Mensaje("Error: No se puede registrar. Comuniquese con el Administrador del Sistema. "), HttpStatus.BAD_REQUEST);
        }



    }

    @GetMapping(path="/get_userxid/{usua_id}")
    public ResponseEntity<Usuario> get_userxid(@PathVariable("usua_id") Integer usua_id){
        ResponseEntity<Usuario> response = null;
        try {
            Usuario objU = usuarioService.get_userxid(usua_id);
            return new ResponseEntity<>(objU, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("usuario/get_userxid/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }
    @GetMapping(path="/get_ubicacionxid/{usua_id}")
    public ResponseEntity<List<Map<String, Object>> > get_ubicacionxid(@PathVariable("usua_id") Integer usua_id){
        ResponseEntity<List<Map<String, Object>> > response = null;
        try {
            List<Map<String, Object>>  listU = usuarioService.get_miubicacion_xid(usua_id);
            return new ResponseEntity<>(listU, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("usuario/get_ubicacionxid/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @PostMapping ("/update_user")
    public ResponseEntity<?> update_usuario(@RequestBody Usuario usuario){
        ResponseEntity<Usuario> response = null;
        try {
        usuarioService.update_usuario(usuario );
        return new ResponseEntity<>(usuario, HttpStatus.CREATED);
    }catch(Exception e) {
        log_error obj= new log_error();
        obj.setLoge_error(e.getMessage());
        obj.setLoge_webmethod("usuario/update_user");
        logErrorService.add_log_error(obj);

    }
        return response;
    }

    @GetMapping(path="/recover_password/{usua_email}")
    public ResponseEntity<Usuario> recover_password(@PathVariable("usua_email") String usua_email){
        ResponseEntity<Usuario> response = null;
        try {
        if(!usuarioService.existsByEmail(usua_email))
            return new ResponseEntity(new Mensaje("Email no existe"), HttpStatus.BAD_REQUEST);
        if(usuarioService.existsxEmail_google(usua_email))
            return new ResponseEntity(new Mensaje("Esta cuenta esta asociada directamente con google, ingrese por medio de CONTINUAR CON GOOGLE"), HttpStatus.BAD_REQUEST);
        else{
            Usuario objU=usuarioService.recuperar_clave( usua_email);
            String codigo=usuarioService.enviar_codigo_de_verficacion(objU.getUsua_id());
            String message = "Su codigo de recuperacion es: "+codigo + "\n\nRecuerda que el codigo de verificacion tiene vigencia de 5 min, en caso contrario debes solicitar un nuevo codigo."  + "\n\n Datos de contacto: " + "\n Nombre: " + objU.getUsua_nombres() + " "+ objU.getUsua_apellpat()  + " "+ objU.getUsua_apellmat() +"\n E-mail: " + objU.getUsua_email();
            usuarioService.sendEmail("jefferson2150@gmail.com",objU.getUsua_email(),"SERVICIOS GENERALES FIVE - Olvidé mi Contraseña",message);
            return new ResponseEntity<>(objU, HttpStatus.CREATED);
        }

    }catch(Exception e) {
        log_error obj= new log_error();
        obj.setLoge_error(e.getMessage());
        obj.setLoge_webmethod("usuario/recover_password/{usua_email}");
        logErrorService.add_log_error(obj);

    }
        return response;
    }

    @GetMapping(path="/validar_codigo_recuperacion/{codigo}")
    public ResponseEntity<Usuario> validar_codigo_recuperacion(@PathVariable("codigo") String codigo){
        ResponseEntity<Usuario> response = null;
        try {
            System.out.println(usuarioService.validar_codigo_recuperacion(codigo));
            if(usuarioService.validar_codigo_recuperacion(codigo)==0)
                return new ResponseEntity(new Mensaje("Código Incorrecto"), HttpStatus.BAD_REQUEST);
            if(usuarioService.validar_codigo_recuperacion(codigo)==-1)
                return new ResponseEntity(new Mensaje("Código Expirado. Solicite un nuevo código."), HttpStatus.BAD_REQUEST);
            return new ResponseEntity(new Mensaje("Código Vigente"), HttpStatus.OK);

        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("user/validar_codigo_recuperacion/{codigo}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }


    @GetMapping(path="/resetear_clave/{codigo}/{clave}")
    public ResponseEntity<Usuario> resetear_clave(@PathVariable("codigo") String codigo,@PathVariable("clave") String clave){
        ResponseEntity<Usuario> response = null;
        try {
            if(usuarioService.resetear_clave(codigo,clave)==0)
                return new ResponseEntity(new Mensaje("Código Incorrecto"), HttpStatus.BAD_REQUEST);
            if(usuarioService.resetear_clave(codigo,clave)==-1)
                return new ResponseEntity(new Mensaje("Código Expirado. Solicite un nuevo código."), HttpStatus.BAD_REQUEST);
            return new ResponseEntity(new Mensaje("Se cambió su clave correctamente."), HttpStatus.OK);

        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("user/resetear_clave/{codigo}/{clave}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }
    @GetMapping ("/verificar_dni/{usua_dni}")
    public ResponseEntity<?> verificar_dni(@PathVariable("usua_dni") String usua_dni){
        ResponseEntity<Usuario> response = null;
        try {
            if(usuarioService.existsByDNI(usua_dni))
                return new ResponseEntity(new Mensaje("Documento ya existe"), HttpStatus.BAD_REQUEST);
            return new ResponseEntity(new Mensaje("Correcto"), HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("user/verificar_dni/{usua_dni}");
            logErrorService.add_log_error(obj);

        }
            return response;

    }

    @GetMapping ("/verificar_clave/{usua_id}/{usua_clave}")
    public ResponseEntity<?> verificar_clave(@PathVariable("usua_id") Integer usua_id, @PathVariable("usua_clave") String usua_clave){
        ResponseEntity<Usuario> response = null;
        try {
            if(usuarioService.existsByClave(usua_id,usua_clave))
                return new ResponseEntity(new Mensaje("Clave es correcta"), HttpStatus.OK);
            return new ResponseEntity(new Mensaje("Clave incorrecta"), HttpStatus.BAD_REQUEST);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("user/verificar_clave/{usua_id}/{usua_clave}");
            logErrorService.add_log_error(obj);

        }
        return response;

    }


    @GetMapping ("/verificar_email/{usua_email}")
    public ResponseEntity<?> verificar_email(@PathVariable("usua_email") String usua_email){
        ResponseEntity<Usuario> response = null;
        try {
            if(usuarioService.existsByEmail(usua_email))
                return new ResponseEntity(new Mensaje("Ya existe una cuenta con este Email"), HttpStatus.BAD_REQUEST);
            return new ResponseEntity(new Mensaje("Correcto"), HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("usuario/verificar_email/{usua_email}");
            logErrorService.add_log_error(obj);

        }
            return response;

    }


    @GetMapping ("/verificar_email_public/{usua_email}")
    public ResponseEntity<?> verificar_email_public(@PathVariable("usua_email") String usua_email){
        ResponseEntity<Usuario> response = null;
        try {
            if(usuarioService.existsByEmail(usua_email))
                return new ResponseEntity(new Mensaje("Ya existe una cuenta con este Email"), HttpStatus.BAD_REQUEST);
            return new ResponseEntity(new Mensaje("Correcto"), HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("usuario/verificar_email/{usua_email}");
            logErrorService.add_log_error(obj);

        }
        return response;

    }

    @GetMapping(path="/especialistas/{espe_id}/{soci_nivel}/{soci_ubicacion}/{soci_diaaten}/{soci_horaaten}")
    public ResponseEntity<List<Map<String, Object>> > get_tecnicos_x_especilidad(@PathVariable("espe_id") Integer espe_id,
                                                                    @PathVariable("soci_nivel") Integer soci_nivel,
                                                                    @PathVariable("soci_ubicacion") String soci_ubicacion,
                                                                    @PathVariable("soci_diaaten") String soci_diaaten,
                                                                    @PathVariable("soci_horaaten") String soci_horaaten){
        ResponseEntity<List<Map<String, Object>> > response = null;
        try {
            List<Map<String, Object>>  list=usuarioService.get_tecnicos_x_especilidad(espe_id, soci_nivel, soci_ubicacion, soci_diaaten, soci_horaaten);
            System.out.println(list.size());
            return new ResponseEntity<List<Map<String, Object>> >(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("usuario/especialistas");
            logErrorService.add_log_error(obj);

        }
            return response;
    }


    @GetMapping(path="/re_especialistas_/{corr_id}/{espe_id}/{soci_nivel}/{soci_ubicacion}/{soci_diaaten}/{soci_horaaten}")
    public ResponseEntity<List<Map<String, Object>> > get_tecnicos_x_especilidad2(
            @PathVariable("corr_id") Integer corr_id,
            @PathVariable("espe_id") Integer espe_id,
             @PathVariable("soci_nivel") Integer soci_nivel,
             @PathVariable("soci_ubicacion") String soci_ubicacion,
             @PathVariable("soci_diaaten") String soci_diaaten,
             @PathVariable("soci_horaaten") String soci_horaaten){
        ResponseEntity<List<Map<String, Object>> > response = null;
        try {
            List<Map<String, Object>>  list=usuarioService.get_tecnicos_x_especilidad2(corr_id,espe_id, soci_nivel, soci_ubicacion, soci_diaaten, soci_horaaten);
            return new ResponseEntity<List<Map<String, Object>> >(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("usuario/especialistas");
            logErrorService.add_log_error(obj);

        }
        return response;
    }



}
