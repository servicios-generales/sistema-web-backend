package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.Mensaje;
import com.idesolution.sist_five.dto.ParametroA;
import com.idesolution.sist_five.models.Concepto;
import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.Etiqueta;
import com.idesolution.sist_five.models.Usuario;
import com.idesolution.sist_five.service.ConceptoService;
import com.idesolution.sist_five.service.EtiquetaService;
import com.idesolution.sist_five.service.LogErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/concepto")
public class ConceptoRest {
    @Autowired
    private ConceptoService conceptoService;

    @Autowired
    private LogErrorService logErrorService;

    @Autowired
    private EtiquetaService etiquetaService;

    @GetMapping(path="/list/{conc_prefijo}")
    public ResponseEntity<List<Concepto>> get_conceptos_nivel_urgencia(@PathVariable("conc_prefijo")Integer conc_prefijo){
        ResponseEntity<List<Concepto>> response = null;
        try {
            List<Concepto> list=conceptoService.get_conceptos(conc_prefijo);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("concepto/list");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @GetMapping(path="/terminos_condiciones")
    public ResponseEntity<List<Map<String,Object>>> get_terminos_condiciones(){
        ResponseEntity<List<Map<String,Object>>> response = null;

        try{
            List<Map<String,Object>> list=conceptoService.get_terminos_condiciones();
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("concepto/terminos_condiciones");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;


    }

    @GetMapping(path="/medios_pago")
    public ResponseEntity<List<Concepto>> get_medios_pago(){
        ResponseEntity<List<Concepto>> response = null;
        try {
            List<Concepto> list=conceptoService.get_medios_pago();
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("concepto/medios_pago");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @GetMapping(path="/medios_pago_tipos/{conc_correlativo}")
    public ResponseEntity<List<Map<String,Object>>> get_medios_pago_tipos(@PathVariable("conc_correlativo")Integer conc_correlativo){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String,Object>> list=conceptoService.get_medios_pago_tipos(conc_correlativo);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("concepto/medios_pago_tipos/{conc_correlativo}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @GetMapping(path="/detalle_pago_tipos/{conc_correlativo}/{para_int1}")
    public ResponseEntity<List<Map<String,Object>>> get_medios_pago_tipos_detalle(@PathVariable("conc_correlativo")Integer conc_correlativo,
                                                                                  @PathVariable("para_int1")Integer para_int1){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String,Object>> list=conceptoService.get_detalle_pago_tipos(conc_correlativo, para_int1);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("concepto/detalle_pago_tipos/{conc_correlativo}/{para_int1}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @GetMapping(path="/nota_visita_tecnico")
    public ResponseEntity<List<Concepto>> get_nota_visita_tecnico(){
        ResponseEntity<List<Concepto>> response = null;

        try{
            List<Concepto> list=conceptoService.get_nota_visita_tecnico();
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("concepto/nota_visita_tecnico");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;


    }

    @GetMapping(path="/list_etiquetas/{usua_id}")
    public ResponseEntity<List<Map<String,Object>>> get_list_etiquetas_xid(@PathVariable("usua_id")Integer usua_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String,Object>> list=etiquetaService.get_list_etiquetas_xid(usua_id);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("concepto/list_etiquetas/{usua_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @GetMapping(path="/detalle_etiqueta/{etiq_id}")
    public ResponseEntity<List<Etiqueta>> get_detalle_etiqueta_xid(@PathVariable("etiq_id")Integer etiq_id){
        ResponseEntity<List<Etiqueta>> response = null;
        try {
            List<Etiqueta> list=etiquetaService.get_detalle_etiqueta_xid(etiq_id);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("concepto/detalle_etiqueta/{etiq_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @GetMapping(path="/leyenda/{conc_prefijo}")
    public ResponseEntity<List<Map<String,Object>>> get_conceptos_leyenda(@PathVariable("conc_prefijo")Integer conc_prefijo){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String,Object>> list=conceptoService.get_conceptos_leyenda(conc_prefijo);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("concepto/leyenda");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @GetMapping(path="/list_cabeceras")
    public ResponseEntity<List<Map<String,Object>>> get_conceptos_cabeceras(){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String,Object>> list=conceptoService.get_conceptos_cabecera();
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("concepto/list_cabeceras");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }


    @PostMapping ("/update_concepto")
    public ResponseEntity<?> update_concepto(@RequestBody Concepto objc){
        ResponseEntity<Usuario> response = null;
        try {
            conceptoService.update_concepto(objc);
            return new ResponseEntity<>(objc, HttpStatus.CREATED);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("concepto/update_concepto");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @PostMapping ("/add_concepto")
    public ResponseEntity<Mensaje> add_concepto(@RequestBody Concepto objc){
        ResponseEntity<Mensaje> response = null;

        if(conceptoService.existsByConc_descripcion(objc.getConc_descripcion()))
            return new ResponseEntity(new Mensaje("Concepto ya existe"), HttpStatus.BAD_REQUEST);

        try {
            conceptoService.add_concepto(objc );
            return new ResponseEntity<Mensaje>(new Mensaje("Concepto registrado correctamente"), HttpStatus.CREATED);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("docker ");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @DeleteMapping("/delete_concepto/{conc_id}")
    void deleteEmployee(@PathVariable Integer conc_id) {
        try {
            conceptoService.delete_concepto(conc_id);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("concepto/add_concepto");
            logErrorService.add_log_error(obj);

        }

    }
}
