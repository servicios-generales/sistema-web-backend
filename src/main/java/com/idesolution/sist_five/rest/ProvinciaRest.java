package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.Opcion_menu;
import com.idesolution.sist_five.models.Parametro;
import com.idesolution.sist_five.models.Provincia;
import com.idesolution.sist_five.service.LogErrorService;
import com.idesolution.sist_five.service.ProvinciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/provincia")
public class ProvinciaRest {
    @Autowired
    private ProvinciaService provinciaService;

    @Autowired
    private LogErrorService logErrorService;
    @GetMapping(path="/list/{depa_id}")
    public ResponseEntity<List<Provincia>> get_provincias(@PathVariable("depa_id") Integer depa_id){
        ResponseEntity<List<Provincia>> response = null;
        try{
            List<Provincia> list=provinciaService.get_provincias( depa_id);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("provincia/list/{depa_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }
}
