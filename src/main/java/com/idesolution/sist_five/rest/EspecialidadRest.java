package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.Mensaje;
import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.Especialidad;
import com.idesolution.sist_five.models.Especialidad;
import com.idesolution.sist_five.models.Usuario;
import com.idesolution.sist_five.service.EspecialidadService;
import com.idesolution.sist_five.service.LogErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/especialidad")
public class EspecialidadRest {
    @Autowired
    private EspecialidadService especialidadService;
    @Autowired
    private LogErrorService logErrorService;

    @GetMapping(path="/listxespecialidad/{serv_id}")
    public ResponseEntity<List<Especialidad>> listar_x_especialidad(@PathVariable Integer serv_id){
        ResponseEntity<List<Especialidad>> response = null;
        try{
            List<Especialidad> lista = especialidadService.listar_especialidades_x_servicio(serv_id);
            return new ResponseEntity(lista, HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("especialidad/listxespecialidad/{serv_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
      return response;
    }

    @GetMapping(path="/listxusuario/{usua_id}")
    public ResponseEntity<List<Especialidad>> listar_x_usuario(@PathVariable Integer usua_id){
        ResponseEntity<List<Especialidad>> response = null;
        try{
            List<Especialidad> lista = especialidadService.listar_especialidad_x_usuario(usua_id);
            return new ResponseEntity(lista, HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("especialidad/listxusuario/{usua_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @GetMapping(path="/listxservicio/{serv_id}")
    public ResponseEntity<List<Especialidad>> listxservicio(@PathVariable Integer serv_id){
        ResponseEntity<List<Especialidad>> response = null;
        try{
            List<Especialidad> lista = especialidadService.listar_especialidades_x_servicio(serv_id);
            return new ResponseEntity(lista, HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("/listxservicio/{serv_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @GetMapping(path="/listxservicio_mantenimiento/{serv_id}")
    public ResponseEntity<List<Especialidad>> listxservicio_mantenimiento(@PathVariable Integer serv_id){
        ResponseEntity<List<Especialidad>> response = null;
        try{
            List<Especialidad> lista = especialidadService.listar_especialidades_x_servicio_mantenimiento(serv_id);
            return new ResponseEntity(lista, HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("/listxservicio_mantenimiento/{serv_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @GetMapping(path="/list")
    public ResponseEntity<List<Map<String, Object>>> get_listar_especialidades(){
        ResponseEntity<List<Map<String, Object>>> response = null;
        try{
            List<Map<String, Object>> lista = especialidadService.get_listar_especialidades();
            return new ResponseEntity(lista, HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("especialidad/list");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }


    @GetMapping(path="/listxid/{espe_id}")
    public ResponseEntity<List<Especialidad>> listar_x_id(@PathVariable Integer espe_id){
        ResponseEntity<List<Especialidad>> response = null;
        try{
            List<Especialidad> lista = especialidadService.list_especialidad_x_id(espe_id);
            return new ResponseEntity(lista, HttpStatus.OK);
        }catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("especialidad/listxid/{espe_id}");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }
    @PostMapping("/update_especialidad")
    public ResponseEntity<?> update_especialidad(@RequestBody Especialidad objs){
        ResponseEntity<Especialidad> response = null;
        try {
            especialidadService.update_especialidad(objs);
            return new ResponseEntity<>(objs, HttpStatus.CREATED);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("especialidad/update_especialidad");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @PostMapping ("/add_especialidad")
    public ResponseEntity<?> add_especialidad(@RequestBody Especialidad obje){
        ResponseEntity<Especialidad> response = null;

        if(especialidadService.existsByEspe_nombre(obje.getEspe_nombre()))
            return new ResponseEntity(new Mensaje("Especialidad ya existe"), HttpStatus.BAD_REQUEST);

        try {
            Long espeid=especialidadService.add_especialidad(obje);
            obje.setEspe_id(espeid);
            return new ResponseEntity<>(obje, HttpStatus.CREATED);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("especialidad/add_especialidad");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @DeleteMapping("/delete_especialidad/{espe_id}")
    void delete_especialidad(@PathVariable Integer espe_id) {
        try {

        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("especialidad/delete_especialidad");
            logErrorService.add_log_error(obj);

        }
        especialidadService.delete_especialidad(espe_id);
    }
}

