package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.ConfirmarSolicitud;
import com.idesolution.sist_five.dto.Mensaje;
import com.idesolution.sist_five.dto.Proforma;
import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.Usuario;
import com.idesolution.sist_five.service.LogErrorService;
import com.idesolution.sist_five.service.SolicitarCitaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/requerimientos")
public class Requerimientos {

    @Autowired
    private SolicitarCitaService solicitarCitaService;
    @Autowired
    private LogErrorService logErrorService;

    @GetMapping(path="/list/{usua_id}/{tipo}")
    public ResponseEntity<List<Map<String,Object>>> get_requerimientos(@PathVariable("usua_id") Integer usua_id,@PathVariable("tipo") Integer tipo){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_requerimientos(usua_id, tipo);

            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("requerimientos/list/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }
    @GetMapping(path="/list_pendientes/{usua_id}")
    public ResponseEntity<List<Map<String,Object>>> get_requerimientos_pendientes(@PathVariable("usua_id") Integer usua_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_requerimientos_pendientes(usua_id);

            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("requerimientos/list_pendientes/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @GetMapping(path="/list_confirmadas/{usua_id}")
    public ResponseEntity<List<Map<String,Object>>> get_requerimientos_confirmadas(@PathVariable("usua_id") Integer usua_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_requerimientos_confirmadas(usua_id);

            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("requerimientos/list_confirmadas/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }
    @GetMapping(path="/detalle/{soci_id}")
    public ResponseEntity<List<Map<String,Object>>> get_requerimiento_detalle(@PathVariable("soci_id") Integer soci_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_requerimiento_detalle(soci_id);
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("requerimientos/detalle/{soci_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @GetMapping(path="/detalle_pendiente/{soci_id}")
    public ResponseEntity<List<Map<String,Object>>> get_requerimiento_detalle_pendiente(@PathVariable("soci_id") Integer soci_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_requerimiento_detalle_pendiente(soci_id);
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("requerimientos/detalle_pendiente/{soci_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }


    @GetMapping(path="/detalle_confirmada/{soci_id}")
    public ResponseEntity<List<Map<String,Object>>> get_requerimiento_detalle_confirmada(@PathVariable("soci_id") Integer soci_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_requerimiento_detalle_confirmada(soci_id);
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("requerimientos/detalle_confirmada/{soci_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @GetMapping(path="/detalle_ultimo/{soci_id}")
    public ResponseEntity<List<Map<String,Object>>> get_requerimiento_detalle2(@PathVariable("soci_id") Integer soci_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.get_requerimiento_detalle2(soci_id);
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("requerimientos/detalle_ultimo/{soci_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @PostMapping("/aceptar")
    public ResponseEntity<Mensaje> acepta_solicitud(@RequestBody ConfirmarSolicitud cs){
        try {
            cs.setSoci_tipo(2); ///cuando se acepta
            if(Float.parseFloat(solicitarCitaService.obtener_cobro_five(cs.getSoci_id()))<=cs.getSoci_costo()){
                solicitarCitaService.confirmar_solicitud(cs);
                return new ResponseEntity<Mensaje>(new Mensaje("Correcto"), HttpStatus.CREATED);
            }
            return new ResponseEntity<Mensaje>(new Mensaje("El monto debe ser mayor al costo del servicio."), HttpStatus.BAD_REQUEST);
        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("requerimientos/aceptar");
            logErrorService.add_log_error(obj);
            return new ResponseEntity<Mensaje>(new Mensaje("Error: No se puede registrar. Comuniquese con el Administrador del Sistema. "), HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("/rechazar")
    public ResponseEntity<Mensaje> rechazar_solicitud(@RequestBody ConfirmarSolicitud cs){
        try {
            cs.setSoci_tipo(3); ///cuando se acepta
            solicitarCitaService.confirmar_solicitud(cs);
            return new ResponseEntity<Mensaje>(new Mensaje("Rechazado"), HttpStatus.CREATED);
        }
        catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("requerimientos/rechazar");
            logErrorService.add_log_error(obj);
            return new ResponseEntity<Mensaje>(new Mensaje("Error: No se puede registrar. Comuniquese con el Administrador del Sistema. "), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path="/obtener_costo/{soci_id}")
    public ResponseEntity<List<Map<String,Object>>> obtener_costo_servicio_five(@PathVariable("soci_id") Integer soci_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.obtener_cobro_five_largo(soci_id);
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("requerimientos/obtener_costo/{soci_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }
    @PostMapping("/add_proforma")
    public ResponseEntity<Mensaje> registrar_proforma(@RequestBody List<Proforma> p){
        //ResponseEntity<Mensaje> response = null;
        try{
            Integer prof_id=solicitarCitaService.add_proforma(p.get(0).getUsua_id(), p.get(0).getSoci_id(), p.get(0).getTotal());
            System.out.printf("PROF: /n"+prof_id);
            if(prof_id>0){
                for (int i=0; i<p.size();i++){
                    if(i!=0)
                        solicitarCitaService.add_detalle_proforma(p.get(i).getDetalle_prof(), p.get(i).getImporte_prof(), p.get(i).getCantidad(), prof_id);
                }
                return new ResponseEntity<Mensaje>(new Mensaje("Proforma Registrada Correctamente."), HttpStatus.OK);
            }else{
                return new ResponseEntity<Mensaje>(new Mensaje("El monto debe ser mayor al Costo de visita."), HttpStatus.BAD_REQUEST);
            }


        } catch(Exception e) {
            System.out.print("Error: "+e.getCause());
            return new ResponseEntity<Mensaje>(new Mensaje("Error: No se puede registrar. Comuniquese con el Administrador del Sistema. "), HttpStatus.BAD_REQUEST);
        }
        //return response;
    }

    @PostMapping("/update_proforma")
    public ResponseEntity<Proforma> modificar_proforma(@RequestBody List<Proforma> p){
        ResponseEntity<Proforma> response = null;
        try{
            Integer prof_id=solicitarCitaService.update_proforma(p.get(0).getUsua_id(), p.get(0).getSoci_id(), p.get(0).getTotal());
            System.out.printf("PROF: /n"+prof_id);
            System.out.printf("Usua: /n"+p.get(0).getUsua_id());
            System.out.printf("Soci: /n"+p.get(0).getSoci_id());
            System.out.printf("Total: /n"+p.get(0).getTotal());
            if(prof_id>0){
                for (int i=0; i<p.size();i++){
                    if(i!=0)
                    solicitarCitaService.add_detalle_proforma(p.get(i).getDetalle_prof(), p.get(i).getImporte_prof(), p.get(i).getCantidad(), prof_id);
                }

            return new ResponseEntity(new Mensaje("Proforma Actualizada Correctamente."), HttpStatus.OK);
            }else{
                return new ResponseEntity(new Mensaje("El monto debe ser mayor al Costo de visita."), HttpStatus.BAD_REQUEST);
            }
        } catch(Exception e) {
            System.out.print("Error: "+e.getCause());
        }
        return response;
    }
}
