package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.Mensaje;
import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.Calificacion;
import com.idesolution.sist_five.models.Usuario;
import com.idesolution.sist_five.service.CalificacionService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.*;
import com.idesolution.sist_five.service.LogErrorService;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/calificacion")
public class CalificacionRest {

    @Autowired
    CalificacionService calificacionService;
    @Autowired
    private LogErrorService logErrorService;

    @PostMapping("/nuevo")
    public ResponseEntity<Calificacion> add_calificacion(@RequestBody Calificacion sc){
        ResponseEntity<Calificacion> response = null;
        try{
            calificacionService.add_calificacion(sc);

            return new ResponseEntity(new Mensaje("Solicitud Enviada"), HttpStatus.OK);

        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("calificacion/nuevo");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }
    @GetMapping(path="/get_tecnicos_calificar/{usua_id}")
    public ResponseEntity<List<Map<String, Object>> > get_ubicacionxid(@PathVariable("usua_id") Integer usua_id){
        ResponseEntity<List<Map<String, Object>> > response = null;
        try {
            List<Map<String, Object>>  listU = calificacionService.listar_tecnicos_calificar(usua_id);
            return new ResponseEntity<>(listU, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("usuario/get_tecnicos_calificar/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }
    @GetMapping(path="/get_tecnicos_calificar_xid/{soci_id}")
    public ResponseEntity<List<Map<String, Object>> > tecnicos_calificar_xid(@PathVariable("soci_id") Integer soci_id){
        ResponseEntity<List<Map<String, Object>> > response = null;
        try {
            List<Map<String, Object>>  listU = calificacionService.listar_tecnicos_calificar_xid(soci_id);
            return new ResponseEntity<>(listU, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("usuario/get_tecnicos_calificar/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @GetMapping(path="/guardar_tecnico_a_calificar/{soci_id}/{cliente_id}/{especialista_id}")
    public ResponseEntity<List<Map<String, Object>> > guardar_tecnico_a_calificar(@PathVariable("soci_id") Integer soci_id,
                                                                             @PathVariable("cliente_id") Integer cliente_id,
                                                                             @PathVariable("especialista_id") Integer especialista_id){
        ResponseEntity<List<Map<String, Object>> > response = null;
        try {
            List<Map<String, Object>>  listU = calificacionService.guardar_tecnico_a_calificar(cliente_id,especialista_id,soci_id);
            return new ResponseEntity<>(listU, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("calificacion/guardar_tecnico_a_calificar/{soci_id}/{cliente_id}/{especialista_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @GetMapping(path="/get_informacion_especialista/{usua_id}")
    public ResponseEntity<List<Map<String, Object>> > get_informacion_tecnico(@PathVariable("usua_id") Integer usua_id){
        ResponseEntity<List<Map<String, Object>> > response = null;
        try {
            List<Map<String, Object>>  listU = calificacionService.get_informacion_tecnico(usua_id);
            return new ResponseEntity<>(listU, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("usuario/get_tecnicos_calificar/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

    @GetMapping(path="/get_comentarios_especialista/{usua_id}")
    public ResponseEntity<List<Map<String, Object>> > get_comentarios_tecnico(@PathVariable("usua_id") Integer usua_id){
        ResponseEntity<List<Map<String, Object>> > response = null;
        try {
            List<Map<String, Object>>  listU = calificacionService.get_comentarios_tecnico(usua_id);
            return new ResponseEntity<>(listU, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("usuario/get_tecnicos_calificar/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }
}
