package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.dto.Mensaje;
import com.idesolution.sist_five.dto.Proforma;
import com.idesolution.sist_five.dto.log_error;
import com.idesolution.sist_five.models.*;
import com.idesolution.sist_five.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/solicitarcita")
public class SolicitarCitaRest {
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private SolicitarCitaService solicitarCitaService;

    @Autowired
    private LogErrorService logErrorService;

    private final Service service;
    @Autowired
    public SolicitarCitaRest(Service service) {
        this.service = service;
    }

    @PostMapping("/unico")
    public ResponseEntity<Solicitud> solicitar_cita(@RequestBody SolicitudEtiqueta sc){
        ResponseEntity<Solicitud> response = null;
        try{
        String[] parts= sc.getSoci_destino().split(",");
        for (int i=0; i<parts.length;i++){
            SolicitudCitaEtiqueta ObjSc=new SolicitudCitaEtiqueta();
            ObjSc.setUsua_id(sc.getUsua_id());
            ObjSc.setEspe_id(sc.getEspe_id());
            ObjSc.setSoci_nivel(sc.getSoci_nivel());
            ObjSc.setSoci_ubicacion(sc.getSoci_ubicacion());
            ObjSc.setSoci_diaaten(sc.getSoci_diaaten());
            ObjSc.setSoci_horaaten(sc.getSoci_horaaten());
            ObjSc.setSoci_detalle(sc.getSoci_detalle());
            ObjSc.setSoci_destino(Integer.parseInt(parts[i]));
            ObjSc.setSoci_direccion(sc.getSoci_direccion());
            ObjSc.setSoci_estado(0);
            ObjSc.setSoci_latitud(sc.getSoci_latitud());
            ObjSc.setSoci_longitud(sc.getSoci_longitud());
            ObjSc.setSoci_instruccion(sc.getSoci_instruccion());
            ObjSc.setSoci_etiqueta(sc.getSoci_etiqueta());
            ObjSc.setSoci_idetiqueta(sc.getSoci_idetiqueta());
            ObjSc.setSoci_guardaetiqueta(sc.getSoci_guardaetiqueta());

            solicitarCitaService.solicitar_cita(ObjSc);
            Usuario objU=usuarioService.get_userxid(Integer.parseInt(parts[i]));
            //SmsRequest objsms=new SmsRequest("51"+objU.getUsua_celular(),"Hola "+objU.getUsua_nombres()+" tienes un nueva solicitud. "+ObjSc.getSoci_detalle()+". Ingresa a https://five.ide-solution.com/ ");
            //service.sendSms(objsms);
        }

        return new ResponseEntity(new Mensaje("Solicitud Enviada"), HttpStatus.OK);

        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("solicitarcitar/nuevo");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }

    @PostMapping("/unico_repetir")
    public ResponseEntity<Solicitud> resolicitar_cita(@RequestBody ReSolicitud sc){
        ResponseEntity<Solicitud> response = null;
        try{
            String[] parts= sc.getSoci_destino().split(",");
            for (int i=0; i<parts.length;i++){
                SolicitudCitaEtiqueta ObjSc=new SolicitudCitaEtiqueta();
                ObjSc.setUsua_id(sc.getUsua_id());
                ObjSc.setEspe_id(sc.getEspe_id());
                ObjSc.setSoci_nivel(sc.getSoci_nivel());
                ObjSc.setSoci_ubicacion(sc.getSoci_ubicacion());
                ObjSc.setSoci_diaaten(sc.getSoci_diaaten());
                ObjSc.setSoci_horaaten(sc.getSoci_horaaten());
                ObjSc.setSoci_detalle(sc.getSoci_detalle());
                ObjSc.setSoci_destino(Integer.parseInt(parts[i]));
                ObjSc.setSoci_direccion(sc.getSoci_direccion());
                ObjSc.setSoci_estado(0);
                ObjSc.setSoci_latitud(sc.getSoci_latitud());
                ObjSc.setSoci_longitud(sc.getSoci_longitud());
                ObjSc.setSoci_instruccion(sc.getSoci_instruccion());
                ObjSc.setSoci_idetiqueta(sc.getSoci_idetiqueta());
                ObjSc.setCorr_id(sc.getCorr_id());
                solicitarCitaService.resolicitar_cita(ObjSc);
                Usuario objU=usuarioService.get_userxid(Integer.parseInt(parts[i]));
               // SmsRequest objsms=new SmsRequest("51"+objU.getUsua_celular(),"Hola "+objU.getUsua_nombres()+" tienes un nueva solicitud. "+ObjSc.getSoci_detalle()+". Ingresa a https://five.ide-solution.com/ ");
                //service.sendSms(objsms);
            }

            return new ResponseEntity(new Mensaje("Solicitud Enviada"), HttpStatus.OK);

        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("solicitarcitar/nuevo");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }




    @PostMapping("/multiple")
    public ResponseEntity<Solicitud> solicitar_cita_multiple(@RequestBody SolicitudEtiqueta sc){
        ResponseEntity<Solicitud> response = null;
        try{
            String[] parts= sc.getSoci_destino().split(",");

            Integer correlativo=solicitarCitaService.registrar_correlativo(sc.getUsua_id());

            Integer cont=0;
            for (int i=0; i<parts.length;i++){
                SolicitudCitaEtiqueta ObjSc=new SolicitudCitaEtiqueta();
                ObjSc.setUsua_id(sc.getUsua_id());
                ObjSc.setEspe_id(sc.getEspe_id());
                ObjSc.setSoci_nivel(sc.getSoci_nivel());
                ObjSc.setSoci_ubicacion(sc.getSoci_ubicacion());
                ObjSc.setSoci_diaaten(sc.getSoci_diaaten());
                ObjSc.setSoci_horaaten(sc.getSoci_horaaten());
                ObjSc.setSoci_detalle(sc.getSoci_detalle());
                ObjSc.setSoci_destino(Integer.parseInt(parts[i]));
                ObjSc.setSoci_direccion(sc.getSoci_direccion());
                ObjSc.setSoci_estado(0);
                ObjSc.setSoci_latitud(sc.getSoci_latitud());
                ObjSc.setSoci_longitud(sc.getSoci_longitud());
                ObjSc.setSoci_instruccion(sc.getSoci_instruccion());
                ObjSc.setSoci_etiqueta(sc.getSoci_etiqueta());
                ObjSc.setSoci_idetiqueta(sc.getSoci_idetiqueta());
                ObjSc.setSoci_guardaetiqueta(sc.getSoci_guardaetiqueta());
                ObjSc.setCorr_id(correlativo);
                solicitarCitaService.solicitar_cita_multiple(ObjSc);
                cont++;
                Usuario objU=usuarioService.get_userxid(Integer.parseInt(parts[i]));
                //SmsRequest objsms=new SmsRequest("51"+objU.getUsua_celular(),"Hola "+objU.getUsua_nombres()+" tienes un nueva solicitud. "+ObjSc.getSoci_detalle()+". Ingresa a https://five.ide-solution.com/ ");
                //service.sendSms(objsms);
            }
            //SimpleDateFormat d = new SimpleDateFormat("dd/MM/yyyy");
            //Date date = d.parse(sc.getSoci_diaaten());
            //System.out.println(DateFormat.getDateInstance().format(date));

            solicitarCitaService.add_alerta(correlativo,sc.getSoci_diaaten(),sc.getSoci_horaaten(),"a1.png","-",sc.getUsua_id());

            return new ResponseEntity(new Mensaje("Solicitud Enviada"), HttpStatus.OK);

        } catch(Exception e) {
            System.out.print("Error: "+e.getCause());
        }
        return response;
    }


    @PostMapping("/multiple_repetir")
    public ResponseEntity<Solicitud> resolicitar_cita_multiple(@RequestBody ReSolicitud sc){
        ResponseEntity<Solicitud> response = null;
        try{
            String[] parts= sc.getSoci_destino().split(",");

            Integer correlativo=sc.getCorr_id();
            solicitarCitaService.inactivar_solicitud(correlativo);

            Integer cont=0;
            for (int i=0; i<parts.length;i++){
                SolicitudCitaEtiqueta ObjSc=new SolicitudCitaEtiqueta();
                ObjSc.setUsua_id(sc.getUsua_id());
                ObjSc.setEspe_id(sc.getEspe_id());
                ObjSc.setSoci_nivel(sc.getSoci_nivel());
                ObjSc.setSoci_ubicacion(sc.getSoci_ubicacion());
                ObjSc.setSoci_diaaten(sc.getSoci_diaaten());
                ObjSc.setSoci_horaaten(sc.getSoci_horaaten());
                ObjSc.setSoci_detalle(sc.getSoci_detalle());
                ObjSc.setSoci_destino(Integer.parseInt(parts[i]));
                ObjSc.setSoci_direccion(sc.getSoci_direccion());
                ObjSc.setSoci_estado(0);
                ObjSc.setSoci_latitud(sc.getSoci_latitud());
                ObjSc.setSoci_longitud(sc.getSoci_longitud());
                ObjSc.setSoci_instruccion(sc.getSoci_instruccion());
                ObjSc.setSoci_idetiqueta(sc.getSoci_idetiqueta());
                ObjSc.setCorr_id(correlativo);
                solicitarCitaService.resolicitar_cita_multiple(ObjSc);
                cont++;
                Usuario objU=usuarioService.get_userxid(Integer.parseInt(parts[i]));
               // SmsRequest objsms=new SmsRequest("51"+objU.getUsua_celular(),"Hola "+objU.getUsua_nombres()+" tienes un nueva solicitud. "+ObjSc.getSoci_detalle()+". Ingresa a https://five.ide-solution.com/ ");
               // service.sendSms(objsms);
            }
            //SimpleDateFormat d = new SimpleDateFormat("dd/MM/yyyy");
            //Date date = d.parse(sc.getSoci_diaaten());
            //System.out.println(DateFormat.getDateInstance().format(date));

            solicitarCitaService.add_alerta(correlativo,sc.getSoci_diaaten(),sc.getSoci_horaaten(),"a1.png","-",sc.getUsua_id());

            return new ResponseEntity(new Mensaje("Solicitud Enviada"), HttpStatus.OK);

        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("resolicitarcitar/multiple");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }


    @PostMapping("/usuarios_cita")
    public ResponseEntity<List<Usuario>> usuarios_cita(@RequestBody SolicitudCita sc){
        ResponseEntity<List<Usuario>> response = null;
        try{
            List<Usuario> list=solicitarCitaService.usuarios_cita(sc);
        return new ResponseEntity<List<Usuario>>(list, HttpStatus.OK);
    } catch(Exception e) {
        log_error objlog=new log_error();
        objlog.setLoge_webmethod("solicitarcitar/usuarios_cita");
        objlog.setLoge_error("Error: "+e.getMessage());
        logErrorService.add_log_error(objlog);
    }
        return response;
    }

    


    @GetMapping(path="/alertas/{usua_id}")
    public ResponseEntity<List<Map<String,Object>>> list_alertas(@PathVariable("usua_id") Integer usua_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.list_alertas(usua_id);
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitarcita/alertas/{usua_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }
    @GetMapping(path="/repetir/{corr_id}")
    public ResponseEntity<List<Map<String,Object>>> repetir_solicitud(@PathVariable("corr_id") Integer corr_id){
        ResponseEntity<List<Map<String,Object>>> response = null;
        try {
            List<Map<String, Object>>  list= solicitarCitaService.repetir_solicitud_cita(corr_id);
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(Exception e) {
            log_error obj= new log_error();
            obj.setLoge_error(e.getMessage());
            obj.setLoge_webmethod("solicitarcita/repetir/{corr_id}");
            logErrorService.add_log_error(obj);

        }
        return response;
    }

/*
    @PostMapping("/multiple_repetir")
    public ResponseEntity<Solicitud> resolicitar_cita_multiple(@RequestBody ReSolicitud sc){
        ResponseEntity<Solicitud> response = null;
        try{

                SmsRequest objsms=new SmsRequest("51"+objU.getUsua_celular(),"Hola "+objU.getUsua_nombres()+" tienes un nueva solicitud. "+ObjSc.getSoci_detalle()+". Ingresa a https://five.ide-solution.com/ ");
                service.sendSms(objsms);

            return new ResponseEntity(new Mensaje("Solicitud Enviada"), HttpStatus.OK);

        } catch(Exception e) {
            log_error objlog=new log_error();
            objlog.setLoge_webmethod("resolicitarcitar/multiple");
            objlog.setLoge_error("Error: "+e.getMessage());
            logErrorService.add_log_error(objlog);
        }
        return response;
    }*/




}
