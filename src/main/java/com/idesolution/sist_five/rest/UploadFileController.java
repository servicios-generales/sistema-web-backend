package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.repositorys.S3Services;
import com.idesolution.sist_five.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.awt.desktop.SystemSleepEvent;

@RestController
public class UploadFileController {
    @Autowired
    S3Services s3Services;
    @Autowired
    private UsuarioService usuarioService;

    @PostMapping("/api/file/upload")
    public String uploadMultipartFile(@RequestParam("keyname") String keyName, @RequestParam("file") MultipartFile file) {
        // String keyName = file.getOriginalFilename();
        s3Services.uploadFile(keyName+".png", file);
        usuarioService.update_usuario_foto(keyName,keyName+".png");
        return "Upload Successfully. -> KeyName = " + keyName;
    }


    @PostMapping("/api/file/upload_especialidad")
    public String uploadMultipartFileEspecialidad(@RequestParam("keyname") String keyName, @RequestParam("file") MultipartFile file) {
        // String keyName = file.getOriginalFilename();
        s3Services.uploadFile(keyName+"e.png", file);
        usuarioService.update_usuario_foto(keyName+"e",keyName+"e.png");
        return "Upload Successfully. -> KeyName = " + keyName+"e";
    }
}
