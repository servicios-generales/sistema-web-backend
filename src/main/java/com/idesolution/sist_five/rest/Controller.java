package com.idesolution.sist_five.rest;

import com.idesolution.sist_five.models.SmsRequest;
import com.idesolution.sist_five.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/solicitud")
public class Controller {
    private final Service service;
    @Autowired
    public Controller(Service service){
        this.service=service;
    }
    @PostMapping(path="/enviarauto")
    public void sendSmsAuto(@Valid @RequestBody SmsRequest smsRequest){
        service.sendSms(smsRequest);
    }

    @PostMapping(path="/enviar")
    public void sendSms(@Valid @RequestBody SmsRequest smsRequest){
        service.sendSms(smsRequest);
    }
}
